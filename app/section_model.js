sketchit.models.Section = Ext.regModel('Section', {
    fields: [
        {
            name: 'tag',
            type: 'int'
        }, {
            name: 'name',
            type: 'string'
        }, {
            name: 'type',
            type: 'string'
        }, {
            name: 'phone',
            type: 'string'
        }
    ],

    validations: [
        {
            type: 'presence',
            name: 'name'
        }, {
            type: 'format',
            name: 'email',
            matcher: /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/,
            message: 'must be a valid email'
        }
    ],

    proxy: {
        type: 'localstorage',
        id: 'sencha-users'
    }
});