(function() {
	var $D = DirectFEA;
	var fps = 0, now, lastUpdate = (new Date)*1 - 1;
	// The higher this value, the less the FPS will be affected by quick changes
	// Setting this to 1 will show you the FPS of the last sampled frame only
	var fpsFilter = 50;
	var isAnalysisReadyToRun = false;
	var duplicateNodes = false;
	var defaultSettings = {
		mode : 'draw',
		fps : 100,
		touchMoveAnimation : false,
		touchMoveFps : 10,
		snapAnimationFps : 50,
		snapAnimationElapse : 1500,
		dynamicsAnalysis: false,
		dynamicAnalysisNsteps : 700,
		analysisType : 'Static',

		autoAnalysis : true,
		showMessage : true,
		messageBoxPositionX : 10,
		messageBoxPositionY : 10,
		messageTextFont : "bold 12px sans-serif",
		messageTextFillStye : "rgb(0,0,0)",
		messageLastTime : 5,
		
		frameRatePositionX : 10,
		frameRatePositionY : 30,

		modelScale : 2.0,
		loadScale : 100.0,
		// loadScale : 1.0,

		mouseWheelSpeedFactor : 5, //1~10, scale = e^(wheelDelta*speedFactor/speedBase)
		mouseWheelSpeedBase : 5000, //fixed

		viewPortScale : 1.0,
		maxViewPortScale : 20.0,
		minViewPortScale : 0.2,
		viewPortShiftX : 0.0,
		viewPortShiftY : 0.0,

		showDeformation : true,
		showMoment : true,

		// deformationScale : 20,
		deformationScale : 300,
		momentScale : 0.01,
		autoDeformationScale : true,
		maxDeformationOnScreen : 40,
		deformationResolution : 20,
		autoMomentScale : true,
		maxMomentOnScreen : 80,
		momentResolution : 20,

		snapToNode : true,
		snapToNodeThreshold : 10,
		moveSnapToNodeThreshold: 10,

		snapToLine : true,
		snapToLineThreshold : 25,

		snapToGrid : true,
		grid : 20,
		gridLineWidth : 1,
		gridLineStyle : "rgba(0,0,0,0.5)",

		SPCSnapToDirection : true,
		SPCSnapToDirectionThreshold : 0.2,

		SPCSnapToNode : true,
		SPCSnapToNodeThreshold : 15,

		SPCSnapToLine : true,
		SPCSnapToLineThreshold : 15,

		circleSnapToSPCThreshold : 25,

		loadSnapToNode: true,
		loadSnapToNodeThreshold : 15,
		
		loadSnapToLine : true,
		loadSnapToLineThreshold : 15,

		autoMergeNodeOnLine : true,
		autoMergeNodeOnLineThreshold : 10,

		showStructure: true,
		showNodeId : false,
		showElementId : false,
		showMarks : false,
		showGrid : true,
		showSPC : true,
		showLineElementDirection : false,
		showConstraints: true,
		showLoads:true,
		showNodes: true,
		

		canvasBgColor : "rgb(255,255,255)",
		inputStrokeStyle : "rgb(0,0,255)",
		inputStrokeWidth : 2,

		// defaultLineELementType : DirectFEA.ElasticBeamColumn,
		defaultLineELementType : "ElasticBeamColumn",
		// defaultGeomTransf : Domain.theGeomTransfs[3], 
		defaultGeomTransfId : 3,// 1:linear, 2:PDelta, 3:Corotational
		defaultNodeLoadType : "load",

		UniformElementLoadDirectionThreshold : 0.3,
		UniformElementLoadSnapToLineThreshold : 15,

		circleSelectThreshold : 0.1,
		clickSelectThreshold : 10,
		
		autoMesh: true,
		autoMeshSize: 10
	};
	
	
	sketchit = new Ext.Application({
  		name: "sketchit",
	
	    launch: function() {
	        this.views.viewport = new this.views.Viewport();
	        
	        this.views.exportModelPanel = new this.views.ExportModelPanel();
	        
	        this.views.settingTabs = new this.views.SettingTabs();
	        this.views.settingTabs.getComponent(4).on("show",this.loadSettingsToSettingTabs,this);
			this.views.settingTabs.getComponent(4).getComponent(1).setHandler(this.applySettingsFromSettingTabs,this);
			this.views.inspector = new sketchit.views.Inspector();
			
			this.views.animator = new sketchit.views.AnimateWidget({
				hidden : true
			});
	        
	
	        // var canvas = this.views.viewport.getComponent(0);
	        var canvas = this.views.viewport.down("#canvas");
	        
			var topBar = this.views.viewport.down("#topBar");;
			var bottomBar = this.views.viewport.down("#bottomBar");;
	        this.views.canvas = canvas;
			this.views.topBar = topBar;
			this.views.bottomBar = bottomBar;
			
			this.views.settingButton = bottomBar.getComponent(0);
			this.views.runButton = bottomBar.getComponent(1);
			this.views.rescaleButton = bottomBar.getComponent(2);
			this.views.meshButton = bottomBar.getComponent(3);
			this.views.clearAllButton = bottomBar.getComponent(5);
			this.views.unselectAllButton = bottomBar.getComponent(6);
			this.views.deleteButton = bottomBar.getComponent(7);
			this.views.undoButton = bottomBar.getComponent(8);
			this.views.redoButton = bottomBar.getComponent(9);
			this.views.saveButton = bottomBar.getComponent(10);
			this.views.terminalButton = bottomBar.getComponent(11);
			this.views.openButton = bottomBar.getComponent(12);
			
			this.views.logButton = bottomBar.getComponent(13);
			this.views.modeSelector = bottomBar.getComponent(15);
			this.views.analysisTypeSelector = topBar.getComponent(0);
			
			this.views.showInspectorButton = topBar.getComponent(2);
			this.views.showInspectorButton.setHandler(function(btn,e){
				// if (this.views.inspector.isHidden()) {
					// this.views.inspector.showBy(btn);
				// } else {
					// this.views.inspector.hide();				
				// }
				
				// var eles=this.Domain.exportToJSON().theElements;
				// var arr=[];
				// for (var i in eles) {
					// if(eles.hasOwnProperty(i) && $D.isDefined(eles[i])) {
						// arr.push(eles[i]);
					// }
				// }
				var arr = sketchit.dataAdapter.getElementList();
				sketchit.views.elementList.getStore().loadData(arr);
				sketchit.views.elementList.show()
// 				
				
				
			},this)
			
			this.views.showAnimatorButton = topBar.getComponent(3);
			this.views.showAnimatorButton.setHandler(function(btn,e){
				if (this.views.animator.isHidden()) {
					this.views.animator.show();
				} else {
					this.views.animator.hide();
				}
			},this)
			
			this.views.showLibraryButton = topBar.getComponent(4);
			this.views.showLibraryButton.setHandler(function(btn,e){
				if (this.views.libraryView.isHidden()) {
					this.views.libraryView.showBy(btn);
				} else {
					this.views.libraryView.hide();
				}
			},this)
			
			//TODO:
			// this.views.showAnimatorButton = topBar.getComponent(3);
			
			this.views.viewport.on({
				orientationchange : this.onOrientationchange,
				scope : this
			})

			// init canvas handlers
			this.views.canvas.mon(this.views.canvas.el, {
				doubletap : this.onDoubleTap,
				touchmove : this.onTouchMove,
				touchstart : this.onTouchStart,
				touchend : this.onTouchEnd,
				mousewheel : this.onMouseWheel,
				pinchstart : this.onPinchStart,
				pinch : this.onPinch,
				pinchend : this.onPinchEnd,
				scope : this
			});

			// // setting button
			// var showMoreSetting=function(btn){
// 				
			// };
			this.views.settingButton.setHandler(function(btn,event){
				if (this.views.settingTabs.isHidden()) {
					this.views.settingTabs.showBy(btn)
				} else {
					this.views.settingTabs.hide();
				}
			}, this);
			
			//run button
			// var player = new sketchit.views.AnimateWidget();
			var player = this.views.animator;
			// player.show();
			// player.hide();
			window.player=player;
			
			this.views.runButton.setHandler(function() {
				if (player.isHidden()) {
					player.show();
					var time = "0.020";
					var Dm =this.Domain;
					var tid;
					var f=function(){
						
						// Dm.loadNodeDispResultAtT(time);
						// time = parseFloat(time) + 0.02;
						// time = time.toFixed(3)+'';
						var dt = 20;
						var buffer_gap = 2;
						var time2 = parseFloat(time) + buffer_gap;
						time2=time2.toFixed(3)+'';
						
						if (Dm.nodeDispHistory[time]) {
							Dm.loadNodeDispResultAtT(time);
							time = parseFloat(time) + dt/1000;
							time = time.toFixed(3)+'';
							sketchit.printMessage("animating...")
							
							
						} else {
							sketchit.printMessage("buffering...")
						}
						
						tid = setTimeout(f,dt);
					};
					var play=function(){
						f();
						player.getComponent(0).setIconClass('ButtonPause')
					};
					var pause=function(){
						clearTimeout(tid);
						player.getComponent(0).setIconClass('ButtonFFW')
					};
					var reset=function(){
						pause();
						time = "0.020";
						Dm.loadNodeDispResultAtT(time);
					};
					
					player.getComponent(0).setHandler(function(){
						if (player.getComponent(0).iconCls==="ButtonFFW") {
							play();
						} else {
							pause();
						}
					},this);
					
					player.getComponent(1).setHandler(function(){
						reset();
					},this);
					
					player.getComponent(2).setHandler(function(){
						player.hide();
					},this);
				}
				
				this.analyze();
				play();
			}, this);
			
			// autoScale button					
			this.views.rescaleButton.setHandler(this.autoScale, this);
			
			//mesh button
			this.views.meshButton.setHandler(this.autoMesh, this);
			
			//clear all button
			this.views.clearAllButton.setHandler(this.clearAll, this);

			//unselect all button
			this.views.unselectAllButton.setHandler(function() {
				this.Domain.mark();
				if(!this.Domain["unselectAll"]()) {
					this.printMessage("do nothing");
					this.Domain.unmark();
				} else {
					this.Domain.group();
				}
			}, this);
			
			//delete button
			this.views.deleteButton.setHandler(function() {
				this.Domain.mark();
				if(!this.Domain["removeSelectedObjects"]()) {
					this.printMessage("do nothing");
					this.Domain.unmark();
				} else {
					this.Domain.group();
					if(this.settings.autoAnalysis) {
						this.analyze(function() {
						});
					} else {
					}
				}
			}, this);
			
			//undo button
			this.views.undoButton.setHandler(this.undo, this);
			this.views.undoButton.setDisabled(true);
			
			//redo button
			this.views.redoButton.setHandler(this.redo, this);
			this.views.redoButton.setDisabled(true);
			
			//save button
			this.views.saveButton.setHandler(this.saveScript, this);
			
			//terminal button
			var term = new Ext.Panel({
				floating: true,
				// hideOnMaskTap : false,
				width: 500,
				height: 300,
				style : {
					opacity:0.8
				},
				layout: {
					type : 'fit'
				},
				items: [{
		        	xtype : 'textareafield',
                    maxRows : 12,
		        }]
			});
			term.show();
			
			var commands = "";
			var scope = this;
			term.getComponent(0).el.dom.onkeypress=function(e){
				if (e.keyCode == 13) {
					scope.ws.send(commands);
					commands="";
				} else {
					commands += String.fromCharCode(e.keyCode);
				}
				
			};
			term.hide();
			
			this.views.terminalButton.setHandler(function(btn,e){
				if (!term || term.isHidden()) {
					term.showBy(btn);
				} else {
					term.hide();
				}					
			}, this);
			
			
			//Open button
			var dm = this.Domain;
			var importModel = function(){
				var obj = eval("("+importwindow.getComponent(0).getValue()+")");
				dm.BuildModelFromJson(obj)
				
			}
			var importwindow = new Ext.Panel({
				floating: true,
				// hideOnMaskTap : false,
				hidden:true,
				width: 500,
				height: 400,
				style : {
					opacity:0.8
				},
				// layout: {
					// // type : 'vbox'
				// },
				items: [{
		        	xtype : 'textareafield',
                    maxRows : 15,
		        },{
		        	xtype : 'button',
		        	ui: 'confirm',
		        	text : 'import',
		        	handler : importModel
		        }]
			});
			
			this.views.openButton.setHandler(function(btn,e){
				if (term.isHidden()) {
					importwindow.showBy(btn);
				} else {
					importwindow.hide();
				}					
			}, this);

			//log button
			this.views.logButton.setHandler(this.showLog, this);
			
			//mode toggle button
			this.views.modeSelector.on({
				toggle : function() {
					this.settings.mode = this.views.modeSelector.getPressed().text;
				},
				scope : this
			});
			
			//analysisType toggle button
			this.views.analysisTypeSelector.on({
				toggle : function() {
					this.settings.analysisType = this.views.analysisTypeSelector.getPressed().text;
					if (this.settings.analysisType == 'Static') {
						this.settings.autoAnalysis = true;
					} else {
						this.settings.autoAnalysis = false;
					}
				},
				scope : this
			});
			
			
			
			
			//init model Domain
			this.Domain = new $D.Domain();
			window.Domain = this.Domain;
			var str = '{"theNodes":{"1":{"X":380,"Y":560,"beforeMoveX":380,"beforeMoveY":560,"dispX":0,"dispY":0,"dispRZ":0,"isSelected":false,"isShow":true,"showEdge":false,"showDeformation":true,"showTag":true,"eleCount":2,"connectType":"beam","massX":511.9882811158865,"massY":511.9882811158865,"massRz":511.9882811158865,"id":1,"elementIDs":["1","2"],"nodeLoadIDs":"","ajacentNodeIDs":["11","23"]},"2":{"X":340,"Y":500,"beforeMoveX":340,"beforeMoveY":500,"dispX":0,"dispY":0,"dispRZ":0,"isSelected":false,"isShow":true,"showEdge":false,"showDeformation":true,"showTag":true,"eleCount":4,"connectType":"beam","massX":1107.9941405579434,"massY":1107.9941405579434,"massRz":1107.9941405579434,"id":2,"elementIDs":["3","4","7","32"],"nodeLoadIDs":"","ajacentNodeIDs":["22","26","29","35"]},"3":{"X":420,"Y":440,"beforeMoveX":420,"beforeMoveY":440,"dispX":0,"dispY":0,"dispRZ":0,"isSelected":false,"isShow":true,"showEdge":false,"showDeformation":true,"showTag":true,"eleCount":4,"connectType":"beam","massX":1136,"massY":1136,"massRz":1136,"id":3,"elementIDs":["6","13","42","48"],"nodeLoadIDs":"","ajacentNodeIDs":["32","33","38","53"]},"4":{"X":420,"Y":500,"beforeMoveX":420,"beforeMoveY":500,"dispX":0,"dispY":0,"dispRZ":0,"isSelected":false,"isShow":true,"showEdge":false,"showDeformation":true,"showTag":true,"eleCount":4,"connectType":"beam","massX":1107.9941405579439,"massY":1107.9941405579439,"massRz":1107.9941405579439,"id":4,"elementIDs":["5","9","35","38"],"nodeLoadIDs":"","ajacentNodeIDs":["25","28","31","41"]},"5":{"X":340,"Y":260,"beforeMoveX":340,"beforeMoveY":260,"dispX":0,"dispY":0,"dispRZ":0,"isSelected":false,"isShow":true,"showEdge":false,"showDeformation":true,"showTag":true,"eleCount":3,"connectType":"beam","massX":885.5216528049699,"massY":885.5216528049699,"massRz":885.5216528049699,"id":5,"elementIDs":["20","74","86"],"nodeLoadIDs":"","ajacentNodeIDs":["64","76","77"]},"6":{"X":420,"Y":260,"beforeMoveX":420,"beforeMoveY":260,"dispX":0,"dispY":0,"dispRZ":0,"isSelected":false,"isShow":true,"showEdge":false,"showDeformation":true,"showTag":true,"eleCount":3,"connectType":"beam","massX":885.5216528049699,"massY":885.5216528049699,"massRz":885.5216528049699,"id":6,"elementIDs":["21","68","82"],"nodeLoadIDs":"","ajacentNodeIDs":["58","72","78"]},"7":{"X":340,"Y":440,"beforeMoveX":340,"beforeMoveY":440,"dispX":0,"dispY":0,"dispRZ":0,"isSelected":false,"isShow":true,"showEdge":false,"showDeformation":true,"showTag":true,"eleCount":4,"connectType":"beam","massX":1136,"massY":1136,"massRz":1136,"id":7,"elementIDs":["8","11","40","54"],"nodeLoadIDs":"","ajacentNodeIDs":["30","39","44","47"]},"8":{"X":420,"Y":380,"beforeMoveX":420,"beforeMoveY":380,"dispX":0,"dispY":0,"dispRZ":0,"isSelected":false,"isShow":true,"showEdge":false,"showDeformation":true,"showTag":true,"eleCount":4,"connectType":"beam","massX":1136,"massY":1136,"massRz":1136,"id":8,"elementIDs":["10","44","60","78"],"nodeLoadIDs":"","ajacentNodeIDs":["34","45","50","68"]},"9":{"X":340,"Y":380,"beforeMoveX":340,"beforeMoveY":380,"dispX":0,"dispY":0,"dispRZ":0,"isSelected":false,"isShow":true,"showEdge":false,"showDeformation":true,"showTag":true,"eleCount":4,"connectType":"beam","massX":1136,"massY":1136,"massRz":1136,"id":9,"elementIDs":["12","15","50","66"],"nodeLoadIDs":"","ajacentNodeIDs":["40","51","56","59"]},"10":{"X":420,"Y":320,"beforeMoveX":420,"beforeMoveY":320,"dispX":0,"dispY":0,"dispRZ":0,"isSelected":false,"isShow":true,"showEdge":false,"showDeformation":true,"showTag":true,"eleCount":4,"connectType":"beam","massX":1136,"massY":1136,"massRz":1136,"id":10,"elementIDs":["14","19","56","72"],"nodeLoadIDs":"","ajacentNodeIDs":["46","57","62","73"]},"11":{"X":370,"Y":545,"beforeMoveX":370,"beforeMoveY":545,"dispX":0,"dispY":0,"dispRZ":0,"isSelected":false,"isShow":true,"showEdge":false,"showDeformation":true,"showTag":true,"eleCount":2,"connectType":"beam","massX":511.9882811158864,"massY":511.9882811158864,"massRz":511.9882811158864,"id":11,"elementIDs":["1","30"],"nodeLoadIDs":"","ajacentNodeIDs":["1","21"]},"12":{"X":320,"Y":220,"beforeMoveX":320,"beforeMoveY":220,"dispX":0,"dispY":0,"dispRZ":0,"isSelected":false,"isShow":true,"showEdge":false,"showDeformation":true,"showTag":true,"eleCount":3,"connectType":"beam","massX":885.52165280497,"massY":885.52165280497,"massRz":885.52165280497,"id":12,"elementIDs":["22","23","87"],"nodeLoadIDs":"","ajacentNodeIDs":["77","79","84"]},"13":{"X":340,"Y":320,"beforeMoveX":340,"beforeMoveY":320,"dispX":0,"dispY":0,"dispRZ":0,"isSelected":false,"isShow":true,"showEdge":false,"showDeformation":true,"showTag":true,"eleCount":4,"connectType":"beam","massX":1136,"massY":1136,"massRz":1136,"id":13,"elementIDs":["16","17","18","62"],"nodeLoadIDs":"","ajacentNodeIDs":["52","63","65","69"]},"14":{"X":440,"Y":220,"beforeMoveX":440,"beforeMoveY":220,"dispX":0,"dispY":0,"dispRZ":0,"isSelected":false,"isShow":true,"showEdge":false,"showDeformation":true,"showTag":true,"eleCount":3,"connectType":"beam","massX":885.52165280497,"massY":885.52165280497,"massRz":885.52165280497,"id":14,"elementIDs":["26","88","93"],"nodeLoadIDs":"","ajacentNodeIDs":["78","83","98"]},"15":{"X":260,"Y":220,"beforeMoveX":260,"beforeMoveY":220,"dispX":0,"dispY":0,"dispRZ":0,"isSelected":false,"isShow":true,"showEdge":false,"showDeformation":true,"showTag":true,"eleCount":2,"connectType":"beam","massX":568,"massY":568,"massRz":568,"id":15,"elementIDs":["24","95"],"nodeLoadIDs":"","ajacentNodeIDs":["85","86"]},"16":{"X":260,"Y":180,"beforeMoveX":260,"beforeMoveY":180,"dispX":0,"dispY":0,"dispRZ":0,"isSelected":false,"isShow":true,"showEdge":false,"showDeformation":true,"showTag":true,"eleCount":3,"connectType":"beam","massX":835.7577678093063,"massY":835.7577678093063,"massRz":835.7577678093063,"id":16,"elementIDs":["25","28","96"],"nodeLoadIDs":"","ajacentNodeIDs":["86","87","101"]},"17":{"X":500,"Y":180,"beforeMoveX":500,"beforeMoveY":180,"dispX":0,"dispY":0,"dispRZ":0,"isSelected":false,"isShow":true,"showEdge":false,"showDeformation":true,"showTag":true,"eleCount":3,"connectType":"beam","massX":835.7577678093066,"massY":835.7577678093066,"massRz":835.7577678093066,"id":17,"elementIDs":["29","107","110"],"nodeLoadIDs":"","ajacentNodeIDs":["97","100","103"]},"18":{"X":500,"Y":220,"beforeMoveX":500,"beforeMoveY":220,"dispX":0,"dispY":0,"dispRZ":0,"isSelected":false,"isShow":true,"showEdge":false,"showDeformation":true,"showTag":true,"eleCount":2,"connectType":"beam","massX":568,"massY":568,"massRz":568,"id":18,"elementIDs":["27","109"],"nodeLoadIDs":"","ajacentNodeIDs":["99","100"]},"19":{"X":220,"Y":140,"beforeMoveX":220,"beforeMoveY":140,"dispX":0,"dispY":0,"dispRZ":0,"isSelected":false,"isShow":true,"showEdge":false,"showDeformation":true,"showTag":true,"eleCount":1,"connectType":"beam","massX":267.7577678093062,"massY":267.7577678093062,"massRz":267.7577678093062,"id":19,"elementIDs":["112"],"nodeLoadIDs":"","ajacentNodeIDs":["102"]},"20":{"X":540,"Y":140,"beforeMoveX":540,"beforeMoveY":140,"dispX":0,"dispY":0,"dispRZ":0,"isSelected":false,"isShow":true,"showEdge":false,"showDeformation":true,"showTag":true,"eleCount":1,"connectType":"beam","massX":267.7577678093065,"massY":267.7577678093065,"massRz":267.7577678093065,"id":20,"elementIDs":["114"],"nodeLoadIDs":"","ajacentNodeIDs":["104"]},"21":{"X":360,"Y":530,"beforeMoveX":360,"beforeMoveY":530,"dispX":0,"dispY":0,"dispRZ":0,"isSelected":false,"isShow":true,"showEdge":false,"showDeformation":true,"showTag":true,"eleCount":2,"connectType":"beam","massX":511.9882811158864,"massY":511.9882811158864,"massRz":511.9882811158864,"id":21,"elementIDs":["30","31"],"nodeLoadIDs":"","ajacentNodeIDs":["11","22"]},"22":{"X":350,"Y":515,"beforeMoveX":350,"beforeMoveY":515,"dispX":0,"dispY":0,"dispRZ":0,"isSelected":false,"isShow":true,"showEdge":false,"showDeformation":true,"showTag":true,"eleCount":2,"connectType":"beam","massX":511.9882811158864,"massY":511.9882811158864,"massRz":511.9882811158864,"id":22,"elementIDs":["31","32"],"nodeLoadIDs":"","ajacentNodeIDs":["2","21"]},"23":{"X":390,"Y":545,"beforeMoveX":390,"beforeMoveY":545,"dispX":0,"dispY":0,"dispRZ":0,"isSelected":false,"isShow":true,"showEdge":false,"showDeformation":true,"showTag":true,"eleCount":2,"connectType":"beam","massX":511.9882811158864,"massY":511.9882811158864,"massRz":511.9882811158864,"id":23,"elementIDs":["2","33"],"nodeLoadIDs":"","ajacentNodeIDs":["1","24"]},"24":{"X":400,"Y":530,"beforeMoveX":400,"beforeMoveY":530,"dispX":0,"dispY":0,"dispRZ":0,"isSelected":false,"isShow":true,"showEdge":false,"showDeformation":true,"showTag":true,"eleCount":2,"connectType":"beam","massX":511.9882811158864,"massY":511.9882811158864,"massRz":511.9882811158864,"id":24,"elementIDs":["33","34"],"nodeLoadIDs":"","ajacentNodeIDs":["23","25"]},"25":{"X":410,"Y":515,"beforeMoveX":410,"beforeMoveY":515,"dispX":0,"dispY":0,"dispRZ":0,"isSelected":false,"isShow":true,"showEdge":false,"showDeformation":true,"showTag":true,"eleCount":2,"connectType":"beam","massX":511.9882811158864,"massY":511.9882811158864,"massRz":511.9882811158864,"id":25,"elementIDs":["34","35"],"nodeLoadIDs":"","ajacentNodeIDs":["4","24"]},"26":{"X":360,"Y":500,"beforeMoveX":360,"beforeMoveY":500,"dispX":0,"dispY":0,"dispRZ":0,"isSelected":false,"isShow":true,"showEdge":false,"showDeformation":true,"showTag":true,"eleCount":2,"connectType":"beam","massX":568,"massY":568,"massRz":568,"id":26,"elementIDs":["3","36"],"nodeLoadIDs":"","ajacentNodeIDs":["2","27"]},"27":{"X":380,"Y":500,"beforeMoveX":380,"beforeMoveY":500,"dispX":0,"dispY":0,"dispRZ":0,"isSelected":false,"isShow":true,"showEdge":false,"showDeformation":true,"showTag":true,"eleCount":2,"connectType":"beam","massX":568,"massY":568,"massRz":568,"id":27,"elementIDs":["36","37"],"nodeLoadIDs":"","ajacentNodeIDs":["26","28"]},"28":{"X":400,"Y":500,"beforeMoveX":400,"beforeMoveY":500,"dispX":0,"dispY":0,"dispRZ":0,"isSelected":false,"isShow":true,"showEdge":false,"showDeformation":true,"showTag":true,"eleCount":2,"connectType":"beam","massX":568,"massY":568,"massRz":568,"id":28,"elementIDs":["37","38"],"nodeLoadIDs":"","ajacentNodeIDs":["4","27"]},"29":{"X":340,"Y":480,"beforeMoveX":340,"beforeMoveY":480,"dispX":0,"dispY":0,"dispRZ":0,"isSelected":false,"isShow":true,"showEdge":false,"showDeformation":true,"showTag":true,"eleCount":2,"connectType":"beam","massX":568,"massY":568,"massRz":568,"id":29,"elementIDs":["4","39"],"nodeLoadIDs":"","ajacentNodeIDs":["2","30"]},"30":{"X":340,"Y":460,"beforeMoveX":340,"beforeMoveY":460,"dispX":0,"dispY":0,"dispRZ":0,"isSelected":false,"isShow":true,"showEdge":false,"showDeformation":true,"showTag":true,"eleCount":2,"connectType":"beam","massX":568,"massY":568,"massRz":568,"id":30,"elementIDs":["39","40"],"nodeLoadIDs":"","ajacentNodeIDs":["7","29"]},"31":{"X":420,"Y":480,"beforeMoveX":420,"beforeMoveY":480,"dispX":0,"dispY":0,"dispRZ":0,"isSelected":false,"isShow":true,"showEdge":false,"showDeformation":true,"showTag":true,"eleCount":2,"connectType":"beam","massX":568,"massY":568,"massRz":568,"id":31,"elementIDs":["5","41"],"nodeLoadIDs":"","ajacentNodeIDs":["4","32"]},"32":{"X":420,"Y":460,"beforeMoveX":420,"beforeMoveY":460,"dispX":0,"dispY":0,"dispRZ":0,"isSelected":false,"isShow":true,"showEdge":false,"showDeformation":true,"showTag":true,"eleCount":2,"connectType":"beam","massX":568,"massY":568,"massRz":568,"id":32,"elementIDs":["41","42"],"nodeLoadIDs":"","ajacentNodeIDs":["3","31"]},"33":{"X":420,"Y":420,"beforeMoveX":420,"beforeMoveY":420,"dispX":0,"dispY":0,"dispRZ":0,"isSelected":false,"isShow":true,"showEdge":false,"showDeformation":true,"showTag":true,"eleCount":2,"connectType":"beam","massX":568,"massY":568,"massRz":568,"id":33,"elementIDs":["6","43"],"nodeLoadIDs":"","ajacentNodeIDs":["3","34"]},"34":{"X":420,"Y":400,"beforeMoveX":420,"beforeMoveY":400,"dispX":0,"dispY":0,"dispRZ":0,"isSelected":false,"isShow":true,"showEdge":false,"showDeformation":true,"showTag":true,"eleCount":2,"connectType":"beam","massX":568,"massY":568,"massRz":568,"id":34,"elementIDs":["43","44"],"nodeLoadIDs":"","ajacentNodeIDs":["8","33"]},"35":{"X":356,"Y":488,"beforeMoveX":356,"beforeMoveY":488,"dispX":0,"dispY":0,"dispRZ":0,"isSelected":false,"isShow":true,"showEdge":false,"showDeformation":true,"showTag":true,"eleCount":2,"connectType":"beam","massX":568,"massY":568,"massRz":568,"id":35,"elementIDs":["7","45"],"nodeLoadIDs":"","ajacentNodeIDs":["2","36"]},"36":{"X":372,"Y":476,"beforeMoveX":372,"beforeMoveY":476,"dispX":0,"dispY":0,"dispRZ":0,"isSelected":false,"isShow":true,"showEdge":false,"showDeformation":true,"showTag":true,"eleCount":2,"connectType":"beam","massX":568,"massY":568,"massRz":568,"id":36,"elementIDs":["45","46"],"nodeLoadIDs":"","ajacentNodeIDs":["35","37"]},"37":{"X":388,"Y":464,"beforeMoveX":388,"beforeMoveY":464,"dispX":0,"dispY":0,"dispRZ":0,"isSelected":false,"isShow":true,"showEdge":false,"showDeformation":true,"showTag":true,"eleCount":2,"connectType":"beam","massX":568,"massY":568,"massRz":568,"id":37,"elementIDs":["46","47"],"nodeLoadIDs":"","ajacentNodeIDs":["36","38"]},"38":{"X":404,"Y":452,"beforeMoveX":404,"beforeMoveY":452,"dispX":0,"dispY":0,"dispRZ":0,"isSelected":false,"isShow":true,"showEdge":false,"showDeformation":true,"showTag":true,"eleCount":2,"connectType":"beam","massX":568,"massY":568,"massRz":568,"id":38,"elementIDs":["47","48"],"nodeLoadIDs":"","ajacentNodeIDs":["3","37"]},"39":{"X":340,"Y":420,"beforeMoveX":340,"beforeMoveY":420,"dispX":0,"dispY":0,"dispRZ":0,"isSelected":false,"isShow":true,"showEdge":false,"showDeformation":true,"showTag":true,"eleCount":2,"connectType":"beam","massX":568,"massY":568,"massRz":568,"id":39,"elementIDs":["8","49"],"nodeLoadIDs":"","ajacentNodeIDs":["7","40"]},"40":{"X":340,"Y":400,"beforeMoveX":340,"beforeMoveY":400,"dispX":0,"dispY":0,"dispRZ":0,"isSelected":false,"isShow":true,"showEdge":false,"showDeformation":true,"showTag":true,"eleCount":2,"connectType":"beam","massX":568,"massY":568,"massRz":568,"id":40,"elementIDs":["49","50"],"nodeLoadIDs":"","ajacentNodeIDs":["9","39"]},"41":{"X":404,"Y":488,"beforeMoveX":404,"beforeMoveY":488,"dispX":0,"dispY":0,"dispRZ":0,"isSelected":false,"isShow":true,"showEdge":false,"showDeformation":true,"showTag":true,"eleCount":2,"connectType":"beam","massX":568,"massY":568,"massRz":568,"id":41,"elementIDs":["9","51"],"nodeLoadIDs":"","ajacentNodeIDs":["4","42"]},"42":{"X":388,"Y":476,"beforeMoveX":388,"beforeMoveY":476,"dispX":0,"dispY":0,"dispRZ":0,"isSelected":false,"isShow":true,"showEdge":false,"showDeformation":true,"showTag":true,"eleCount":2,"connectType":"beam","massX":568,"massY":568,"massRz":568,"id":42,"elementIDs":["51","52"],"nodeLoadIDs":"","ajacentNodeIDs":["41","43"]},"43":{"X":372,"Y":464,"beforeMoveX":372,"beforeMoveY":464,"dispX":0,"dispY":0,"dispRZ":0,"isSelected":false,"isShow":true,"showEdge":false,"showDeformation":true,"showTag":true,"eleCount":2,"connectType":"beam","massX":568,"massY":568,"massRz":568,"id":43,"elementIDs":["52","53"],"nodeLoadIDs":"","ajacentNodeIDs":["42","44"]},"44":{"X":356,"Y":452,"beforeMoveX":356,"beforeMoveY":452,"dispX":0,"dispY":0,"dispRZ":0,"isSelected":false,"isShow":true,"showEdge":false,"showDeformation":true,"showTag":true,"eleCount":2,"connectType":"beam","massX":568,"massY":568,"massRz":568,"id":44,"elementIDs":["53","54"],"nodeLoadIDs":"","ajacentNodeIDs":["7","43"]},"45":{"X":420,"Y":360,"beforeMoveX":420,"beforeMoveY":360,"dispX":0,"dispY":0,"dispRZ":0,"isSelected":false,"isShow":true,"showEdge":false,"showDeformation":true,"showTag":true,"eleCount":2,"connectType":"beam","massX":568,"massY":568,"massRz":568,"id":45,"elementIDs":["10","55"],"nodeLoadIDs":"","ajacentNodeIDs":["8","46"]},"46":{"X":420,"Y":340,"beforeMoveX":420,"beforeMoveY":340,"dispX":0,"dispY":0,"dispRZ":0,"isSelected":false,"isShow":true,"showEdge":false,"showDeformation":true,"showTag":true,"eleCount":2,"connectType":"beam","massX":568,"massY":568,"massRz":568,"id":46,"elementIDs":["55","56"],"nodeLoadIDs":"","ajacentNodeIDs":["10","45"]},"47":{"X":356,"Y":428,"beforeMoveX":356,"beforeMoveY":428,"dispX":0,"dispY":0,"dispRZ":0,"isSelected":false,"isShow":true,"showEdge":false,"showDeformation":true,"showTag":true,"eleCount":2,"connectType":"beam","massX":568,"massY":568,"massRz":568,"id":47,"elementIDs":["11","57"],"nodeLoadIDs":"","ajacentNodeIDs":["7","48"]},"48":{"X":372,"Y":416,"beforeMoveX":372,"beforeMoveY":416,"dispX":0,"dispY":0,"dispRZ":0,"isSelected":false,"isShow":true,"showEdge":false,"showDeformation":true,"showTag":true,"eleCount":2,"connectType":"beam","massX":568,"massY":568,"massRz":568,"id":48,"elementIDs":["57","58"],"nodeLoadIDs":"","ajacentNodeIDs":["47","49"]},"49":{"X":388,"Y":404,"beforeMoveX":388,"beforeMoveY":404,"dispX":0,"dispY":0,"dispRZ":0,"isSelected":false,"isShow":true,"showEdge":false,"showDeformation":true,"showTag":true,"eleCount":2,"connectType":"beam","massX":568,"massY":568,"massRz":568,"id":49,"elementIDs":["58","59"],"nodeLoadIDs":"","ajacentNodeIDs":["48","50"]},"50":{"X":404,"Y":392,"beforeMoveX":404,"beforeMoveY":392,"dispX":0,"dispY":0,"dispRZ":0,"isSelected":false,"isShow":true,"showEdge":false,"showDeformation":true,"showTag":true,"eleCount":2,"connectType":"beam","massX":568,"massY":568,"massRz":568,"id":50,"elementIDs":["59","60"],"nodeLoadIDs":"","ajacentNodeIDs":["8","49"]},"51":{"X":340,"Y":360,"beforeMoveX":340,"beforeMoveY":360,"dispX":0,"dispY":0,"dispRZ":0,"isSelected":false,"isShow":true,"showEdge":false,"showDeformation":true,"showTag":true,"eleCount":2,"connectType":"beam","massX":568,"massY":568,"massRz":568,"id":51,"elementIDs":["12","61"],"nodeLoadIDs":"","ajacentNodeIDs":["9","52"]},"52":{"X":340,"Y":340,"beforeMoveX":340,"beforeMoveY":340,"dispX":0,"dispY":0,"dispRZ":0,"isSelected":false,"isShow":true,"showEdge":false,"showDeformation":true,"showTag":true,"eleCount":2,"connectType":"beam","massX":568,"massY":568,"massRz":568,"id":52,"elementIDs":["61","62"],"nodeLoadIDs":"","ajacentNodeIDs":["13","51"]},"53":{"X":404,"Y":428,"beforeMoveX":404,"beforeMoveY":428,"dispX":0,"dispY":0,"dispRZ":0,"isSelected":false,"isShow":true,"showEdge":false,"showDeformation":true,"showTag":true,"eleCount":2,"connectType":"beam","massX":568,"massY":568,"massRz":568,"id":53,"elementIDs":["13","63"],"nodeLoadIDs":"","ajacentNodeIDs":["3","54"]},"54":{"X":388,"Y":416,"beforeMoveX":388,"beforeMoveY":416,"dispX":0,"dispY":0,"dispRZ":0,"isSelected":false,"isShow":true,"showEdge":false,"showDeformation":true,"showTag":true,"eleCount":2,"connectType":"beam","massX":568,"massY":568,"massRz":568,"id":54,"elementIDs":["63","64"],"nodeLoadIDs":"","ajacentNodeIDs":["53","55"]},"55":{"X":372,"Y":404,"beforeMoveX":372,"beforeMoveY":404,"dispX":0,"dispY":0,"dispRZ":0,"isSelected":false,"isShow":true,"showEdge":false,"showDeformation":true,"showTag":true,"eleCount":2,"connectType":"beam","massX":568,"massY":568,"massRz":568,"id":55,"elementIDs":["64","65"],"nodeLoadIDs":"","ajacentNodeIDs":["54","56"]},"56":{"X":356,"Y":392,"beforeMoveX":356,"beforeMoveY":392,"dispX":0,"dispY":0,"dispRZ":0,"isSelected":false,"isShow":true,"showEdge":false,"showDeformation":true,"showTag":true,"eleCount":2,"connectType":"beam","massX":568,"massY":568,"massRz":568,"id":56,"elementIDs":["65","66"],"nodeLoadIDs":"","ajacentNodeIDs":["9","55"]},"57":{"X":420,"Y":300,"beforeMoveX":420,"beforeMoveY":300,"dispX":0,"dispY":0,"dispRZ":0,"isSelected":false,"isShow":true,"showEdge":false,"showDeformation":true,"showTag":true,"eleCount":2,"connectType":"beam","massX":568,"massY":568,"massRz":568,"id":57,"elementIDs":["14","67"],"nodeLoadIDs":"","ajacentNodeIDs":["10","58"]},"58":{"X":420,"Y":280,"beforeMoveX":420,"beforeMoveY":280,"dispX":0,"dispY":0,"dispRZ":0,"isSelected":false,"isShow":true,"showEdge":false,"showDeformation":true,"showTag":true,"eleCount":2,"connectType":"beam","massX":568,"massY":568,"massRz":568,"id":58,"elementIDs":["67","68"],"nodeLoadIDs":"","ajacentNodeIDs":["6","57"]},"59":{"X":356,"Y":368,"beforeMoveX":356,"beforeMoveY":368,"dispX":0,"dispY":0,"dispRZ":0,"isSelected":false,"isShow":true,"showEdge":false,"showDeformation":true,"showTag":true,"eleCount":2,"connectType":"beam","massX":568,"massY":568,"massRz":568,"id":59,"elementIDs":["15","69"],"nodeLoadIDs":"","ajacentNodeIDs":["9","60"]},"60":{"X":372,"Y":356,"beforeMoveX":372,"beforeMoveY":356,"dispX":0,"dispY":0,"dispRZ":0,"isSelected":false,"isShow":true,"showEdge":false,"showDeformation":true,"showTag":true,"eleCount":2,"connectType":"beam","massX":568,"massY":568,"massRz":568,"id":60,"elementIDs":["69","70"],"nodeLoadIDs":"","ajacentNodeIDs":["59","61"]},"61":{"X":388,"Y":344,"beforeMoveX":388,"beforeMoveY":344,"dispX":0,"dispY":0,"dispRZ":0,"isSelected":false,"isShow":true,"showEdge":false,"showDeformation":true,"showTag":true,"eleCount":2,"connectType":"beam","massX":568,"massY":568,"massRz":568,"id":61,"elementIDs":["70","71"],"nodeLoadIDs":"","ajacentNodeIDs":["60","62"]},"62":{"X":404,"Y":332,"beforeMoveX":404,"beforeMoveY":332,"dispX":0,"dispY":0,"dispRZ":0,"isSelected":false,"isShow":true,"showEdge":false,"showDeformation":true,"showTag":true,"eleCount":2,"connectType":"beam","massX":568,"massY":568,"massRz":568,"id":62,"elementIDs":["71","72"],"nodeLoadIDs":"","ajacentNodeIDs":["10","61"]},"63":{"X":340,"Y":300,"beforeMoveX":340,"beforeMoveY":300,"dispX":0,"dispY":0,"dispRZ":0,"isSelected":false,"isShow":true,"showEdge":false,"showDeformation":true,"showTag":true,"eleCount":2,"connectType":"beam","massX":568,"massY":568,"massRz":568,"id":63,"elementIDs":["16","73"],"nodeLoadIDs":"","ajacentNodeIDs":["13","64"]},"64":{"X":340,"Y":280,"beforeMoveX":340,"beforeMoveY":280,"dispX":0,"dispY":0,"dispRZ":0,"isSelected":false,"isShow":true,"showEdge":false,"showDeformation":true,"showTag":true,"eleCount":2,"connectType":"beam","massX":568,"massY":568,"massRz":568,"id":64,"elementIDs":["73","74"],"nodeLoadIDs":"","ajacentNodeIDs":["5","63"]},"65":{"X":356,"Y":332,"beforeMoveX":356,"beforeMoveY":332,"dispX":0,"dispY":0,"dispRZ":0,"isSelected":false,"isShow":true,"showEdge":false,"showDeformation":true,"showTag":true,"eleCount":2,"connectType":"beam","massX":568,"massY":568,"massRz":568,"id":65,"elementIDs":["17","75"],"nodeLoadIDs":"","ajacentNodeIDs":["13","66"]},"66":{"X":372,"Y":344,"beforeMoveX":372,"beforeMoveY":344,"dispX":0,"dispY":0,"dispRZ":0,"isSelected":false,"isShow":true,"showEdge":false,"showDeformation":true,"showTag":true,"eleCount":2,"connectType":"beam","massX":568,"massY":568,"massRz":568,"id":66,"elementIDs":["75","76"],"nodeLoadIDs":"","ajacentNodeIDs":["65","67"]},"67":{"X":388,"Y":356,"beforeMoveX":388,"beforeMoveY":356,"dispX":0,"dispY":0,"dispRZ":0,"isSelected":false,"isShow":true,"showEdge":false,"showDeformation":true,"showTag":true,"eleCount":2,"connectType":"beam","massX":568,"massY":568,"massRz":568,"id":67,"elementIDs":["76","77"],"nodeLoadIDs":"","ajacentNodeIDs":["66","68"]},"68":{"X":404,"Y":368,"beforeMoveX":404,"beforeMoveY":368,"dispX":0,"dispY":0,"dispRZ":0,"isSelected":false,"isShow":true,"showEdge":false,"showDeformation":true,"showTag":true,"eleCount":2,"connectType":"beam","massX":568,"massY":568,"massRz":568,"id":68,"elementIDs":["77","78"],"nodeLoadIDs":"","ajacentNodeIDs":["8","67"]},"69":{"X":356,"Y":308,"beforeMoveX":356,"beforeMoveY":308,"dispX":0,"dispY":0,"dispRZ":0,"isSelected":false,"isShow":true,"showEdge":false,"showDeformation":true,"showTag":true,"eleCount":2,"connectType":"beam","massX":568,"massY":568,"massRz":568,"id":69,"elementIDs":["18","79"],"nodeLoadIDs":"","ajacentNodeIDs":["13","70"]},"70":{"X":372,"Y":296,"beforeMoveX":372,"beforeMoveY":296,"dispX":0,"dispY":0,"dispRZ":0,"isSelected":false,"isShow":true,"showEdge":false,"showDeformation":true,"showTag":true,"eleCount":2,"connectType":"beam","massX":568,"massY":568,"massRz":568,"id":70,"elementIDs":["79","80"],"nodeLoadIDs":"","ajacentNodeIDs":["69","71"]},"71":{"X":388,"Y":284,"beforeMoveX":388,"beforeMoveY":284,"dispX":0,"dispY":0,"dispRZ":0,"isSelected":false,"isShow":true,"showEdge":false,"showDeformation":true,"showTag":true,"eleCount":2,"connectType":"beam","massX":568,"massY":568,"massRz":568,"id":71,"elementIDs":["80","81"],"nodeLoadIDs":"","ajacentNodeIDs":["70","72"]},"72":{"X":404,"Y":272,"beforeMoveX":404,"beforeMoveY":272,"dispX":0,"dispY":0,"dispRZ":0,"isSelected":false,"isShow":true,"showEdge":false,"showDeformation":true,"showTag":true,"eleCount":2,"connectType":"beam","massX":568,"massY":568,"massRz":568,"id":72,"elementIDs":["81","82"],"nodeLoadIDs":"","ajacentNodeIDs":["6","71"]},"73":{"X":404,"Y":308,"beforeMoveX":404,"beforeMoveY":308,"dispX":0,"dispY":0,"dispRZ":0,"isSelected":false,"isShow":true,"showEdge":false,"showDeformation":true,"showTag":true,"eleCount":2,"connectType":"beam","massX":568,"massY":568,"massRz":568,"id":73,"elementIDs":["19","83"],"nodeLoadIDs":"","ajacentNodeIDs":["10","74"]},"74":{"X":388,"Y":296,"beforeMoveX":388,"beforeMoveY":296,"dispX":0,"dispY":0,"dispRZ":0,"isSelected":false,"isShow":true,"showEdge":false,"showDeformation":true,"showTag":true,"eleCount":2,"connectType":"beam","massX":568,"massY":568,"massRz":568,"id":74,"elementIDs":["83","84"],"nodeLoadIDs":"","ajacentNodeIDs":["73","75"]},"75":{"X":372,"Y":284,"beforeMoveX":372,"beforeMoveY":284,"dispX":0,"dispY":0,"dispRZ":0,"isSelected":false,"isShow":true,"showEdge":false,"showDeformation":true,"showTag":true,"eleCount":2,"connectType":"beam","massX":568,"massY":568,"massRz":568,"id":75,"elementIDs":["84","85"],"nodeLoadIDs":"","ajacentNodeIDs":["74","76"]},"76":{"X":356,"Y":272,"beforeMoveX":356,"beforeMoveY":272,"dispX":0,"dispY":0,"dispRZ":0,"isSelected":false,"isShow":true,"showEdge":false,"showDeformation":true,"showTag":true,"eleCount":2,"connectType":"beam","massX":568,"massY":568,"massRz":568,"id":76,"elementIDs":["85","86"],"nodeLoadIDs":"","ajacentNodeIDs":["5","75"]},"77":{"X":330,"Y":240,"beforeMoveX":330,"beforeMoveY":240,"dispX":0,"dispY":0,"dispRZ":0,"isSelected":false,"isShow":true,"showEdge":false,"showDeformation":true,"showTag":true,"eleCount":2,"connectType":"beam","massX":635.0433056099403,"massY":635.0433056099403,"massRz":635.0433056099403,"id":77,"elementIDs":["20","87"],"nodeLoadIDs":"","ajacentNodeIDs":["5","12"]},"78":{"X":430,"Y":240,"beforeMoveX":430,"beforeMoveY":240,"dispX":0,"dispY":0,"dispRZ":0,"isSelected":false,"isShow":true,"showEdge":false,"showDeformation":true,"showTag":true,"eleCount":2,"connectType":"beam","massX":635.0433056099403,"massY":635.0433056099403,"massRz":635.0433056099403,"id":78,"elementIDs":["21","88"],"nodeLoadIDs":"","ajacentNodeIDs":["6","14"]},"79":{"X":340,"Y":220,"beforeMoveX":340,"beforeMoveY":220,"dispX":0,"dispY":0,"dispRZ":0,"isSelected":false,"isShow":true,"showEdge":false,"showDeformation":true,"showTag":true,"eleCount":2,"connectType":"beam","massX":568,"massY":568,"massRz":568,"id":79,"elementIDs":["22","89"],"nodeLoadIDs":"","ajacentNodeIDs":["12","80"]},"80":{"X":360,"Y":220,"beforeMoveX":360,"beforeMoveY":220,"dispX":0,"dispY":0,"dispRZ":0,"isSelected":false,"isShow":true,"showEdge":false,"showDeformation":true,"showTag":true,"eleCount":2,"connectType":"beam","massX":568,"massY":568,"massRz":568,"id":80,"elementIDs":["89","90"],"nodeLoadIDs":"","ajacentNodeIDs":["79","81"]},"81":{"X":380,"Y":220,"beforeMoveX":380,"beforeMoveY":220,"dispX":0,"dispY":0,"dispRZ":0,"isSelected":false,"isShow":true,"showEdge":false,"showDeformation":true,"showTag":true,"eleCount":2,"connectType":"beam","massX":568,"massY":568,"massRz":568,"id":81,"elementIDs":["90","91"],"nodeLoadIDs":"","ajacentNodeIDs":["80","82"]},"82":{"X":400,"Y":220,"beforeMoveX":400,"beforeMoveY":220,"dispX":0,"dispY":0,"dispRZ":0,"isSelected":false,"isShow":true,"showEdge":false,"showDeformation":true,"showTag":true,"eleCount":2,"connectType":"beam","massX":568,"massY":568,"massRz":568,"id":82,"elementIDs":["91","92"],"nodeLoadIDs":"","ajacentNodeIDs":["81","83"]},"83":{"X":420,"Y":220,"beforeMoveX":420,"beforeMoveY":220,"dispX":0,"dispY":0,"dispRZ":0,"isSelected":false,"isShow":true,"showEdge":false,"showDeformation":true,"showTag":true,"eleCount":2,"connectType":"beam","massX":568,"massY":568,"massRz":568,"id":83,"elementIDs":["92","93"],"nodeLoadIDs":"","ajacentNodeIDs":["14","82"]},"84":{"X":300,"Y":220,"beforeMoveX":300,"beforeMoveY":220,"dispX":0,"dispY":0,"dispRZ":0,"isSelected":false,"isShow":true,"showEdge":false,"showDeformation":true,"showTag":true,"eleCount":2,"connectType":"beam","massX":568,"massY":568,"massRz":568,"id":84,"elementIDs":["23","94"],"nodeLoadIDs":"","ajacentNodeIDs":["12","85"]},"85":{"X":280,"Y":220,"beforeMoveX":280,"beforeMoveY":220,"dispX":0,"dispY":0,"dispRZ":0,"isSelected":false,"isShow":true,"showEdge":false,"showDeformation":true,"showTag":true,"eleCount":2,"connectType":"beam","massX":568,"massY":568,"massRz":568,"id":85,"elementIDs":["94","95"],"nodeLoadIDs":"","ajacentNodeIDs":["15","84"]},"86":{"X":260,"Y":200,"beforeMoveX":260,"beforeMoveY":200,"dispX":0,"dispY":0,"dispRZ":0,"isSelected":false,"isShow":true,"showEdge":false,"showDeformation":true,"showTag":true,"eleCount":2,"connectType":"beam","massX":568,"massY":568,"massRz":568,"id":86,"elementIDs":["24","96"],"nodeLoadIDs":"","ajacentNodeIDs":["15","16"]},"87":{"X":280,"Y":180,"beforeMoveX":280,"beforeMoveY":180,"dispX":0,"dispY":0,"dispRZ":0,"isSelected":false,"isShow":true,"showEdge":false,"showDeformation":true,"showTag":true,"eleCount":2,"connectType":"beam","massX":568,"massY":568,"massRz":568,"id":87,"elementIDs":["25","97"],"nodeLoadIDs":"","ajacentNodeIDs":["16","88"]},"88":{"X":300,"Y":180,"beforeMoveX":300,"beforeMoveY":180,"dispX":0,"dispY":0,"dispRZ":0,"isSelected":false,"isShow":true,"showEdge":false,"showDeformation":true,"showTag":true,"eleCount":2,"connectType":"beam","massX":568,"massY":568,"massRz":568,"id":88,"elementIDs":["97","98"],"nodeLoadIDs":"","ajacentNodeIDs":["87","89"]},"89":{"X":320,"Y":180,"beforeMoveX":320,"beforeMoveY":180,"dispX":0,"dispY":0,"dispRZ":0,"isSelected":false,"isShow":true,"showEdge":false,"showDeformation":true,"showTag":true,"eleCount":2,"connectType":"beam","massX":568,"massY":568,"massRz":568,"id":89,"elementIDs":["98","99"],"nodeLoadIDs":"","ajacentNodeIDs":["88","90"]},"90":{"X":340,"Y":180,"beforeMoveX":340,"beforeMoveY":180,"dispX":0,"dispY":0,"dispRZ":0,"isSelected":false,"isShow":true,"showEdge":false,"showDeformation":true,"showTag":true,"eleCount":2,"connectType":"beam","massX":568,"massY":568,"massRz":568,"id":90,"elementIDs":["99","100"],"nodeLoadIDs":"","ajacentNodeIDs":["89","91"]},"91":{"X":360,"Y":180,"beforeMoveX":360,"beforeMoveY":180,"dispX":0,"dispY":0,"dispRZ":0,"isSelected":false,"isShow":true,"showEdge":false,"showDeformation":true,"showTag":true,"eleCount":2,"connectType":"beam","massX":568,"massY":568,"massRz":568,"id":91,"elementIDs":["100","101"],"nodeLoadIDs":"","ajacentNodeIDs":["90","92"]},"92":{"X":380,"Y":180,"beforeMoveX":380,"beforeMoveY":180,"dispX":0,"dispY":0,"dispRZ":0,"isSelected":false,"isShow":true,"showEdge":false,"showDeformation":true,"showTag":true,"eleCount":2,"connectType":"beam","massX":568,"massY":568,"massRz":568,"id":92,"elementIDs":["101","102"],"nodeLoadIDs":"","ajacentNodeIDs":["91","93"]},"93":{"X":400,"Y":180,"beforeMoveX":400,"beforeMoveY":180,"dispX":0,"dispY":0,"dispRZ":0,"isSelected":false,"isShow":true,"showEdge":false,"showDeformation":true,"showTag":true,"eleCount":2,"connectType":"beam","massX":568,"massY":568,"massRz":568,"id":93,"elementIDs":["102","103"],"nodeLoadIDs":"","ajacentNodeIDs":["92","94"]},"94":{"X":420,"Y":180,"beforeMoveX":420,"beforeMoveY":180,"dispX":0,"dispY":0,"dispRZ":0,"isSelected":false,"isShow":true,"showEdge":false,"showDeformation":true,"showTag":true,"eleCount":2,"connectType":"beam","massX":568,"massY":568,"massRz":568,"id":94,"elementIDs":["103","104"],"nodeLoadIDs":"","ajacentNodeIDs":["93","95"]},"95":{"X":440,"Y":180,"beforeMoveX":440,"beforeMoveY":180,"dispX":0,"dispY":0,"dispRZ":0,"isSelected":false,"isShow":true,"showEdge":false,"showDeformation":true,"showTag":true,"eleCount":2,"connectType":"beam","massX":568,"massY":568,"massRz":568,"id":95,"elementIDs":["104","105"],"nodeLoadIDs":"","ajacentNodeIDs":["94","96"]},"96":{"X":460,"Y":180,"beforeMoveX":460,"beforeMoveY":180,"dispX":0,"dispY":0,"dispRZ":0,"isSelected":false,"isShow":true,"showEdge":false,"showDeformation":true,"showTag":true,"eleCount":2,"connectType":"beam","massX":568,"massY":568,"massRz":568,"id":96,"elementIDs":["105","106"],"nodeLoadIDs":"","ajacentNodeIDs":["95","97"]},"97":{"X":480,"Y":180,"beforeMoveX":480,"beforeMoveY":180,"dispX":0,"dispY":0,"dispRZ":0,"isSelected":false,"isShow":true,"showEdge":false,"showDeformation":true,"showTag":true,"eleCount":2,"connectType":"beam","massX":568,"massY":568,"massRz":568,"id":97,"elementIDs":["106","107"],"nodeLoadIDs":"","ajacentNodeIDs":["17","96"]},"98":{"X":460,"Y":220,"beforeMoveX":460,"beforeMoveY":220,"dispX":0,"dispY":0,"dispRZ":0,"isSelected":false,"isShow":true,"showEdge":false,"showDeformation":true,"showTag":true,"eleCount":2,"connectType":"beam","massX":568,"massY":568,"massRz":568,"id":98,"elementIDs":["26","108"],"nodeLoadIDs":"","ajacentNodeIDs":["14","99"]},"99":{"X":480,"Y":220,"beforeMoveX":480,"beforeMoveY":220,"dispX":0,"dispY":0,"dispRZ":0,"isSelected":false,"isShow":true,"showEdge":false,"showDeformation":true,"showTag":true,"eleCount":2,"connectType":"beam","massX":568,"massY":568,"massRz":568,"id":99,"elementIDs":["108","109"],"nodeLoadIDs":"","ajacentNodeIDs":["18","98"]},"100":{"X":500,"Y":200,"beforeMoveX":500,"beforeMoveY":200,"dispX":0,"dispY":0,"dispRZ":0,"isSelected":false,"isShow":true,"showEdge":false,"showDeformation":true,"showTag":true,"eleCount":2,"connectType":"beam","massX":568,"massY":568,"massRz":568,"id":100,"elementIDs":["27","110"],"nodeLoadIDs":"","ajacentNodeIDs":["17","18"]},"101":{"X":246.66666666666666,"Y":166.66666666666666,"beforeMoveX":246.66666666666666,"beforeMoveY":166.66666666666666,"dispX":0,"dispY":0,"dispRZ":0,"isSelected":false,"isShow":true,"showEdge":false,"showDeformation":true,"showTag":true,"eleCount":2,"connectType":"beam","massX":535.5155356186117,"massY":535.5155356186117,"massRz":535.5155356186117,"id":101,"elementIDs":["28","111"],"nodeLoadIDs":"","ajacentNodeIDs":["16","102"]},"102":{"X":233.33333333333334,"Y":153.33333333333334,"beforeMoveX":233.33333333333334,"beforeMoveY":153.33333333333334,"dispX":0,"dispY":0,"dispRZ":0,"isSelected":false,"isShow":true,"showEdge":false,"showDeformation":true,"showTag":true,"eleCount":2,"connectType":"beam","massX":535.5155356186117,"massY":535.5155356186117,"massRz":535.5155356186117,"id":102,"elementIDs":["111","112"],"nodeLoadIDs":"","ajacentNodeIDs":["19","101"]},"103":{"X":513.3333333333334,"Y":166.66666666666666,"beforeMoveX":513.3333333333334,"beforeMoveY":166.66666666666666,"dispX":0,"dispY":0,"dispRZ":0,"isSelected":false,"isShow":true,"showEdge":false,"showDeformation":true,"showTag":true,"eleCount":2,"connectType":"beam","massX":535.5155356186115,"massY":535.5155356186115,"massRz":535.5155356186115,"id":103,"elementIDs":["29","113"],"nodeLoadIDs":"","ajacentNodeIDs":["17","104"]},"104":{"X":526.6666666666666,"Y":153.33333333333334,"beforeMoveX":526.6666666666666,"beforeMoveY":153.33333333333334,"dispX":0,"dispY":0,"dispRZ":0,"isSelected":false,"isShow":true,"showEdge":false,"showDeformation":true,"showTag":true,"eleCount":2,"connectType":"beam","massX":535.5155356186115,"massY":535.5155356186115,"massRz":535.5155356186115,"id":104,"elementIDs":["113","114"],"nodeLoadIDs":"","ajacentNodeIDs":["20","103"]}},"theElements":{"1":{"className":"ElasticBeamColumn","A":100,"E":29000,"I":833.3333,"density":0.284,"isSelected":false,"isShow":true,"mass":511.9882811158864,"id":1,"nodeIDs":[1,11],"elementLoadsIDs":"","geomTransfId":3},"2":{"className":"ElasticBeamColumn","A":100,"E":29000,"I":833.3333,"density":0.284,"isSelected":false,"isShow":true,"mass":511.9882811158864,"id":2,"nodeIDs":[1,23],"elementLoadsIDs":"","geomTransfId":3},"3":{"className":"ElasticBeamColumn","A":100,"E":29000,"I":833.3333,"density":0.284,"isSelected":false,"isShow":true,"mass":568,"id":3,"nodeIDs":[2,26],"elementLoadsIDs":"","geomTransfId":3},"4":{"className":"ElasticBeamColumn","A":100,"E":29000,"I":833.3333,"density":0.284,"isSelected":false,"isShow":true,"mass":568,"id":4,"nodeIDs":[2,29],"elementLoadsIDs":"","geomTransfId":3},"5":{"className":"ElasticBeamColumn","A":100,"E":29000,"I":833.3333,"density":0.284,"isSelected":false,"isShow":true,"mass":568,"id":5,"nodeIDs":[4,31],"elementLoadsIDs":"","geomTransfId":3},"6":{"className":"ElasticBeamColumn","A":100,"E":29000,"I":833.3333,"density":0.284,"isSelected":false,"isShow":true,"mass":568,"id":6,"nodeIDs":[3,33],"elementLoadsIDs":"","geomTransfId":3},"7":{"className":"ElasticBeamColumn","A":100,"E":29000,"I":833.3333,"density":0.284,"isSelected":false,"isShow":true,"mass":568,"id":7,"nodeIDs":[2,35],"elementLoadsIDs":"","geomTransfId":3},"8":{"className":"ElasticBeamColumn","A":100,"E":29000,"I":833.3333,"density":0.284,"isSelected":false,"isShow":true,"mass":568,"id":8,"nodeIDs":[7,39],"elementLoadsIDs":"","geomTransfId":3},"9":{"className":"ElasticBeamColumn","A":100,"E":29000,"I":833.3333,"density":0.284,"isSelected":false,"isShow":true,"mass":568,"id":9,"nodeIDs":[4,41],"elementLoadsIDs":"","geomTransfId":3},"10":{"className":"ElasticBeamColumn","A":100,"E":29000,"I":833.3333,"density":0.284,"isSelected":false,"isShow":true,"mass":568,"id":10,"nodeIDs":[8,45],"elementLoadsIDs":"","geomTransfId":3},"11":{"className":"ElasticBeamColumn","A":100,"E":29000,"I":833.3333,"density":0.284,"isSelected":false,"isShow":true,"mass":568,"id":11,"nodeIDs":[7,47],"elementLoadsIDs":"","geomTransfId":3},"12":{"className":"ElasticBeamColumn","A":100,"E":29000,"I":833.3333,"density":0.284,"isSelected":false,"isShow":true,"mass":568,"id":12,"nodeIDs":[9,51],"elementLoadsIDs":"","geomTransfId":3},"13":{"className":"ElasticBeamColumn","A":100,"E":29000,"I":833.3333,"density":0.284,"isSelected":false,"isShow":true,"mass":568,"id":13,"nodeIDs":[3,53],"elementLoadsIDs":"","geomTransfId":3},"14":{"className":"ElasticBeamColumn","A":100,"E":29000,"I":833.3333,"density":0.284,"isSelected":false,"isShow":true,"mass":568,"id":14,"nodeIDs":[10,57],"elementLoadsIDs":"","geomTransfId":3},"15":{"className":"ElasticBeamColumn","A":100,"E":29000,"I":833.3333,"density":0.284,"isSelected":false,"isShow":true,"mass":568,"id":15,"nodeIDs":[9,59],"elementLoadsIDs":"","geomTransfId":3},"16":{"className":"ElasticBeamColumn","A":100,"E":29000,"I":833.3333,"density":0.284,"isSelected":false,"isShow":true,"mass":568,"id":16,"nodeIDs":[13,63],"elementLoadsIDs":"","geomTransfId":3},"17":{"className":"ElasticBeamColumn","A":100,"E":29000,"I":833.3333,"density":0.284,"isSelected":false,"isShow":true,"mass":568,"id":17,"nodeIDs":[13,65],"elementLoadsIDs":"","geomTransfId":3},"18":{"className":"ElasticBeamColumn","A":100,"E":29000,"I":833.3333,"density":0.284,"isSelected":false,"isShow":true,"mass":568,"id":18,"nodeIDs":[13,69],"elementLoadsIDs":"","geomTransfId":3},"19":{"className":"ElasticBeamColumn","A":100,"E":29000,"I":833.3333,"density":0.284,"isSelected":false,"isShow":true,"mass":568,"id":19,"nodeIDs":[10,73],"elementLoadsIDs":"","geomTransfId":3},"20":{"className":"ElasticBeamColumn","A":100,"E":29000,"I":833.3333,"density":0.284,"isSelected":false,"isShow":true,"mass":635.0433056099403,"id":20,"nodeIDs":[5,77],"elementLoadsIDs":"","geomTransfId":3},"21":{"className":"ElasticBeamColumn","A":100,"E":29000,"I":833.3333,"density":0.284,"isSelected":false,"isShow":true,"mass":635.0433056099403,"id":21,"nodeIDs":[6,78],"elementLoadsIDs":"","geomTransfId":3},"22":{"className":"ElasticBeamColumn","A":100,"E":29000,"I":833.3333,"density":0.284,"isSelected":false,"isShow":true,"mass":568,"id":22,"nodeIDs":[12,79],"elementLoadsIDs":"","geomTransfId":3},"23":{"className":"ElasticBeamColumn","A":100,"E":29000,"I":833.3333,"density":0.284,"isSelected":false,"isShow":true,"mass":568,"id":23,"nodeIDs":[12,84],"elementLoadsIDs":"","geomTransfId":3},"24":{"className":"ElasticBeamColumn","A":100,"E":29000,"I":833.3333,"density":0.284,"isSelected":false,"isShow":true,"mass":568,"id":24,"nodeIDs":[15,86],"elementLoadsIDs":"","geomTransfId":3},"25":{"className":"ElasticBeamColumn","A":100,"E":29000,"I":833.3333,"density":0.284,"isSelected":false,"isShow":true,"mass":568,"id":25,"nodeIDs":[16,87],"elementLoadsIDs":"","geomTransfId":3},"26":{"className":"ElasticBeamColumn","A":100,"E":29000,"I":833.3333,"density":0.284,"isSelected":false,"isShow":true,"mass":568,"id":26,"nodeIDs":[14,98],"elementLoadsIDs":"","geomTransfId":3},"27":{"className":"ElasticBeamColumn","A":100,"E":29000,"I":833.3333,"density":0.284,"isSelected":false,"isShow":true,"mass":568,"id":27,"nodeIDs":[18,100],"elementLoadsIDs":"","geomTransfId":3},"28":{"className":"ElasticBeamColumn","A":100,"E":29000,"I":833.3333,"density":0.284,"isSelected":false,"isShow":true,"mass":535.5155356186124,"id":28,"nodeIDs":[16,101],"elementLoadsIDs":"","geomTransfId":3},"29":{"className":"ElasticBeamColumn","A":100,"E":29000,"I":833.3333,"density":0.284,"isSelected":false,"isShow":true,"mass":535.515535618613,"id":29,"nodeIDs":[17,103],"elementLoadsIDs":"","geomTransfId":3},"30":{"className":"ElasticBeamColumn","A":100,"E":29000,"I":833.3333,"density":0.284,"isSelected":false,"isShow":true,"mass":511.9882811158864,"id":30,"nodeIDs":[11,21],"elementLoadsIDs":"","geomTransfId":3},"31":{"className":"ElasticBeamColumn","A":100,"E":29000,"I":833.3333,"density":0.284,"isSelected":false,"isShow":true,"mass":511.9882811158864,"id":31,"nodeIDs":[21,22],"elementLoadsIDs":"","geomTransfId":3},"32":{"className":"ElasticBeamColumn","A":100,"E":29000,"I":833.3333,"density":0.284,"isSelected":false,"isShow":true,"mass":511.9882811158864,"id":32,"nodeIDs":[22,2],"elementLoadsIDs":"","geomTransfId":3},"33":{"className":"ElasticBeamColumn","A":100,"E":29000,"I":833.3333,"density":0.284,"isSelected":false,"isShow":true,"mass":511.9882811158864,"id":33,"nodeIDs":[23,24],"elementLoadsIDs":"","geomTransfId":3},"34":{"className":"ElasticBeamColumn","A":100,"E":29000,"I":833.3333,"density":0.284,"isSelected":false,"isShow":true,"mass":511.9882811158864,"id":34,"nodeIDs":[24,25],"elementLoadsIDs":"","geomTransfId":3},"35":{"className":"ElasticBeamColumn","A":100,"E":29000,"I":833.3333,"density":0.284,"isSelected":false,"isShow":true,"mass":511.9882811158864,"id":35,"nodeIDs":[25,4],"elementLoadsIDs":"","geomTransfId":3},"36":{"className":"ElasticBeamColumn","A":100,"E":29000,"I":833.3333,"density":0.284,"isSelected":false,"isShow":true,"mass":568,"id":36,"nodeIDs":[26,27],"elementLoadsIDs":"","geomTransfId":3},"37":{"className":"ElasticBeamColumn","A":100,"E":29000,"I":833.3333,"density":0.284,"isSelected":false,"isShow":true,"mass":568,"id":37,"nodeIDs":[27,28],"elementLoadsIDs":"","geomTransfId":3},"38":{"className":"ElasticBeamColumn","A":100,"E":29000,"I":833.3333,"density":0.284,"isSelected":false,"isShow":true,"mass":568,"id":38,"nodeIDs":[28,4],"elementLoadsIDs":"","geomTransfId":3},"39":{"className":"ElasticBeamColumn","A":100,"E":29000,"I":833.3333,"density":0.284,"isSelected":false,"isShow":true,"mass":568,"id":39,"nodeIDs":[29,30],"elementLoadsIDs":"","geomTransfId":3},"40":{"className":"ElasticBeamColumn","A":100,"E":29000,"I":833.3333,"density":0.284,"isSelected":false,"isShow":true,"mass":568,"id":40,"nodeIDs":[30,7],"elementLoadsIDs":"","geomTransfId":3},"41":{"className":"ElasticBeamColumn","A":100,"E":29000,"I":833.3333,"density":0.284,"isSelected":false,"isShow":true,"mass":568,"id":41,"nodeIDs":[31,32],"elementLoadsIDs":"","geomTransfId":3},"42":{"className":"ElasticBeamColumn","A":100,"E":29000,"I":833.3333,"density":0.284,"isSelected":false,"isShow":true,"mass":568,"id":42,"nodeIDs":[32,3],"elementLoadsIDs":"","geomTransfId":3},"43":{"className":"ElasticBeamColumn","A":100,"E":29000,"I":833.3333,"density":0.284,"isSelected":false,"isShow":true,"mass":568,"id":43,"nodeIDs":[33,34],"elementLoadsIDs":"","geomTransfId":3},"44":{"className":"ElasticBeamColumn","A":100,"E":29000,"I":833.3333,"density":0.284,"isSelected":false,"isShow":true,"mass":568,"id":44,"nodeIDs":[34,8],"elementLoadsIDs":"","geomTransfId":3},"45":{"className":"ElasticBeamColumn","A":100,"E":29000,"I":833.3333,"density":0.284,"isSelected":false,"isShow":true,"mass":568,"id":45,"nodeIDs":[35,36],"elementLoadsIDs":"","geomTransfId":3},"46":{"className":"ElasticBeamColumn","A":100,"E":29000,"I":833.3333,"density":0.284,"isSelected":false,"isShow":true,"mass":568,"id":46,"nodeIDs":[36,37],"elementLoadsIDs":"","geomTransfId":3},"47":{"className":"ElasticBeamColumn","A":100,"E":29000,"I":833.3333,"density":0.284,"isSelected":false,"isShow":true,"mass":568,"id":47,"nodeIDs":[37,38],"elementLoadsIDs":"","geomTransfId":3},"48":{"className":"ElasticBeamColumn","A":100,"E":29000,"I":833.3333,"density":0.284,"isSelected":false,"isShow":true,"mass":568,"id":48,"nodeIDs":[38,3],"elementLoadsIDs":"","geomTransfId":3},"49":{"className":"ElasticBeamColumn","A":100,"E":29000,"I":833.3333,"density":0.284,"isSelected":false,"isShow":true,"mass":568,"id":49,"nodeIDs":[39,40],"elementLoadsIDs":"","geomTransfId":3},"50":{"className":"ElasticBeamColumn","A":100,"E":29000,"I":833.3333,"density":0.284,"isSelected":false,"isShow":true,"mass":568,"id":50,"nodeIDs":[40,9],"elementLoadsIDs":"","geomTransfId":3},"51":{"className":"ElasticBeamColumn","A":100,"E":29000,"I":833.3333,"density":0.284,"isSelected":false,"isShow":true,"mass":568,"id":51,"nodeIDs":[41,42],"elementLoadsIDs":"","geomTransfId":3},"52":{"className":"ElasticBeamColumn","A":100,"E":29000,"I":833.3333,"density":0.284,"isSelected":false,"isShow":true,"mass":568,"id":52,"nodeIDs":[42,43],"elementLoadsIDs":"","geomTransfId":3},"53":{"className":"ElasticBeamColumn","A":100,"E":29000,"I":833.3333,"density":0.284,"isSelected":false,"isShow":true,"mass":568,"id":53,"nodeIDs":[43,44],"elementLoadsIDs":"","geomTransfId":3},"54":{"className":"ElasticBeamColumn","A":100,"E":29000,"I":833.3333,"density":0.284,"isSelected":false,"isShow":true,"mass":568,"id":54,"nodeIDs":[44,7],"elementLoadsIDs":"","geomTransfId":3},"55":{"className":"ElasticBeamColumn","A":100,"E":29000,"I":833.3333,"density":0.284,"isSelected":false,"isShow":true,"mass":568,"id":55,"nodeIDs":[45,46],"elementLoadsIDs":"","geomTransfId":3},"56":{"className":"ElasticBeamColumn","A":100,"E":29000,"I":833.3333,"density":0.284,"isSelected":false,"isShow":true,"mass":568,"id":56,"nodeIDs":[46,10],"elementLoadsIDs":"","geomTransfId":3},"57":{"className":"ElasticBeamColumn","A":100,"E":29000,"I":833.3333,"density":0.284,"isSelected":false,"isShow":true,"mass":568,"id":57,"nodeIDs":[47,48],"elementLoadsIDs":"","geomTransfId":3},"58":{"className":"ElasticBeamColumn","A":100,"E":29000,"I":833.3333,"density":0.284,"isSelected":false,"isShow":true,"mass":568,"id":58,"nodeIDs":[48,49],"elementLoadsIDs":"","geomTransfId":3},"59":{"className":"ElasticBeamColumn","A":100,"E":29000,"I":833.3333,"density":0.284,"isSelected":false,"isShow":true,"mass":568,"id":59,"nodeIDs":[49,50],"elementLoadsIDs":"","geomTransfId":3},"60":{"className":"ElasticBeamColumn","A":100,"E":29000,"I":833.3333,"density":0.284,"isSelected":false,"isShow":true,"mass":568,"id":60,"nodeIDs":[50,8],"elementLoadsIDs":"","geomTransfId":3},"61":{"className":"ElasticBeamColumn","A":100,"E":29000,"I":833.3333,"density":0.284,"isSelected":false,"isShow":true,"mass":568,"id":61,"nodeIDs":[51,52],"elementLoadsIDs":"","geomTransfId":3},"62":{"className":"ElasticBeamColumn","A":100,"E":29000,"I":833.3333,"density":0.284,"isSelected":false,"isShow":true,"mass":568,"id":62,"nodeIDs":[52,13],"elementLoadsIDs":"","geomTransfId":3},"63":{"className":"ElasticBeamColumn","A":100,"E":29000,"I":833.3333,"density":0.284,"isSelected":false,"isShow":true,"mass":568,"id":63,"nodeIDs":[53,54],"elementLoadsIDs":"","geomTransfId":3},"64":{"className":"ElasticBeamColumn","A":100,"E":29000,"I":833.3333,"density":0.284,"isSelected":false,"isShow":true,"mass":568,"id":64,"nodeIDs":[54,55],"elementLoadsIDs":"","geomTransfId":3},"65":{"className":"ElasticBeamColumn","A":100,"E":29000,"I":833.3333,"density":0.284,"isSelected":false,"isShow":true,"mass":568,"id":65,"nodeIDs":[55,56],"elementLoadsIDs":"","geomTransfId":3},"66":{"className":"ElasticBeamColumn","A":100,"E":29000,"I":833.3333,"density":0.284,"isSelected":false,"isShow":true,"mass":568,"id":66,"nodeIDs":[56,9],"elementLoadsIDs":"","geomTransfId":3},"67":{"className":"ElasticBeamColumn","A":100,"E":29000,"I":833.3333,"density":0.284,"isSelected":false,"isShow":true,"mass":568,"id":67,"nodeIDs":[57,58],"elementLoadsIDs":"","geomTransfId":3},"68":{"className":"ElasticBeamColumn","A":100,"E":29000,"I":833.3333,"density":0.284,"isSelected":false,"isShow":true,"mass":568,"id":68,"nodeIDs":[58,6],"elementLoadsIDs":"","geomTransfId":3},"69":{"className":"ElasticBeamColumn","A":100,"E":29000,"I":833.3333,"density":0.284,"isSelected":false,"isShow":true,"mass":568,"id":69,"nodeIDs":[59,60],"elementLoadsIDs":"","geomTransfId":3},"70":{"className":"ElasticBeamColumn","A":100,"E":29000,"I":833.3333,"density":0.284,"isSelected":false,"isShow":true,"mass":568,"id":70,"nodeIDs":[60,61],"elementLoadsIDs":"","geomTransfId":3},"71":{"className":"ElasticBeamColumn","A":100,"E":29000,"I":833.3333,"density":0.284,"isSelected":false,"isShow":true,"mass":568,"id":71,"nodeIDs":[61,62],"elementLoadsIDs":"","geomTransfId":3},"72":{"className":"ElasticBeamColumn","A":100,"E":29000,"I":833.3333,"density":0.284,"isSelected":false,"isShow":true,"mass":568,"id":72,"nodeIDs":[62,10],"elementLoadsIDs":"","geomTransfId":3},"73":{"className":"ElasticBeamColumn","A":100,"E":29000,"I":833.3333,"density":0.284,"isSelected":false,"isShow":true,"mass":568,"id":73,"nodeIDs":[63,64],"elementLoadsIDs":"","geomTransfId":3},"74":{"className":"ElasticBeamColumn","A":100,"E":29000,"I":833.3333,"density":0.284,"isSelected":false,"isShow":true,"mass":568,"id":74,"nodeIDs":[64,5],"elementLoadsIDs":"","geomTransfId":3},"75":{"className":"ElasticBeamColumn","A":100,"E":29000,"I":833.3333,"density":0.284,"isSelected":false,"isShow":true,"mass":568,"id":75,"nodeIDs":[65,66],"elementLoadsIDs":"","geomTransfId":3},"76":{"className":"ElasticBeamColumn","A":100,"E":29000,"I":833.3333,"density":0.284,"isSelected":false,"isShow":true,"mass":568,"id":76,"nodeIDs":[66,67],"elementLoadsIDs":"","geomTransfId":3},"77":{"className":"ElasticBeamColumn","A":100,"E":29000,"I":833.3333,"density":0.284,"isSelected":false,"isShow":true,"mass":568,"id":77,"nodeIDs":[67,68],"elementLoadsIDs":"","geomTransfId":3},"78":{"className":"ElasticBeamColumn","A":100,"E":29000,"I":833.3333,"density":0.284,"isSelected":false,"isShow":true,"mass":568,"id":78,"nodeIDs":[68,8],"elementLoadsIDs":"","geomTransfId":3},"79":{"className":"ElasticBeamColumn","A":100,"E":29000,"I":833.3333,"density":0.284,"isSelected":false,"isShow":true,"mass":568,"id":79,"nodeIDs":[69,70],"elementLoadsIDs":"","geomTransfId":3},"80":{"className":"ElasticBeamColumn","A":100,"E":29000,"I":833.3333,"density":0.284,"isSelected":false,"isShow":true,"mass":568,"id":80,"nodeIDs":[70,71],"elementLoadsIDs":"","geomTransfId":3},"81":{"className":"ElasticBeamColumn","A":100,"E":29000,"I":833.3333,"density":0.284,"isSelected":false,"isShow":true,"mass":568,"id":81,"nodeIDs":[71,72],"elementLoadsIDs":"","geomTransfId":3},"82":{"className":"ElasticBeamColumn","A":100,"E":29000,"I":833.3333,"density":0.284,"isSelected":false,"isShow":true,"mass":568,"id":82,"nodeIDs":[72,6],"elementLoadsIDs":"","geomTransfId":3},"83":{"className":"ElasticBeamColumn","A":100,"E":29000,"I":833.3333,"density":0.284,"isSelected":false,"isShow":true,"mass":568,"id":83,"nodeIDs":[73,74],"elementLoadsIDs":"","geomTransfId":3},"84":{"className":"ElasticBeamColumn","A":100,"E":29000,"I":833.3333,"density":0.284,"isSelected":false,"isShow":true,"mass":568,"id":84,"nodeIDs":[74,75],"elementLoadsIDs":"","geomTransfId":3},"85":{"className":"ElasticBeamColumn","A":100,"E":29000,"I":833.3333,"density":0.284,"isSelected":false,"isShow":true,"mass":568,"id":85,"nodeIDs":[75,76],"elementLoadsIDs":"","geomTransfId":3},"86":{"className":"ElasticBeamColumn","A":100,"E":29000,"I":833.3333,"density":0.284,"isSelected":false,"isShow":true,"mass":568,"id":86,"nodeIDs":[76,5],"elementLoadsIDs":"","geomTransfId":3},"87":{"className":"ElasticBeamColumn","A":100,"E":29000,"I":833.3333,"density":0.284,"isSelected":false,"isShow":true,"mass":635.0433056099403,"id":87,"nodeIDs":[77,12],"elementLoadsIDs":"","geomTransfId":3},"88":{"className":"ElasticBeamColumn","A":100,"E":29000,"I":833.3333,"density":0.284,"isSelected":false,"isShow":true,"mass":635.0433056099403,"id":88,"nodeIDs":[78,14],"elementLoadsIDs":"","geomTransfId":3},"89":{"className":"ElasticBeamColumn","A":100,"E":29000,"I":833.3333,"density":0.284,"isSelected":false,"isShow":true,"mass":568,"id":89,"nodeIDs":[79,80],"elementLoadsIDs":"","geomTransfId":3},"90":{"className":"ElasticBeamColumn","A":100,"E":29000,"I":833.3333,"density":0.284,"isSelected":false,"isShow":true,"mass":568,"id":90,"nodeIDs":[80,81],"elementLoadsIDs":"","geomTransfId":3},"91":{"className":"ElasticBeamColumn","A":100,"E":29000,"I":833.3333,"density":0.284,"isSelected":false,"isShow":true,"mass":568,"id":91,"nodeIDs":[81,82],"elementLoadsIDs":"","geomTransfId":3},"92":{"className":"ElasticBeamColumn","A":100,"E":29000,"I":833.3333,"density":0.284,"isSelected":false,"isShow":true,"mass":568,"id":92,"nodeIDs":[82,83],"elementLoadsIDs":"","geomTransfId":3},"93":{"className":"ElasticBeamColumn","A":100,"E":29000,"I":833.3333,"density":0.284,"isSelected":false,"isShow":true,"mass":568,"id":93,"nodeIDs":[83,14],"elementLoadsIDs":"","geomTransfId":3},"94":{"className":"ElasticBeamColumn","A":100,"E":29000,"I":833.3333,"density":0.284,"isSelected":false,"isShow":true,"mass":568,"id":94,"nodeIDs":[84,85],"elementLoadsIDs":"","geomTransfId":3},"95":{"className":"ElasticBeamColumn","A":100,"E":29000,"I":833.3333,"density":0.284,"isSelected":false,"isShow":true,"mass":568,"id":95,"nodeIDs":[85,15],"elementLoadsIDs":"","geomTransfId":3},"96":{"className":"ElasticBeamColumn","A":100,"E":29000,"I":833.3333,"density":0.284,"isSelected":false,"isShow":true,"mass":568,"id":96,"nodeIDs":[86,16],"elementLoadsIDs":"","geomTransfId":3},"97":{"className":"ElasticBeamColumn","A":100,"E":29000,"I":833.3333,"density":0.284,"isSelected":false,"isShow":true,"mass":568,"id":97,"nodeIDs":[87,88],"elementLoadsIDs":"","geomTransfId":3},"98":{"className":"ElasticBeamColumn","A":100,"E":29000,"I":833.3333,"density":0.284,"isSelected":false,"isShow":true,"mass":568,"id":98,"nodeIDs":[88,89],"elementLoadsIDs":"","geomTransfId":3},"99":{"className":"ElasticBeamColumn","A":100,"E":29000,"I":833.3333,"density":0.284,"isSelected":false,"isShow":true,"mass":568,"id":99,"nodeIDs":[89,90],"elementLoadsIDs":"","geomTransfId":3},"100":{"className":"ElasticBeamColumn","A":100,"E":29000,"I":833.3333,"density":0.284,"isSelected":false,"isShow":true,"mass":568,"id":100,"nodeIDs":[90,91],"elementLoadsIDs":"","geomTransfId":3},"101":{"className":"ElasticBeamColumn","A":100,"E":29000,"I":833.3333,"density":0.284,"isSelected":false,"isShow":true,"mass":568,"id":101,"nodeIDs":[91,92],"elementLoadsIDs":"","geomTransfId":3},"102":{"className":"ElasticBeamColumn","A":100,"E":29000,"I":833.3333,"density":0.284,"isSelected":false,"isShow":true,"mass":568,"id":102,"nodeIDs":[92,93],"elementLoadsIDs":"","geomTransfId":3},"103":{"className":"ElasticBeamColumn","A":100,"E":29000,"I":833.3333,"density":0.284,"isSelected":false,"isShow":true,"mass":568,"id":103,"nodeIDs":[93,94],"elementLoadsIDs":"","geomTransfId":3},"104":{"className":"ElasticBeamColumn","A":100,"E":29000,"I":833.3333,"density":0.284,"isSelected":false,"isShow":true,"mass":568,"id":104,"nodeIDs":[94,95],"elementLoadsIDs":"","geomTransfId":3},"105":{"className":"ElasticBeamColumn","A":100,"E":29000,"I":833.3333,"density":0.284,"isSelected":false,"isShow":true,"mass":568,"id":105,"nodeIDs":[95,96],"elementLoadsIDs":"","geomTransfId":3},"106":{"className":"ElasticBeamColumn","A":100,"E":29000,"I":833.3333,"density":0.284,"isSelected":false,"isShow":true,"mass":568,"id":106,"nodeIDs":[96,97],"elementLoadsIDs":"","geomTransfId":3},"107":{"className":"ElasticBeamColumn","A":100,"E":29000,"I":833.3333,"density":0.284,"isSelected":false,"isShow":true,"mass":568,"id":107,"nodeIDs":[97,17],"elementLoadsIDs":"","geomTransfId":3},"108":{"className":"ElasticBeamColumn","A":100,"E":29000,"I":833.3333,"density":0.284,"isSelected":false,"isShow":true,"mass":568,"id":108,"nodeIDs":[98,99],"elementLoadsIDs":"","geomTransfId":3},"109":{"className":"ElasticBeamColumn","A":100,"E":29000,"I":833.3333,"density":0.284,"isSelected":false,"isShow":true,"mass":568,"id":109,"nodeIDs":[99,18],"elementLoadsIDs":"","geomTransfId":3},"110":{"className":"ElasticBeamColumn","A":100,"E":29000,"I":833.3333,"density":0.284,"isSelected":false,"isShow":true,"mass":568,"id":110,"nodeIDs":[100,17],"elementLoadsIDs":"","geomTransfId":3},"111":{"className":"ElasticBeamColumn","A":100,"E":29000,"I":833.3333,"density":0.284,"isSelected":false,"isShow":true,"mass":535.5155356186111,"id":111,"nodeIDs":[101,102],"elementLoadsIDs":"","geomTransfId":3},"112":{"className":"ElasticBeamColumn","A":100,"E":29000,"I":833.3333,"density":0.284,"isSelected":false,"isShow":true,"mass":535.5155356186124,"id":112,"nodeIDs":[102,19],"elementLoadsIDs":"","geomTransfId":3},"113":{"className":"ElasticBeamColumn","A":100,"E":29000,"I":833.3333,"density":0.284,"isSelected":false,"isShow":true,"mass":535.5155356186101,"id":113,"nodeIDs":[103,104],"elementLoadsIDs":"","geomTransfId":3},"114":{"className":"ElasticBeamColumn","A":100,"E":29000,"I":833.3333,"density":0.284,"isSelected":false,"isShow":true,"mass":535.515535618613,"id":114,"nodeIDs":[104,20],"elementLoadsIDs":"","geomTransfId":3}},"theSPCs":{"1":{"X":1,"Y":1,"RZ":1,"angle":-1.5707963267948966,"direction":"up","isSelected":false,"isShow":true,"id":1,"nodeID":19},"2":{"X":1,"Y":1,"RZ":1,"angle":-1.5707963267948966,"direction":"up","isSelected":false,"isShow":true,"id":2,"nodeID":20}}}';
			var testmodel1=eval('('+str+')')
			
			var str = '{"theNodes":{"1":{"X":280,"Y":500,"beforeMoveX":280,"beforeMoveY":500,"dispX":0,"dispY":0,"dispRZ":0,"isSelected":false,"isShow":true,"showEdge":false,"showDeformation":true,"showTag":true,"eleCount":2,"connectType":"beam","massX":284,"massY":284,"massRz":284,"id":1,"elementIDs":["1","2"],"nodeLoadIDs":"","ajacentNodeIDs":["8","16"]},"2":{"X":280,"Y":280,"beforeMoveX":280,"beforeMoveY":280,"dispX":0,"dispY":0,"dispRZ":0,"isSelected":false,"isShow":true,"showEdge":false,"showDeformation":true,"showTag":true,"eleCount":1,"connectType":"beam","massX":142,"massY":142,"massRz":142,"id":2,"elementIDs":["88"],"nodeLoadIDs":"","ajacentNodeIDs":["87"]},"3":{"X":520,"Y":420,"beforeMoveX":520,"beforeMoveY":420,"dispX":0,"dispY":0,"dispRZ":0,"isSelected":false,"isShow":true,"showEdge":false,"showDeformation":true,"showTag":true,"eleCount":3,"connectType":"beam","massX":426.00000000000045,"massY":426.00000000000045,"massRz":426.00000000000045,"id":3,"elementIDs":["5","46","83"],"nodeLoadIDs":"","ajacentNodeIDs":["45","53","82"]},"4":{"X":520,"Y":500,"beforeMoveX":520,"beforeMoveY":500,"dispX":0,"dispY":0,"dispRZ":0,"isSelected":false,"isShow":true,"showEdge":false,"showDeformation":true,"showTag":true,"eleCount":2,"connectType":"beam","massX":284.00000000000045,"massY":284.00000000000045,"massRz":284.00000000000045,"id":4,"elementIDs":["3","39"],"nodeLoadIDs":"","ajacentNodeIDs":["38","39"]},"5":{"X":520,"Y":280,"beforeMoveX":520,"beforeMoveY":280,"dispX":0,"dispY":0,"dispRZ":0,"isSelected":false,"isShow":true,"showEdge":false,"showDeformation":true,"showTag":true,"eleCount":1,"connectType":"beam","massX":142,"massY":142,"massRz":142,"id":5,"elementIDs":["93"],"nodeLoadIDs":"","ajacentNodeIDs":["92"]},"6":{"X":520,"Y":340,"beforeMoveX":520,"beforeMoveY":340,"dispX":0,"dispY":0,"dispRZ":0,"isSelected":false,"isShow":true,"showEdge":false,"showDeformation":true,"showTag":true,"eleCount":3,"connectType":"beam","massX":425.99999999999955,"massY":425.99999999999955,"massRz":425.99999999999955,"id":6,"elementIDs":["8","60","116"],"nodeLoadIDs":"","ajacentNodeIDs":["59","88","115"]},"7":{"X":280,"Y":420,"beforeMoveX":280,"beforeMoveY":420,"dispX":0,"dispY":0,"dispRZ":0,"isSelected":false,"isShow":true,"showEdge":false,"showDeformation":true,"showTag":true,"eleCount":3,"connectType":"beam","massX":426,"massY":426,"massRz":426,"id":7,"elementIDs":["4","6","16"],"nodeLoadIDs":"","ajacentNodeIDs":["15","46","60"]},"8":{"X":280,"Y":490,"beforeMoveX":280,"beforeMoveY":490,"dispX":0,"dispY":0,"dispRZ":0,"isSelected":false,"isShow":true,"showEdge":false,"showDeformation":true,"showTag":true,"eleCount":2,"connectType":"beam","massX":284,"massY":284,"massRz":284,"id":8,"elementIDs":["1","10"],"nodeLoadIDs":"","ajacentNodeIDs":["1","10"]},"9":{"X":280,"Y":340,"beforeMoveX":280,"beforeMoveY":340,"dispX":0,"dispY":0,"dispRZ":0,"isSelected":false,"isShow":true,"showEdge":false,"showDeformation":true,"showTag":true,"eleCount":3,"connectType":"beam","massX":426,"massY":426,"massRz":426,"id":9,"elementIDs":["7","9","53"],"nodeLoadIDs":"","ajacentNodeIDs":["52","83","93"]},"10":{"X":280,"Y":480,"beforeMoveX":280,"beforeMoveY":480,"dispX":0,"dispY":0,"dispRZ":0,"isSelected":false,"isShow":true,"showEdge":false,"showDeformation":true,"showTag":true,"eleCount":2,"connectType":"beam","massX":284,"massY":284,"massRz":284,"id":10,"elementIDs":["10","11"],"nodeLoadIDs":"","ajacentNodeIDs":["8","11"]},"11":{"X":280,"Y":470,"beforeMoveX":280,"beforeMoveY":470,"dispX":0,"dispY":0,"dispRZ":0,"isSelected":false,"isShow":true,"showEdge":false,"showDeformation":true,"showTag":true,"eleCount":2,"connectType":"beam","massX":284,"massY":284,"massRz":284,"id":11,"elementIDs":["11","12"],"nodeLoadIDs":"","ajacentNodeIDs":["10","12"]},"12":{"X":280,"Y":460,"beforeMoveX":280,"beforeMoveY":460,"dispX":0,"dispY":0,"dispRZ":0,"isSelected":false,"isShow":true,"showEdge":false,"showDeformation":true,"showTag":true,"eleCount":2,"connectType":"beam","massX":284,"massY":284,"massRz":284,"id":12,"elementIDs":["12","13"],"nodeLoadIDs":"","ajacentNodeIDs":["11","13"]},"13":{"X":280,"Y":450,"beforeMoveX":280,"beforeMoveY":450,"dispX":0,"dispY":0,"dispRZ":0,"isSelected":false,"isShow":true,"showEdge":false,"showDeformation":true,"showTag":true,"eleCount":2,"connectType":"beam","massX":284,"massY":284,"massRz":284,"id":13,"elementIDs":["13","14"],"nodeLoadIDs":"","ajacentNodeIDs":["12","14"]},"14":{"X":280,"Y":440,"beforeMoveX":280,"beforeMoveY":440,"dispX":0,"dispY":0,"dispRZ":0,"isSelected":false,"isShow":true,"showEdge":false,"showDeformation":true,"showTag":true,"eleCount":2,"connectType":"beam","massX":284,"massY":284,"massRz":284,"id":14,"elementIDs":["14","15"],"nodeLoadIDs":"","ajacentNodeIDs":["13","15"]},"15":{"X":280,"Y":430,"beforeMoveX":280,"beforeMoveY":430,"dispX":0,"dispY":0,"dispRZ":0,"isSelected":false,"isShow":true,"showEdge":false,"showDeformation":true,"showTag":true,"eleCount":2,"connectType":"beam","massX":284,"massY":284,"massRz":284,"id":15,"elementIDs":["15","16"],"nodeLoadIDs":"","ajacentNodeIDs":["7","14"]},"16":{"X":290,"Y":500,"beforeMoveX":290,"beforeMoveY":500,"dispX":0,"dispY":0,"dispRZ":0,"isSelected":false,"isShow":true,"showEdge":false,"showDeformation":true,"showTag":true,"eleCount":2,"connectType":"beam","massX":284,"massY":284,"massRz":284,"id":16,"elementIDs":["2","17"],"nodeLoadIDs":"","ajacentNodeIDs":["1","17"]},"17":{"X":300,"Y":500,"beforeMoveX":300,"beforeMoveY":500,"dispX":0,"dispY":0,"dispRZ":0,"isSelected":false,"isShow":true,"showEdge":false,"showDeformation":true,"showTag":true,"eleCount":2,"connectType":"beam","massX":284,"massY":284,"massRz":284,"id":17,"elementIDs":["17","18"],"nodeLoadIDs":"","ajacentNodeIDs":["16","18"]},"18":{"X":310,"Y":500,"beforeMoveX":310,"beforeMoveY":500,"dispX":0,"dispY":0,"dispRZ":0,"isSelected":false,"isShow":true,"showEdge":false,"showDeformation":true,"showTag":true,"eleCount":2,"connectType":"beam","massX":284,"massY":284,"massRz":284,"id":18,"elementIDs":["18","19"],"nodeLoadIDs":"","ajacentNodeIDs":["17","19"]},"19":{"X":320,"Y":500,"beforeMoveX":320,"beforeMoveY":500,"dispX":0,"dispY":0,"dispRZ":0,"isSelected":false,"isShow":true,"showEdge":false,"showDeformation":true,"showTag":true,"eleCount":2,"connectType":"beam","massX":284,"massY":284,"massRz":284,"id":19,"elementIDs":["19","20"],"nodeLoadIDs":"","ajacentNodeIDs":["18","20"]},"20":{"X":330,"Y":500,"beforeMoveX":330,"beforeMoveY":500,"dispX":0,"dispY":0,"dispRZ":0,"isSelected":false,"isShow":true,"showEdge":false,"showDeformation":true,"showTag":true,"eleCount":2,"connectType":"beam","massX":284,"massY":284,"massRz":284,"id":20,"elementIDs":["20","21"],"nodeLoadIDs":"","ajacentNodeIDs":["19","21"]},"21":{"X":340,"Y":500,"beforeMoveX":340,"beforeMoveY":500,"dispX":0,"dispY":0,"dispRZ":0,"isSelected":false,"isShow":true,"showEdge":false,"showDeformation":true,"showTag":true,"eleCount":2,"connectType":"beam","massX":284,"massY":284,"massRz":284,"id":21,"elementIDs":["21","22"],"nodeLoadIDs":"","ajacentNodeIDs":["20","22"]},"22":{"X":350,"Y":500,"beforeMoveX":350,"beforeMoveY":500,"dispX":0,"dispY":0,"dispRZ":0,"isSelected":false,"isShow":true,"showEdge":false,"showDeformation":true,"showTag":true,"eleCount":2,"connectType":"beam","massX":284,"massY":284,"massRz":284,"id":22,"elementIDs":["22","23"],"nodeLoadIDs":"","ajacentNodeIDs":["21","23"]},"23":{"X":360,"Y":500,"beforeMoveX":360,"beforeMoveY":500,"dispX":0,"dispY":0,"dispRZ":0,"isSelected":false,"isShow":true,"showEdge":false,"showDeformation":true,"showTag":true,"eleCount":2,"connectType":"beam","massX":284,"massY":284,"massRz":284,"id":23,"elementIDs":["23","24"],"nodeLoadIDs":"","ajacentNodeIDs":["22","24"]},"24":{"X":370,"Y":500,"beforeMoveX":370,"beforeMoveY":500,"dispX":0,"dispY":0,"dispRZ":0,"isSelected":false,"isShow":true,"showEdge":false,"showDeformation":true,"showTag":true,"eleCount":2,"connectType":"beam","massX":284,"massY":284,"massRz":284,"id":24,"elementIDs":["24","25"],"nodeLoadIDs":"","ajacentNodeIDs":["23","25"]},"25":{"X":380,"Y":500,"beforeMoveX":380,"beforeMoveY":500,"dispX":0,"dispY":0,"dispRZ":0,"isSelected":false,"isShow":true,"showEdge":false,"showDeformation":true,"showTag":true,"eleCount":2,"connectType":"beam","massX":284,"massY":284,"massRz":284,"id":25,"elementIDs":["25","26"],"nodeLoadIDs":"","ajacentNodeIDs":["24","26"]},"26":{"X":390,"Y":500,"beforeMoveX":390,"beforeMoveY":500,"dispX":0,"dispY":0,"dispRZ":0,"isSelected":false,"isShow":true,"showEdge":false,"showDeformation":true,"showTag":true,"eleCount":2,"connectType":"beam","massX":284,"massY":284,"massRz":284,"id":26,"elementIDs":["26","27"],"nodeLoadIDs":"","ajacentNodeIDs":["25","27"]},"27":{"X":400,"Y":500,"beforeMoveX":400,"beforeMoveY":500,"dispX":0,"dispY":0,"dispRZ":0,"isSelected":false,"isShow":true,"showEdge":false,"showDeformation":true,"showTag":true,"eleCount":2,"connectType":"beam","massX":284,"massY":284,"massRz":284,"id":27,"elementIDs":["27","28"],"nodeLoadIDs":"","ajacentNodeIDs":["26","28"]},"28":{"X":410,"Y":500,"beforeMoveX":410,"beforeMoveY":500,"dispX":0,"dispY":0,"dispRZ":0,"isSelected":false,"isShow":true,"showEdge":false,"showDeformation":true,"showTag":true,"eleCount":2,"connectType":"beam","massX":284,"massY":284,"massRz":284,"id":28,"elementIDs":["28","29"],"nodeLoadIDs":"","ajacentNodeIDs":["27","29"]},"29":{"X":420,"Y":500,"beforeMoveX":420,"beforeMoveY":500,"dispX":0,"dispY":0,"dispRZ":0,"isSelected":false,"isShow":true,"showEdge":false,"showDeformation":true,"showTag":true,"eleCount":2,"connectType":"beam","massX":284,"massY":284,"massRz":284,"id":29,"elementIDs":["29","30"],"nodeLoadIDs":"","ajacentNodeIDs":["28","30"]},"30":{"X":430,"Y":500,"beforeMoveX":430,"beforeMoveY":500,"dispX":0,"dispY":0,"dispRZ":0,"isSelected":false,"isShow":true,"showEdge":false,"showDeformation":true,"showTag":true,"eleCount":2,"connectType":"beam","massX":284,"massY":284,"massRz":284,"id":30,"elementIDs":["30","31"],"nodeLoadIDs":"","ajacentNodeIDs":["29","31"]},"31":{"X":440,"Y":500,"beforeMoveX":440,"beforeMoveY":500,"dispX":0,"dispY":0,"dispRZ":0,"isSelected":false,"isShow":true,"showEdge":false,"showDeformation":true,"showTag":true,"eleCount":2,"connectType":"beam","massX":284,"massY":284,"massRz":284,"id":31,"elementIDs":["31","32"],"nodeLoadIDs":"","ajacentNodeIDs":["30","32"]},"32":{"X":450,"Y":500,"beforeMoveX":450,"beforeMoveY":500,"dispX":0,"dispY":0,"dispRZ":0,"isSelected":false,"isShow":true,"showEdge":false,"showDeformation":true,"showTag":true,"eleCount":2,"connectType":"beam","massX":284,"massY":284,"massRz":284,"id":32,"elementIDs":["32","33"],"nodeLoadIDs":"","ajacentNodeIDs":["31","33"]},"33":{"X":460,"Y":500,"beforeMoveX":460,"beforeMoveY":500,"dispX":0,"dispY":0,"dispRZ":0,"isSelected":false,"isShow":true,"showEdge":false,"showDeformation":true,"showTag":true,"eleCount":2,"connectType":"beam","massX":284,"massY":284,"massRz":284,"id":33,"elementIDs":["33","34"],"nodeLoadIDs":"","ajacentNodeIDs":["32","34"]},"34":{"X":470,"Y":500,"beforeMoveX":470,"beforeMoveY":500,"dispX":0,"dispY":0,"dispRZ":0,"isSelected":false,"isShow":true,"showEdge":false,"showDeformation":true,"showTag":true,"eleCount":2,"connectType":"beam","massX":284,"massY":284,"massRz":284,"id":34,"elementIDs":["34","35"],"nodeLoadIDs":"","ajacentNodeIDs":["33","35"]},"35":{"X":480,"Y":500,"beforeMoveX":480,"beforeMoveY":500,"dispX":0,"dispY":0,"dispRZ":0,"isSelected":false,"isShow":true,"showEdge":false,"showDeformation":true,"showTag":true,"eleCount":2,"connectType":"beam","massX":284,"massY":284,"massRz":284,"id":35,"elementIDs":["35","36"],"nodeLoadIDs":"","ajacentNodeIDs":["34","36"]},"36":{"X":490,"Y":500,"beforeMoveX":490,"beforeMoveY":500,"dispX":0,"dispY":0,"dispRZ":0,"isSelected":false,"isShow":true,"showEdge":false,"showDeformation":true,"showTag":true,"eleCount":2,"connectType":"beam","massX":284,"massY":284,"massRz":284,"id":36,"elementIDs":["36","37"],"nodeLoadIDs":"","ajacentNodeIDs":["35","37"]},"37":{"X":500,"Y":500,"beforeMoveX":500,"beforeMoveY":500,"dispX":0,"dispY":0,"dispRZ":0,"isSelected":false,"isShow":true,"showEdge":false,"showDeformation":true,"showTag":true,"eleCount":2,"connectType":"beam","massX":284,"massY":284,"massRz":284,"id":37,"elementIDs":["37","38"],"nodeLoadIDs":"","ajacentNodeIDs":["36","38"]},"38":{"X":510,"Y":500,"beforeMoveX":510,"beforeMoveY":500,"dispX":0,"dispY":0,"dispRZ":0,"isSelected":false,"isShow":true,"showEdge":false,"showDeformation":true,"showTag":true,"eleCount":2,"connectType":"beam","massX":284,"massY":284,"massRz":284,"id":38,"elementIDs":["38","39"],"nodeLoadIDs":"","ajacentNodeIDs":["4","37"]},"39":{"X":520,"Y":490,"beforeMoveX":520,"beforeMoveY":490,"dispX":0,"dispY":0,"dispRZ":0,"isSelected":false,"isShow":true,"showEdge":false,"showDeformation":true,"showTag":true,"eleCount":2,"connectType":"beam","massX":284,"massY":284,"massRz":284,"id":39,"elementIDs":["3","40"],"nodeLoadIDs":"","ajacentNodeIDs":["4","40"]},"40":{"X":520,"Y":480,"beforeMoveX":520,"beforeMoveY":480,"dispX":0,"dispY":0,"dispRZ":0,"isSelected":false,"isShow":true,"showEdge":false,"showDeformation":true,"showTag":true,"eleCount":2,"connectType":"beam","massX":284,"massY":284,"massRz":284,"id":40,"elementIDs":["40","41"],"nodeLoadIDs":"","ajacentNodeIDs":["39","41"]},"41":{"X":520,"Y":470,"beforeMoveX":520,"beforeMoveY":470,"dispX":0,"dispY":0,"dispRZ":0,"isSelected":false,"isShow":true,"showEdge":false,"showDeformation":true,"showTag":true,"eleCount":2,"connectType":"beam","massX":284,"massY":284,"massRz":284,"id":41,"elementIDs":["41","42"],"nodeLoadIDs":"","ajacentNodeIDs":["40","42"]},"42":{"X":520,"Y":460,"beforeMoveX":520,"beforeMoveY":460,"dispX":0,"dispY":0,"dispRZ":0,"isSelected":false,"isShow":true,"showEdge":false,"showDeformation":true,"showTag":true,"eleCount":2,"connectType":"beam","massX":284,"massY":284,"massRz":284,"id":42,"elementIDs":["42","43"],"nodeLoadIDs":"","ajacentNodeIDs":["41","43"]},"43":{"X":520,"Y":450,"beforeMoveX":520,"beforeMoveY":450,"dispX":0,"dispY":0,"dispRZ":0,"isSelected":false,"isShow":true,"showEdge":false,"showDeformation":true,"showTag":true,"eleCount":2,"connectType":"beam","massX":284,"massY":284,"massRz":284,"id":43,"elementIDs":["43","44"],"nodeLoadIDs":"","ajacentNodeIDs":["42","44"]},"44":{"X":520,"Y":440,"beforeMoveX":520,"beforeMoveY":440,"dispX":0,"dispY":0,"dispRZ":0,"isSelected":false,"isShow":true,"showEdge":false,"showDeformation":true,"showTag":true,"eleCount":2,"connectType":"beam","massX":284,"massY":284,"massRz":284,"id":44,"elementIDs":["44","45"],"nodeLoadIDs":"","ajacentNodeIDs":["43","45"]},"45":{"X":520,"Y":430,"beforeMoveX":520,"beforeMoveY":430,"dispX":0,"dispY":0,"dispRZ":0,"isSelected":false,"isShow":true,"showEdge":false,"showDeformation":true,"showTag":true,"eleCount":2,"connectType":"beam","massX":284,"massY":284,"massRz":284,"id":45,"elementIDs":["45","46"],"nodeLoadIDs":"","ajacentNodeIDs":["3","44"]},"46":{"X":280,"Y":410,"beforeMoveX":280,"beforeMoveY":410,"dispX":0,"dispY":0,"dispRZ":0,"isSelected":false,"isShow":true,"showEdge":false,"showDeformation":true,"showTag":true,"eleCount":2,"connectType":"beam","massX":284,"massY":284,"massRz":284,"id":46,"elementIDs":["4","47"],"nodeLoadIDs":"","ajacentNodeIDs":["7","47"]},"47":{"X":280,"Y":400,"beforeMoveX":280,"beforeMoveY":400,"dispX":0,"dispY":0,"dispRZ":0,"isSelected":false,"isShow":true,"showEdge":false,"showDeformation":true,"showTag":true,"eleCount":2,"connectType":"beam","massX":284,"massY":284,"massRz":284,"id":47,"elementIDs":["47","48"],"nodeLoadIDs":"","ajacentNodeIDs":["46","48"]},"48":{"X":280,"Y":390,"beforeMoveX":280,"beforeMoveY":390,"dispX":0,"dispY":0,"dispRZ":0,"isSelected":false,"isShow":true,"showEdge":false,"showDeformation":true,"showTag":true,"eleCount":2,"connectType":"beam","massX":284,"massY":284,"massRz":284,"id":48,"elementIDs":["48","49"],"nodeLoadIDs":"","ajacentNodeIDs":["47","49"]},"49":{"X":280,"Y":380,"beforeMoveX":280,"beforeMoveY":380,"dispX":0,"dispY":0,"dispRZ":0,"isSelected":false,"isShow":true,"showEdge":false,"showDeformation":true,"showTag":true,"eleCount":2,"connectType":"beam","massX":284,"massY":284,"massRz":284,"id":49,"elementIDs":["49","50"],"nodeLoadIDs":"","ajacentNodeIDs":["48","50"]},"50":{"X":280,"Y":370,"beforeMoveX":280,"beforeMoveY":370,"dispX":0,"dispY":0,"dispRZ":0,"isSelected":false,"isShow":true,"showEdge":false,"showDeformation":true,"showTag":true,"eleCount":2,"connectType":"beam","massX":284,"massY":284,"massRz":284,"id":50,"elementIDs":["50","51"],"nodeLoadIDs":"","ajacentNodeIDs":["49","51"]},"51":{"X":280,"Y":360,"beforeMoveX":280,"beforeMoveY":360,"dispX":0,"dispY":0,"dispRZ":0,"isSelected":false,"isShow":true,"showEdge":false,"showDeformation":true,"showTag":true,"eleCount":2,"connectType":"beam","massX":284,"massY":284,"massRz":284,"id":51,"elementIDs":["51","52"],"nodeLoadIDs":"","ajacentNodeIDs":["50","52"]},"52":{"X":280,"Y":350,"beforeMoveX":280,"beforeMoveY":350,"dispX":0,"dispY":0,"dispRZ":0,"isSelected":false,"isShow":true,"showEdge":false,"showDeformation":true,"showTag":true,"eleCount":2,"connectType":"beam","massX":284,"massY":284,"massRz":284,"id":52,"elementIDs":["52","53"],"nodeLoadIDs":"","ajacentNodeIDs":["9","51"]},"53":{"X":520,"Y":410,"beforeMoveX":520,"beforeMoveY":410,"dispX":0,"dispY":0,"dispRZ":0,"isSelected":false,"isShow":true,"showEdge":false,"showDeformation":true,"showTag":true,"eleCount":2,"connectType":"beam","massX":284,"massY":284,"massRz":284,"id":53,"elementIDs":["5","54"],"nodeLoadIDs":"","ajacentNodeIDs":["3","54"]},"54":{"X":520,"Y":400,"beforeMoveX":520,"beforeMoveY":400,"dispX":0,"dispY":0,"dispRZ":0,"isSelected":false,"isShow":true,"showEdge":false,"showDeformation":true,"showTag":true,"eleCount":2,"connectType":"beam","massX":284,"massY":284,"massRz":284,"id":54,"elementIDs":["54","55"],"nodeLoadIDs":"","ajacentNodeIDs":["53","55"]},"55":{"X":520,"Y":390,"beforeMoveX":520,"beforeMoveY":390,"dispX":0,"dispY":0,"dispRZ":0,"isSelected":false,"isShow":true,"showEdge":false,"showDeformation":true,"showTag":true,"eleCount":2,"connectType":"beam","massX":284,"massY":284,"massRz":284,"id":55,"elementIDs":["55","56"],"nodeLoadIDs":"","ajacentNodeIDs":["54","56"]},"56":{"X":520,"Y":380,"beforeMoveX":520,"beforeMoveY":380,"dispX":0,"dispY":0,"dispRZ":0,"isSelected":false,"isShow":true,"showEdge":false,"showDeformation":true,"showTag":true,"eleCount":2,"connectType":"beam","massX":284,"massY":284,"massRz":284,"id":56,"elementIDs":["56","57"],"nodeLoadIDs":"","ajacentNodeIDs":["55","57"]},"57":{"X":520,"Y":370,"beforeMoveX":520,"beforeMoveY":370,"dispX":0,"dispY":0,"dispRZ":0,"isSelected":false,"isShow":true,"showEdge":false,"showDeformation":true,"showTag":true,"eleCount":2,"connectType":"beam","massX":284,"massY":284,"massRz":284,"id":57,"elementIDs":["57","58"],"nodeLoadIDs":"","ajacentNodeIDs":["56","58"]},"58":{"X":520,"Y":360,"beforeMoveX":520,"beforeMoveY":360,"dispX":0,"dispY":0,"dispRZ":0,"isSelected":false,"isShow":true,"showEdge":false,"showDeformation":true,"showTag":true,"eleCount":2,"connectType":"beam","massX":284,"massY":284,"massRz":284,"id":58,"elementIDs":["58","59"],"nodeLoadIDs":"","ajacentNodeIDs":["57","59"]},"59":{"X":520,"Y":350,"beforeMoveX":520,"beforeMoveY":350,"dispX":0,"dispY":0,"dispRZ":0,"isSelected":false,"isShow":true,"showEdge":false,"showDeformation":true,"showTag":true,"eleCount":2,"connectType":"beam","massX":284,"massY":284,"massRz":284,"id":59,"elementIDs":["59","60"],"nodeLoadIDs":"","ajacentNodeIDs":["6","58"]},"60":{"X":290,"Y":420,"beforeMoveX":290,"beforeMoveY":420,"dispX":0,"dispY":0,"dispRZ":0,"isSelected":false,"isShow":true,"showEdge":false,"showDeformation":true,"showTag":true,"eleCount":2,"connectType":"beam","massX":284,"massY":284,"massRz":284,"id":60,"elementIDs":["6","61"],"nodeLoadIDs":"","ajacentNodeIDs":["7","61"]},"61":{"X":300,"Y":420,"beforeMoveX":300,"beforeMoveY":420,"dispX":0,"dispY":0,"dispRZ":0,"isSelected":false,"isShow":true,"showEdge":false,"showDeformation":true,"showTag":true,"eleCount":2,"connectType":"beam","massX":284,"massY":284,"massRz":284,"id":61,"elementIDs":["61","62"],"nodeLoadIDs":"","ajacentNodeIDs":["60","62"]},"62":{"X":310,"Y":420,"beforeMoveX":310,"beforeMoveY":420,"dispX":0,"dispY":0,"dispRZ":0,"isSelected":false,"isShow":true,"showEdge":false,"showDeformation":true,"showTag":true,"eleCount":2,"connectType":"beam","massX":284,"massY":284,"massRz":284,"id":62,"elementIDs":["62","63"],"nodeLoadIDs":"","ajacentNodeIDs":["61","63"]},"63":{"X":320,"Y":420,"beforeMoveX":320,"beforeMoveY":420,"dispX":0,"dispY":0,"dispRZ":0,"isSelected":false,"isShow":true,"showEdge":false,"showDeformation":true,"showTag":true,"eleCount":2,"connectType":"beam","massX":284,"massY":284,"massRz":284,"id":63,"elementIDs":["63","64"],"nodeLoadIDs":"","ajacentNodeIDs":["62","64"]},"64":{"X":330,"Y":420,"beforeMoveX":330,"beforeMoveY":420,"dispX":0,"dispY":0,"dispRZ":0,"isSelected":false,"isShow":true,"showEdge":false,"showDeformation":true,"showTag":true,"eleCount":2,"connectType":"beam","massX":284,"massY":284,"massRz":284,"id":64,"elementIDs":["64","65"],"nodeLoadIDs":"","ajacentNodeIDs":["63","65"]},"65":{"X":340,"Y":420,"beforeMoveX":340,"beforeMoveY":420,"dispX":0,"dispY":0,"dispRZ":0,"isSelected":false,"isShow":true,"showEdge":false,"showDeformation":true,"showTag":true,"eleCount":2,"connectType":"beam","massX":284,"massY":284,"massRz":284,"id":65,"elementIDs":["65","66"],"nodeLoadIDs":"","ajacentNodeIDs":["64","66"]},"66":{"X":350,"Y":420,"beforeMoveX":350,"beforeMoveY":420,"dispX":0,"dispY":0,"dispRZ":0,"isSelected":false,"isShow":true,"showEdge":false,"showDeformation":true,"showTag":true,"eleCount":2,"connectType":"beam","massX":284,"massY":284,"massRz":284,"id":66,"elementIDs":["66","67"],"nodeLoadIDs":"","ajacentNodeIDs":["65","67"]},"67":{"X":360,"Y":420,"beforeMoveX":360,"beforeMoveY":420,"dispX":0,"dispY":0,"dispRZ":0,"isSelected":false,"isShow":true,"showEdge":false,"showDeformation":true,"showTag":true,"eleCount":2,"connectType":"beam","massX":284,"massY":284,"massRz":284,"id":67,"elementIDs":["67","68"],"nodeLoadIDs":"","ajacentNodeIDs":["66","68"]},"68":{"X":370,"Y":420,"beforeMoveX":370,"beforeMoveY":420,"dispX":0,"dispY":0,"dispRZ":0,"isSelected":false,"isShow":true,"showEdge":false,"showDeformation":true,"showTag":true,"eleCount":2,"connectType":"beam","massX":284,"massY":284,"massRz":284,"id":68,"elementIDs":["68","69"],"nodeLoadIDs":"","ajacentNodeIDs":["67","69"]},"69":{"X":380,"Y":420,"beforeMoveX":380,"beforeMoveY":420,"dispX":0,"dispY":0,"dispRZ":0,"isSelected":false,"isShow":true,"showEdge":false,"showDeformation":true,"showTag":true,"eleCount":2,"connectType":"beam","massX":284,"massY":284,"massRz":284,"id":69,"elementIDs":["69","70"],"nodeLoadIDs":"","ajacentNodeIDs":["68","70"]},"70":{"X":390,"Y":420,"beforeMoveX":390,"beforeMoveY":420,"dispX":0,"dispY":0,"dispRZ":0,"isSelected":false,"isShow":true,"showEdge":false,"showDeformation":true,"showTag":true,"eleCount":2,"connectType":"beam","massX":284,"massY":284,"massRz":284,"id":70,"elementIDs":["70","71"],"nodeLoadIDs":"","ajacentNodeIDs":["69","71"]},"71":{"X":400,"Y":420,"beforeMoveX":400,"beforeMoveY":420,"dispX":0,"dispY":0,"dispRZ":0,"isSelected":false,"isShow":true,"showEdge":false,"showDeformation":true,"showTag":true,"eleCount":2,"connectType":"beam","massX":284,"massY":284,"massRz":284,"id":71,"elementIDs":["71","72"],"nodeLoadIDs":"","ajacentNodeIDs":["70","72"]},"72":{"X":410,"Y":420,"beforeMoveX":410,"beforeMoveY":420,"dispX":0,"dispY":0,"dispRZ":0,"isSelected":false,"isShow":true,"showEdge":false,"showDeformation":true,"showTag":true,"eleCount":2,"connectType":"beam","massX":284,"massY":284,"massRz":284,"id":72,"elementIDs":["72","73"],"nodeLoadIDs":"","ajacentNodeIDs":["71","73"]},"73":{"X":420,"Y":420,"beforeMoveX":420,"beforeMoveY":420,"dispX":0,"dispY":0,"dispRZ":0,"isSelected":false,"isShow":true,"showEdge":false,"showDeformation":true,"showTag":true,"eleCount":2,"connectType":"beam","massX":284,"massY":284,"massRz":284,"id":73,"elementIDs":["73","74"],"nodeLoadIDs":"","ajacentNodeIDs":["72","74"]},"74":{"X":430,"Y":420,"beforeMoveX":430,"beforeMoveY":420,"dispX":0,"dispY":0,"dispRZ":0,"isSelected":false,"isShow":true,"showEdge":false,"showDeformation":true,"showTag":true,"eleCount":2,"connectType":"beam","massX":284,"massY":284,"massRz":284,"id":74,"elementIDs":["74","75"],"nodeLoadIDs":"","ajacentNodeIDs":["73","75"]},"75":{"X":440,"Y":420,"beforeMoveX":440,"beforeMoveY":420,"dispX":0,"dispY":0,"dispRZ":0,"isSelected":false,"isShow":true,"showEdge":false,"showDeformation":true,"showTag":true,"eleCount":2,"connectType":"beam","massX":284,"massY":284,"massRz":284,"id":75,"elementIDs":["75","76"],"nodeLoadIDs":"","ajacentNodeIDs":["74","76"]},"76":{"X":450,"Y":420,"beforeMoveX":450,"beforeMoveY":420,"dispX":0,"dispY":0,"dispRZ":0,"isSelected":false,"isShow":true,"showEdge":false,"showDeformation":true,"showTag":true,"eleCount":2,"connectType":"beam","massX":284,"massY":284,"massRz":284,"id":76,"elementIDs":["76","77"],"nodeLoadIDs":"","ajacentNodeIDs":["75","77"]},"77":{"X":460,"Y":420,"beforeMoveX":460,"beforeMoveY":420,"dispX":0,"dispY":0,"dispRZ":0,"isSelected":false,"isShow":true,"showEdge":false,"showDeformation":true,"showTag":true,"eleCount":2,"connectType":"beam","massX":284,"massY":284,"massRz":284,"id":77,"elementIDs":["77","78"],"nodeLoadIDs":"","ajacentNodeIDs":["76","78"]},"78":{"X":470,"Y":420,"beforeMoveX":470,"beforeMoveY":420,"dispX":0,"dispY":0,"dispRZ":0,"isSelected":false,"isShow":true,"showEdge":false,"showDeformation":true,"showTag":true,"eleCount":2,"connectType":"beam","massX":284,"massY":284,"massRz":284,"id":78,"elementIDs":["78","79"],"nodeLoadIDs":"","ajacentNodeIDs":["77","79"]},"79":{"X":480,"Y":420,"beforeMoveX":480,"beforeMoveY":420,"dispX":0,"dispY":0,"dispRZ":0,"isSelected":false,"isShow":true,"showEdge":false,"showDeformation":true,"showTag":true,"eleCount":2,"connectType":"beam","massX":284,"massY":284,"massRz":284,"id":79,"elementIDs":["79","80"],"nodeLoadIDs":"","ajacentNodeIDs":["78","80"]},"80":{"X":490,"Y":420,"beforeMoveX":490,"beforeMoveY":420,"dispX":0,"dispY":0,"dispRZ":0,"isSelected":false,"isShow":true,"showEdge":false,"showDeformation":true,"showTag":true,"eleCount":2,"connectType":"beam","massX":284,"massY":284,"massRz":284,"id":80,"elementIDs":["80","81"],"nodeLoadIDs":"","ajacentNodeIDs":["79","81"]},"81":{"X":500,"Y":420,"beforeMoveX":500,"beforeMoveY":420,"dispX":0,"dispY":0,"dispRZ":0,"isSelected":false,"isShow":true,"showEdge":false,"showDeformation":true,"showTag":true,"eleCount":2,"connectType":"beam","massX":284,"massY":284,"massRz":284,"id":81,"elementIDs":["81","82"],"nodeLoadIDs":"","ajacentNodeIDs":["80","82"]},"82":{"X":510,"Y":420,"beforeMoveX":510,"beforeMoveY":420,"dispX":0,"dispY":0,"dispRZ":0,"isSelected":false,"isShow":true,"showEdge":false,"showDeformation":true,"showTag":true,"eleCount":2,"connectType":"beam","massX":284,"massY":284,"massRz":284,"id":82,"elementIDs":["82","83"],"nodeLoadIDs":"","ajacentNodeIDs":["3","81"]},"83":{"X":280,"Y":330,"beforeMoveX":280,"beforeMoveY":330,"dispX":0,"dispY":0,"dispRZ":0,"isSelected":false,"isShow":true,"showEdge":false,"showDeformation":true,"showTag":true,"eleCount":2,"connectType":"beam","massX":284,"massY":284,"massRz":284,"id":83,"elementIDs":["7","84"],"nodeLoadIDs":"","ajacentNodeIDs":["9","84"]},"84":{"X":280,"Y":320,"beforeMoveX":280,"beforeMoveY":320,"dispX":0,"dispY":0,"dispRZ":0,"isSelected":false,"isShow":true,"showEdge":false,"showDeformation":true,"showTag":true,"eleCount":2,"connectType":"beam","massX":284,"massY":284,"massRz":284,"id":84,"elementIDs":["84","85"],"nodeLoadIDs":"","ajacentNodeIDs":["83","85"]},"85":{"X":280,"Y":310,"beforeMoveX":280,"beforeMoveY":310,"dispX":0,"dispY":0,"dispRZ":0,"isSelected":false,"isShow":true,"showEdge":false,"showDeformation":true,"showTag":true,"eleCount":2,"connectType":"beam","massX":284,"massY":284,"massRz":284,"id":85,"elementIDs":["85","86"],"nodeLoadIDs":"","ajacentNodeIDs":["84","86"]},"86":{"X":280,"Y":300,"beforeMoveX":280,"beforeMoveY":300,"dispX":0,"dispY":0,"dispRZ":0,"isSelected":false,"isShow":true,"showEdge":false,"showDeformation":true,"showTag":true,"eleCount":2,"connectType":"beam","massX":284,"massY":284,"massRz":284,"id":86,"elementIDs":["86","87"],"nodeLoadIDs":"","ajacentNodeIDs":["85","87"]},"87":{"X":280,"Y":290,"beforeMoveX":280,"beforeMoveY":290,"dispX":0,"dispY":0,"dispRZ":0,"isSelected":false,"isShow":true,"showEdge":false,"showDeformation":true,"showTag":true,"eleCount":2,"connectType":"beam","massX":284,"massY":284,"massRz":284,"id":87,"elementIDs":["87","88"],"nodeLoadIDs":"","ajacentNodeIDs":["2","86"]},"88":{"X":520,"Y":330,"beforeMoveX":520,"beforeMoveY":330,"dispX":0,"dispY":0,"dispRZ":0,"isSelected":false,"isShow":true,"showEdge":false,"showDeformation":true,"showTag":true,"eleCount":2,"connectType":"beam","massX":284,"massY":284,"massRz":284,"id":88,"elementIDs":["8","89"],"nodeLoadIDs":"","ajacentNodeIDs":["6","89"]},"89":{"X":520,"Y":320,"beforeMoveX":520,"beforeMoveY":320,"dispX":0,"dispY":0,"dispRZ":0,"isSelected":false,"isShow":true,"showEdge":false,"showDeformation":true,"showTag":true,"eleCount":2,"connectType":"beam","massX":284,"massY":284,"massRz":284,"id":89,"elementIDs":["89","90"],"nodeLoadIDs":"","ajacentNodeIDs":["88","90"]},"90":{"X":520,"Y":310,"beforeMoveX":520,"beforeMoveY":310,"dispX":0,"dispY":0,"dispRZ":0,"isSelected":false,"isShow":true,"showEdge":false,"showDeformation":true,"showTag":true,"eleCount":2,"connectType":"beam","massX":284,"massY":284,"massRz":284,"id":90,"elementIDs":["90","91"],"nodeLoadIDs":"","ajacentNodeIDs":["89","91"]},"91":{"X":520,"Y":300,"beforeMoveX":520,"beforeMoveY":300,"dispX":0,"dispY":0,"dispRZ":0,"isSelected":false,"isShow":true,"showEdge":false,"showDeformation":true,"showTag":true,"eleCount":2,"connectType":"beam","massX":284,"massY":284,"massRz":284,"id":91,"elementIDs":["91","92"],"nodeLoadIDs":"","ajacentNodeIDs":["90","92"]},"92":{"X":520,"Y":290,"beforeMoveX":520,"beforeMoveY":290,"dispX":0,"dispY":0,"dispRZ":0,"isSelected":false,"isShow":true,"showEdge":false,"showDeformation":true,"showTag":true,"eleCount":2,"connectType":"beam","massX":284,"massY":284,"massRz":284,"id":92,"elementIDs":["92","93"],"nodeLoadIDs":"","ajacentNodeIDs":["5","91"]},"93":{"X":290,"Y":340,"beforeMoveX":290,"beforeMoveY":340,"dispX":0,"dispY":0,"dispRZ":0,"isSelected":false,"isShow":true,"showEdge":false,"showDeformation":true,"showTag":true,"eleCount":2,"connectType":"beam","massX":284,"massY":284,"massRz":284,"id":93,"elementIDs":["9","94"],"nodeLoadIDs":"","ajacentNodeIDs":["9","94"]},"94":{"X":300,"Y":340,"beforeMoveX":300,"beforeMoveY":340,"dispX":0,"dispY":0,"dispRZ":0,"isSelected":false,"isShow":true,"showEdge":false,"showDeformation":true,"showTag":true,"eleCount":2,"connectType":"beam","massX":284,"massY":284,"massRz":284,"id":94,"elementIDs":["94","95"],"nodeLoadIDs":"","ajacentNodeIDs":["93","95"]},"95":{"X":310,"Y":340,"beforeMoveX":310,"beforeMoveY":340,"dispX":0,"dispY":0,"dispRZ":0,"isSelected":false,"isShow":true,"showEdge":false,"showDeformation":true,"showTag":true,"eleCount":2,"connectType":"beam","massX":284,"massY":284,"massRz":284,"id":95,"elementIDs":["95","96"],"nodeLoadIDs":"","ajacentNodeIDs":["94","96"]},"96":{"X":320,"Y":340,"beforeMoveX":320,"beforeMoveY":340,"dispX":0,"dispY":0,"dispRZ":0,"isSelected":false,"isShow":true,"showEdge":false,"showDeformation":true,"showTag":true,"eleCount":2,"connectType":"beam","massX":284,"massY":284,"massRz":284,"id":96,"elementIDs":["96","97"],"nodeLoadIDs":"","ajacentNodeIDs":["95","97"]},"97":{"X":330,"Y":340,"beforeMoveX":330,"beforeMoveY":340,"dispX":0,"dispY":0,"dispRZ":0,"isSelected":false,"isShow":true,"showEdge":false,"showDeformation":true,"showTag":true,"eleCount":2,"connectType":"beam","massX":284,"massY":284,"massRz":284,"id":97,"elementIDs":["97","98"],"nodeLoadIDs":"","ajacentNodeIDs":["96","98"]},"98":{"X":340,"Y":340,"beforeMoveX":340,"beforeMoveY":340,"dispX":0,"dispY":0,"dispRZ":0,"isSelected":false,"isShow":true,"showEdge":false,"showDeformation":true,"showTag":true,"eleCount":2,"connectType":"beam","massX":284,"massY":284,"massRz":284,"id":98,"elementIDs":["98","99"],"nodeLoadIDs":"","ajacentNodeIDs":["97","99"]},"99":{"X":350,"Y":340,"beforeMoveX":350,"beforeMoveY":340,"dispX":0,"dispY":0,"dispRZ":0,"isSelected":false,"isShow":true,"showEdge":false,"showDeformation":true,"showTag":true,"eleCount":2,"connectType":"beam","massX":284,"massY":284,"massRz":284,"id":99,"elementIDs":["99","100"],"nodeLoadIDs":"","ajacentNodeIDs":["98","100"]},"100":{"X":360,"Y":340,"beforeMoveX":360,"beforeMoveY":340,"dispX":0,"dispY":0,"dispRZ":0,"isSelected":false,"isShow":true,"showEdge":false,"showDeformation":true,"showTag":true,"eleCount":2,"connectType":"beam","massX":284,"massY":284,"massRz":284,"id":100,"elementIDs":["100","101"],"nodeLoadIDs":"","ajacentNodeIDs":["99","101"]},"101":{"X":370,"Y":340,"beforeMoveX":370,"beforeMoveY":340,"dispX":0,"dispY":0,"dispRZ":0,"isSelected":false,"isShow":true,"showEdge":false,"showDeformation":true,"showTag":true,"eleCount":2,"connectType":"beam","massX":284,"massY":284,"massRz":284,"id":101,"elementIDs":["101","102"],"nodeLoadIDs":"","ajacentNodeIDs":["100","102"]},"102":{"X":380,"Y":340,"beforeMoveX":380,"beforeMoveY":340,"dispX":0,"dispY":0,"dispRZ":0,"isSelected":false,"isShow":true,"showEdge":false,"showDeformation":true,"showTag":true,"eleCount":2,"connectType":"beam","massX":284,"massY":284,"massRz":284,"id":102,"elementIDs":["102","103"],"nodeLoadIDs":"","ajacentNodeIDs":["101","103"]},"103":{"X":390,"Y":340,"beforeMoveX":390,"beforeMoveY":340,"dispX":0,"dispY":0,"dispRZ":0,"isSelected":false,"isShow":true,"showEdge":false,"showDeformation":true,"showTag":true,"eleCount":2,"connectType":"beam","massX":284,"massY":284,"massRz":284,"id":103,"elementIDs":["103","104"],"nodeLoadIDs":"","ajacentNodeIDs":["102","104"]},"104":{"X":400,"Y":340,"beforeMoveX":400,"beforeMoveY":340,"dispX":0,"dispY":0,"dispRZ":0,"isSelected":false,"isShow":true,"showEdge":false,"showDeformation":true,"showTag":true,"eleCount":2,"connectType":"beam","massX":284,"massY":284,"massRz":284,"id":104,"elementIDs":["104","105"],"nodeLoadIDs":"","ajacentNodeIDs":["103","105"]},"105":{"X":410,"Y":340,"beforeMoveX":410,"beforeMoveY":340,"dispX":0,"dispY":0,"dispRZ":0,"isSelected":false,"isShow":true,"showEdge":false,"showDeformation":true,"showTag":true,"eleCount":2,"connectType":"beam","massX":284,"massY":284,"massRz":284,"id":105,"elementIDs":["105","106"],"nodeLoadIDs":"","ajacentNodeIDs":["104","106"]},"106":{"X":420,"Y":340,"beforeMoveX":420,"beforeMoveY":340,"dispX":0,"dispY":0,"dispRZ":0,"isSelected":false,"isShow":true,"showEdge":false,"showDeformation":true,"showTag":true,"eleCount":2,"connectType":"beam","massX":284,"massY":284,"massRz":284,"id":106,"elementIDs":["106","107"],"nodeLoadIDs":"","ajacentNodeIDs":["105","107"]},"107":{"X":430,"Y":340,"beforeMoveX":430,"beforeMoveY":340,"dispX":0,"dispY":0,"dispRZ":0,"isSelected":false,"isShow":true,"showEdge":false,"showDeformation":true,"showTag":true,"eleCount":2,"connectType":"beam","massX":284,"massY":284,"massRz":284,"id":107,"elementIDs":["107","108"],"nodeLoadIDs":"","ajacentNodeIDs":["106","108"]},"108":{"X":440,"Y":340,"beforeMoveX":440,"beforeMoveY":340,"dispX":0,"dispY":0,"dispRZ":0,"isSelected":false,"isShow":true,"showEdge":false,"showDeformation":true,"showTag":true,"eleCount":2,"connectType":"beam","massX":284,"massY":284,"massRz":284,"id":108,"elementIDs":["108","109"],"nodeLoadIDs":"","ajacentNodeIDs":["107","109"]},"109":{"X":450,"Y":340,"beforeMoveX":450,"beforeMoveY":340,"dispX":0,"dispY":0,"dispRZ":0,"isSelected":false,"isShow":true,"showEdge":false,"showDeformation":true,"showTag":true,"eleCount":2,"connectType":"beam","massX":284,"massY":284,"massRz":284,"id":109,"elementIDs":["109","110"],"nodeLoadIDs":"","ajacentNodeIDs":["108","110"]},"110":{"X":460,"Y":340,"beforeMoveX":460,"beforeMoveY":340,"dispX":0,"dispY":0,"dispRZ":0,"isSelected":false,"isShow":true,"showEdge":false,"showDeformation":true,"showTag":true,"eleCount":2,"connectType":"beam","massX":284,"massY":284,"massRz":284,"id":110,"elementIDs":["110","111"],"nodeLoadIDs":"","ajacentNodeIDs":["109","111"]},"111":{"X":470,"Y":340,"beforeMoveX":470,"beforeMoveY":340,"dispX":0,"dispY":0,"dispRZ":0,"isSelected":false,"isShow":true,"showEdge":false,"showDeformation":true,"showTag":true,"eleCount":2,"connectType":"beam","massX":284,"massY":284,"massRz":284,"id":111,"elementIDs":["111","112"],"nodeLoadIDs":"","ajacentNodeIDs":["110","112"]},"112":{"X":480,"Y":340,"beforeMoveX":480,"beforeMoveY":340,"dispX":0,"dispY":0,"dispRZ":0,"isSelected":false,"isShow":true,"showEdge":false,"showDeformation":true,"showTag":true,"eleCount":2,"connectType":"beam","massX":284,"massY":284,"massRz":284,"id":112,"elementIDs":["112","113"],"nodeLoadIDs":"","ajacentNodeIDs":["111","113"]},"113":{"X":490,"Y":340,"beforeMoveX":490,"beforeMoveY":340,"dispX":0,"dispY":0,"dispRZ":0,"isSelected":false,"isShow":true,"showEdge":false,"showDeformation":true,"showTag":true,"eleCount":2,"connectType":"beam","massX":284,"massY":284,"massRz":284,"id":113,"elementIDs":["113","114"],"nodeLoadIDs":"","ajacentNodeIDs":["112","114"]},"114":{"X":500,"Y":340,"beforeMoveX":500,"beforeMoveY":340,"dispX":0,"dispY":0,"dispRZ":0,"isSelected":false,"isShow":true,"showEdge":false,"showDeformation":true,"showTag":true,"eleCount":2,"connectType":"beam","massX":284,"massY":284,"massRz":284,"id":114,"elementIDs":["114","115"],"nodeLoadIDs":"","ajacentNodeIDs":["113","115"]},"115":{"X":510,"Y":340,"beforeMoveX":510,"beforeMoveY":340,"dispX":0,"dispY":0,"dispRZ":0,"isSelected":false,"isShow":true,"showEdge":false,"showDeformation":true,"showTag":true,"eleCount":2,"connectType":"beam","massX":284,"massY":284,"massRz":284,"id":115,"elementIDs":["115","116"],"nodeLoadIDs":"","ajacentNodeIDs":["6","114"]}},"theElements":{"1":{"className":"ElasticBeamColumn","A":100,"E":29000,"I":833.3333,"density":0.284,"isSelected":false,"isShow":true,"mass":284,"id":1,"nodeIDs":[1,8],"elementLoadsIDs":"","geomTransfId":3},"2":{"className":"ElasticBeamColumn","A":100,"E":29000,"I":833.3333,"density":0.284,"isSelected":false,"isShow":true,"mass":284,"id":2,"nodeIDs":[1,16],"elementLoadsIDs":"","geomTransfId":3},"3":{"className":"ElasticBeamColumn","A":100,"E":29000,"I":833.3333,"density":0.284,"isSelected":false,"isShow":true,"mass":284,"id":3,"nodeIDs":[4,39],"elementLoadsIDs":"","geomTransfId":3},"4":{"className":"ElasticBeamColumn","A":100,"E":29000,"I":833.3333,"density":0.284,"isSelected":false,"isShow":true,"mass":284,"id":4,"nodeIDs":[7,46],"elementLoadsIDs":"","geomTransfId":3},"5":{"className":"ElasticBeamColumn","A":100,"E":29000,"I":833.3333,"density":0.284,"isSelected":false,"isShow":true,"mass":284,"id":5,"nodeIDs":[3,53],"elementLoadsIDs":"","geomTransfId":3},"6":{"className":"ElasticBeamColumn","A":100,"E":29000,"I":833.3333,"density":0.284,"isSelected":false,"isShow":true,"mass":284,"id":6,"nodeIDs":[7,60],"elementLoadsIDs":"","geomTransfId":3},"7":{"className":"ElasticBeamColumn","A":100,"E":29000,"I":833.3333,"density":0.284,"isSelected":false,"isShow":true,"mass":284,"id":7,"nodeIDs":[9,83],"elementLoadsIDs":"","geomTransfId":3},"8":{"className":"ElasticBeamColumn","A":100,"E":29000,"I":833.3333,"density":0.284,"isSelected":false,"isShow":true,"mass":284,"id":8,"nodeIDs":[6,88],"elementLoadsIDs":"","geomTransfId":3},"9":{"className":"ElasticBeamColumn","A":100,"E":29000,"I":833.3333,"density":0.284,"isSelected":false,"isShow":true,"mass":284,"id":9,"nodeIDs":[9,93],"elementLoadsIDs":"","geomTransfId":3},"10":{"className":"ElasticBeamColumn","A":100,"E":29000,"I":833.3333,"density":0.284,"isSelected":false,"isShow":true,"mass":284,"id":10,"nodeIDs":[8,10],"elementLoadsIDs":"","geomTransfId":3},"11":{"className":"ElasticBeamColumn","A":100,"E":29000,"I":833.3333,"density":0.284,"isSelected":false,"isShow":true,"mass":284,"id":11,"nodeIDs":[10,11],"elementLoadsIDs":"","geomTransfId":3},"12":{"className":"ElasticBeamColumn","A":100,"E":29000,"I":833.3333,"density":0.284,"isSelected":false,"isShow":true,"mass":284,"id":12,"nodeIDs":[11,12],"elementLoadsIDs":"","geomTransfId":3},"13":{"className":"ElasticBeamColumn","A":100,"E":29000,"I":833.3333,"density":0.284,"isSelected":false,"isShow":true,"mass":284,"id":13,"nodeIDs":[12,13],"elementLoadsIDs":"","geomTransfId":3},"14":{"className":"ElasticBeamColumn","A":100,"E":29000,"I":833.3333,"density":0.284,"isSelected":false,"isShow":true,"mass":284,"id":14,"nodeIDs":[13,14],"elementLoadsIDs":"","geomTransfId":3},"15":{"className":"ElasticBeamColumn","A":100,"E":29000,"I":833.3333,"density":0.284,"isSelected":false,"isShow":true,"mass":284,"id":15,"nodeIDs":[14,15],"elementLoadsIDs":"","geomTransfId":3},"16":{"className":"ElasticBeamColumn","A":100,"E":29000,"I":833.3333,"density":0.284,"isSelected":false,"isShow":true,"mass":284,"id":16,"nodeIDs":[15,7],"elementLoadsIDs":"","geomTransfId":3},"17":{"className":"ElasticBeamColumn","A":100,"E":29000,"I":833.3333,"density":0.284,"isSelected":false,"isShow":true,"mass":284,"id":17,"nodeIDs":[16,17],"elementLoadsIDs":"","geomTransfId":3},"18":{"className":"ElasticBeamColumn","A":100,"E":29000,"I":833.3333,"density":0.284,"isSelected":false,"isShow":true,"mass":284,"id":18,"nodeIDs":[17,18],"elementLoadsIDs":"","geomTransfId":3},"19":{"className":"ElasticBeamColumn","A":100,"E":29000,"I":833.3333,"density":0.284,"isSelected":false,"isShow":true,"mass":284,"id":19,"nodeIDs":[18,19],"elementLoadsIDs":"","geomTransfId":3},"20":{"className":"ElasticBeamColumn","A":100,"E":29000,"I":833.3333,"density":0.284,"isSelected":false,"isShow":true,"mass":284,"id":20,"nodeIDs":[19,20],"elementLoadsIDs":"","geomTransfId":3},"21":{"className":"ElasticBeamColumn","A":100,"E":29000,"I":833.3333,"density":0.284,"isSelected":false,"isShow":true,"mass":284,"id":21,"nodeIDs":[20,21],"elementLoadsIDs":"","geomTransfId":3},"22":{"className":"ElasticBeamColumn","A":100,"E":29000,"I":833.3333,"density":0.284,"isSelected":false,"isShow":true,"mass":284,"id":22,"nodeIDs":[21,22],"elementLoadsIDs":"","geomTransfId":3},"23":{"className":"ElasticBeamColumn","A":100,"E":29000,"I":833.3333,"density":0.284,"isSelected":false,"isShow":true,"mass":284,"id":23,"nodeIDs":[22,23],"elementLoadsIDs":"","geomTransfId":3},"24":{"className":"ElasticBeamColumn","A":100,"E":29000,"I":833.3333,"density":0.284,"isSelected":false,"isShow":true,"mass":284,"id":24,"nodeIDs":[23,24],"elementLoadsIDs":"","geomTransfId":3},"25":{"className":"ElasticBeamColumn","A":100,"E":29000,"I":833.3333,"density":0.284,"isSelected":false,"isShow":true,"mass":284,"id":25,"nodeIDs":[24,25],"elementLoadsIDs":"","geomTransfId":3},"26":{"className":"ElasticBeamColumn","A":100,"E":29000,"I":833.3333,"density":0.284,"isSelected":false,"isShow":true,"mass":284,"id":26,"nodeIDs":[25,26],"elementLoadsIDs":"","geomTransfId":3},"27":{"className":"ElasticBeamColumn","A":100,"E":29000,"I":833.3333,"density":0.284,"isSelected":false,"isShow":true,"mass":284,"id":27,"nodeIDs":[26,27],"elementLoadsIDs":"","geomTransfId":3},"28":{"className":"ElasticBeamColumn","A":100,"E":29000,"I":833.3333,"density":0.284,"isSelected":false,"isShow":true,"mass":284,"id":28,"nodeIDs":[27,28],"elementLoadsIDs":"","geomTransfId":3},"29":{"className":"ElasticBeamColumn","A":100,"E":29000,"I":833.3333,"density":0.284,"isSelected":false,"isShow":true,"mass":284,"id":29,"nodeIDs":[28,29],"elementLoadsIDs":"","geomTransfId":3},"30":{"className":"ElasticBeamColumn","A":100,"E":29000,"I":833.3333,"density":0.284,"isSelected":false,"isShow":true,"mass":284,"id":30,"nodeIDs":[29,30],"elementLoadsIDs":"","geomTransfId":3},"31":{"className":"ElasticBeamColumn","A":100,"E":29000,"I":833.3333,"density":0.284,"isSelected":false,"isShow":true,"mass":284,"id":31,"nodeIDs":[30,31],"elementLoadsIDs":"","geomTransfId":3},"32":{"className":"ElasticBeamColumn","A":100,"E":29000,"I":833.3333,"density":0.284,"isSelected":false,"isShow":true,"mass":284,"id":32,"nodeIDs":[31,32],"elementLoadsIDs":"","geomTransfId":3},"33":{"className":"ElasticBeamColumn","A":100,"E":29000,"I":833.3333,"density":0.284,"isSelected":false,"isShow":true,"mass":284,"id":33,"nodeIDs":[32,33],"elementLoadsIDs":"","geomTransfId":3},"34":{"className":"ElasticBeamColumn","A":100,"E":29000,"I":833.3333,"density":0.284,"isSelected":false,"isShow":true,"mass":284,"id":34,"nodeIDs":[33,34],"elementLoadsIDs":"","geomTransfId":3},"35":{"className":"ElasticBeamColumn","A":100,"E":29000,"I":833.3333,"density":0.284,"isSelected":false,"isShow":true,"mass":284,"id":35,"nodeIDs":[34,35],"elementLoadsIDs":"","geomTransfId":3},"36":{"className":"ElasticBeamColumn","A":100,"E":29000,"I":833.3333,"density":0.284,"isSelected":false,"isShow":true,"mass":284,"id":36,"nodeIDs":[35,36],"elementLoadsIDs":"","geomTransfId":3},"37":{"className":"ElasticBeamColumn","A":100,"E":29000,"I":833.3333,"density":0.284,"isSelected":false,"isShow":true,"mass":284,"id":37,"nodeIDs":[36,37],"elementLoadsIDs":"","geomTransfId":3},"38":{"className":"ElasticBeamColumn","A":100,"E":29000,"I":833.3333,"density":0.284,"isSelected":false,"isShow":true,"mass":284,"id":38,"nodeIDs":[37,38],"elementLoadsIDs":"","geomTransfId":3},"39":{"className":"ElasticBeamColumn","A":100,"E":29000,"I":833.3333,"density":0.284,"isSelected":false,"isShow":true,"mass":284,"id":39,"nodeIDs":[38,4],"elementLoadsIDs":"","geomTransfId":3},"40":{"className":"ElasticBeamColumn","A":100,"E":29000,"I":833.3333,"density":0.284,"isSelected":false,"isShow":true,"mass":284,"id":40,"nodeIDs":[39,40],"elementLoadsIDs":"","geomTransfId":3},"41":{"className":"ElasticBeamColumn","A":100,"E":29000,"I":833.3333,"density":0.284,"isSelected":false,"isShow":true,"mass":284,"id":41,"nodeIDs":[40,41],"elementLoadsIDs":"","geomTransfId":3},"42":{"className":"ElasticBeamColumn","A":100,"E":29000,"I":833.3333,"density":0.284,"isSelected":false,"isShow":true,"mass":284,"id":42,"nodeIDs":[41,42],"elementLoadsIDs":"","geomTransfId":3},"43":{"className":"ElasticBeamColumn","A":100,"E":29000,"I":833.3333,"density":0.284,"isSelected":false,"isShow":true,"mass":284,"id":43,"nodeIDs":[42,43],"elementLoadsIDs":"","geomTransfId":3},"44":{"className":"ElasticBeamColumn","A":100,"E":29000,"I":833.3333,"density":0.284,"isSelected":false,"isShow":true,"mass":284,"id":44,"nodeIDs":[43,44],"elementLoadsIDs":"","geomTransfId":3},"45":{"className":"ElasticBeamColumn","A":100,"E":29000,"I":833.3333,"density":0.284,"isSelected":false,"isShow":true,"mass":284,"id":45,"nodeIDs":[44,45],"elementLoadsIDs":"","geomTransfId":3},"46":{"className":"ElasticBeamColumn","A":100,"E":29000,"I":833.3333,"density":0.284,"isSelected":false,"isShow":true,"mass":284,"id":46,"nodeIDs":[45,3],"elementLoadsIDs":"","geomTransfId":3},"47":{"className":"ElasticBeamColumn","A":100,"E":29000,"I":833.3333,"density":0.284,"isSelected":false,"isShow":true,"mass":284,"id":47,"nodeIDs":[46,47],"elementLoadsIDs":"","geomTransfId":3},"48":{"className":"ElasticBeamColumn","A":100,"E":29000,"I":833.3333,"density":0.284,"isSelected":false,"isShow":true,"mass":284,"id":48,"nodeIDs":[47,48],"elementLoadsIDs":"","geomTransfId":3},"49":{"className":"ElasticBeamColumn","A":100,"E":29000,"I":833.3333,"density":0.284,"isSelected":false,"isShow":true,"mass":284,"id":49,"nodeIDs":[48,49],"elementLoadsIDs":"","geomTransfId":3},"50":{"className":"ElasticBeamColumn","A":100,"E":29000,"I":833.3333,"density":0.284,"isSelected":false,"isShow":true,"mass":284,"id":50,"nodeIDs":[49,50],"elementLoadsIDs":"","geomTransfId":3},"51":{"className":"ElasticBeamColumn","A":100,"E":29000,"I":833.3333,"density":0.284,"isSelected":false,"isShow":true,"mass":284,"id":51,"nodeIDs":[50,51],"elementLoadsIDs":"","geomTransfId":3},"52":{"className":"ElasticBeamColumn","A":100,"E":29000,"I":833.3333,"density":0.284,"isSelected":false,"isShow":true,"mass":284,"id":52,"nodeIDs":[51,52],"elementLoadsIDs":"","geomTransfId":3},"53":{"className":"ElasticBeamColumn","A":100,"E":29000,"I":833.3333,"density":0.284,"isSelected":false,"isShow":true,"mass":284,"id":53,"nodeIDs":[52,9],"elementLoadsIDs":"","geomTransfId":3},"54":{"className":"ElasticBeamColumn","A":100,"E":29000,"I":833.3333,"density":0.284,"isSelected":false,"isShow":true,"mass":284,"id":54,"nodeIDs":[53,54],"elementLoadsIDs":"","geomTransfId":3},"55":{"className":"ElasticBeamColumn","A":100,"E":29000,"I":833.3333,"density":0.284,"isSelected":false,"isShow":true,"mass":284,"id":55,"nodeIDs":[54,55],"elementLoadsIDs":"","geomTransfId":3},"56":{"className":"ElasticBeamColumn","A":100,"E":29000,"I":833.3333,"density":0.284,"isSelected":false,"isShow":true,"mass":284,"id":56,"nodeIDs":[55,56],"elementLoadsIDs":"","geomTransfId":3},"57":{"className":"ElasticBeamColumn","A":100,"E":29000,"I":833.3333,"density":0.284,"isSelected":false,"isShow":true,"mass":284,"id":57,"nodeIDs":[56,57],"elementLoadsIDs":"","geomTransfId":3},"58":{"className":"ElasticBeamColumn","A":100,"E":29000,"I":833.3333,"density":0.284,"isSelected":false,"isShow":true,"mass":284,"id":58,"nodeIDs":[57,58],"elementLoadsIDs":"","geomTransfId":3},"59":{"className":"ElasticBeamColumn","A":100,"E":29000,"I":833.3333,"density":0.284,"isSelected":false,"isShow":true,"mass":284,"id":59,"nodeIDs":[58,59],"elementLoadsIDs":"","geomTransfId":3},"60":{"className":"ElasticBeamColumn","A":100,"E":29000,"I":833.3333,"density":0.284,"isSelected":false,"isShow":true,"mass":284,"id":60,"nodeIDs":[59,6],"elementLoadsIDs":"","geomTransfId":3},"61":{"className":"ElasticBeamColumn","A":100,"E":29000,"I":833.3333,"density":0.284,"isSelected":false,"isShow":true,"mass":284,"id":61,"nodeIDs":[60,61],"elementLoadsIDs":"","geomTransfId":3},"62":{"className":"ElasticBeamColumn","A":100,"E":29000,"I":833.3333,"density":0.284,"isSelected":false,"isShow":true,"mass":284,"id":62,"nodeIDs":[61,62],"elementLoadsIDs":"","geomTransfId":3},"63":{"className":"ElasticBeamColumn","A":100,"E":29000,"I":833.3333,"density":0.284,"isSelected":false,"isShow":true,"mass":284,"id":63,"nodeIDs":[62,63],"elementLoadsIDs":"","geomTransfId":3},"64":{"className":"ElasticBeamColumn","A":100,"E":29000,"I":833.3333,"density":0.284,"isSelected":false,"isShow":true,"mass":284,"id":64,"nodeIDs":[63,64],"elementLoadsIDs":"","geomTransfId":3},"65":{"className":"ElasticBeamColumn","A":100,"E":29000,"I":833.3333,"density":0.284,"isSelected":false,"isShow":true,"mass":284,"id":65,"nodeIDs":[64,65],"elementLoadsIDs":"","geomTransfId":3},"66":{"className":"ElasticBeamColumn","A":100,"E":29000,"I":833.3333,"density":0.284,"isSelected":false,"isShow":true,"mass":284,"id":66,"nodeIDs":[65,66],"elementLoadsIDs":"","geomTransfId":3},"67":{"className":"ElasticBeamColumn","A":100,"E":29000,"I":833.3333,"density":0.284,"isSelected":false,"isShow":true,"mass":284,"id":67,"nodeIDs":[66,67],"elementLoadsIDs":"","geomTransfId":3},"68":{"className":"ElasticBeamColumn","A":100,"E":29000,"I":833.3333,"density":0.284,"isSelected":false,"isShow":true,"mass":284,"id":68,"nodeIDs":[67,68],"elementLoadsIDs":"","geomTransfId":3},"69":{"className":"ElasticBeamColumn","A":100,"E":29000,"I":833.3333,"density":0.284,"isSelected":false,"isShow":true,"mass":284,"id":69,"nodeIDs":[68,69],"elementLoadsIDs":"","geomTransfId":3},"70":{"className":"ElasticBeamColumn","A":100,"E":29000,"I":833.3333,"density":0.284,"isSelected":false,"isShow":true,"mass":284,"id":70,"nodeIDs":[69,70],"elementLoadsIDs":"","geomTransfId":3},"71":{"className":"ElasticBeamColumn","A":100,"E":29000,"I":833.3333,"density":0.284,"isSelected":false,"isShow":true,"mass":284,"id":71,"nodeIDs":[70,71],"elementLoadsIDs":"","geomTransfId":3},"72":{"className":"ElasticBeamColumn","A":100,"E":29000,"I":833.3333,"density":0.284,"isSelected":false,"isShow":true,"mass":284,"id":72,"nodeIDs":[71,72],"elementLoadsIDs":"","geomTransfId":3},"73":{"className":"ElasticBeamColumn","A":100,"E":29000,"I":833.3333,"density":0.284,"isSelected":false,"isShow":true,"mass":284,"id":73,"nodeIDs":[72,73],"elementLoadsIDs":"","geomTransfId":3},"74":{"className":"ElasticBeamColumn","A":100,"E":29000,"I":833.3333,"density":0.284,"isSelected":false,"isShow":true,"mass":284,"id":74,"nodeIDs":[73,74],"elementLoadsIDs":"","geomTransfId":3},"75":{"className":"ElasticBeamColumn","A":100,"E":29000,"I":833.3333,"density":0.284,"isSelected":false,"isShow":true,"mass":284,"id":75,"nodeIDs":[74,75],"elementLoadsIDs":"","geomTransfId":3},"76":{"className":"ElasticBeamColumn","A":100,"E":29000,"I":833.3333,"density":0.284,"isSelected":false,"isShow":true,"mass":284,"id":76,"nodeIDs":[75,76],"elementLoadsIDs":"","geomTransfId":3},"77":{"className":"ElasticBeamColumn","A":100,"E":29000,"I":833.3333,"density":0.284,"isSelected":false,"isShow":true,"mass":284,"id":77,"nodeIDs":[76,77],"elementLoadsIDs":"","geomTransfId":3},"78":{"className":"ElasticBeamColumn","A":100,"E":29000,"I":833.3333,"density":0.284,"isSelected":false,"isShow":true,"mass":284,"id":78,"nodeIDs":[77,78],"elementLoadsIDs":"","geomTransfId":3},"79":{"className":"ElasticBeamColumn","A":100,"E":29000,"I":833.3333,"density":0.284,"isSelected":false,"isShow":true,"mass":284,"id":79,"nodeIDs":[78,79],"elementLoadsIDs":"","geomTransfId":3},"80":{"className":"ElasticBeamColumn","A":100,"E":29000,"I":833.3333,"density":0.284,"isSelected":false,"isShow":true,"mass":284,"id":80,"nodeIDs":[79,80],"elementLoadsIDs":"","geomTransfId":3},"81":{"className":"ElasticBeamColumn","A":100,"E":29000,"I":833.3333,"density":0.284,"isSelected":false,"isShow":true,"mass":284,"id":81,"nodeIDs":[80,81],"elementLoadsIDs":"","geomTransfId":3},"82":{"className":"ElasticBeamColumn","A":100,"E":29000,"I":833.3333,"density":0.284,"isSelected":false,"isShow":true,"mass":284,"id":82,"nodeIDs":[81,82],"elementLoadsIDs":"","geomTransfId":3},"83":{"className":"ElasticBeamColumn","A":100,"E":29000,"I":833.3333,"density":0.284,"isSelected":false,"isShow":true,"mass":284,"id":83,"nodeIDs":[82,3],"elementLoadsIDs":"","geomTransfId":3},"84":{"className":"ElasticBeamColumn","A":100,"E":29000,"I":833.3333,"density":0.284,"isSelected":false,"isShow":true,"mass":284,"id":84,"nodeIDs":[83,84],"elementLoadsIDs":"","geomTransfId":3},"85":{"className":"ElasticBeamColumn","A":100,"E":29000,"I":833.3333,"density":0.284,"isSelected":false,"isShow":true,"mass":284,"id":85,"nodeIDs":[84,85],"elementLoadsIDs":"","geomTransfId":3},"86":{"className":"ElasticBeamColumn","A":100,"E":29000,"I":833.3333,"density":0.284,"isSelected":false,"isShow":true,"mass":284,"id":86,"nodeIDs":[85,86],"elementLoadsIDs":"","geomTransfId":3},"87":{"className":"ElasticBeamColumn","A":100,"E":29000,"I":833.3333,"density":0.284,"isSelected":false,"isShow":true,"mass":284,"id":87,"nodeIDs":[86,87],"elementLoadsIDs":"","geomTransfId":3},"88":{"className":"ElasticBeamColumn","A":100,"E":29000,"I":833.3333,"density":0.284,"isSelected":false,"isShow":true,"mass":284,"id":88,"nodeIDs":[87,2],"elementLoadsIDs":"","geomTransfId":3},"89":{"className":"ElasticBeamColumn","A":100,"E":29000,"I":833.3333,"density":0.284,"isSelected":false,"isShow":true,"mass":284,"id":89,"nodeIDs":[88,89],"elementLoadsIDs":"","geomTransfId":3},"90":{"className":"ElasticBeamColumn","A":100,"E":29000,"I":833.3333,"density":0.284,"isSelected":false,"isShow":true,"mass":284,"id":90,"nodeIDs":[89,90],"elementLoadsIDs":"","geomTransfId":3},"91":{"className":"ElasticBeamColumn","A":100,"E":29000,"I":833.3333,"density":0.284,"isSelected":false,"isShow":true,"mass":284,"id":91,"nodeIDs":[90,91],"elementLoadsIDs":"","geomTransfId":3},"92":{"className":"ElasticBeamColumn","A":100,"E":29000,"I":833.3333,"density":0.284,"isSelected":false,"isShow":true,"mass":284,"id":92,"nodeIDs":[91,92],"elementLoadsIDs":"","geomTransfId":3},"93":{"className":"ElasticBeamColumn","A":100,"E":29000,"I":833.3333,"density":0.284,"isSelected":false,"isShow":true,"mass":284,"id":93,"nodeIDs":[92,5],"elementLoadsIDs":"","geomTransfId":3},"94":{"className":"ElasticBeamColumn","A":100,"E":29000,"I":833.3333,"density":0.284,"isSelected":false,"isShow":true,"mass":284,"id":94,"nodeIDs":[93,94],"elementLoadsIDs":"","geomTransfId":3},"95":{"className":"ElasticBeamColumn","A":100,"E":29000,"I":833.3333,"density":0.284,"isSelected":false,"isShow":true,"mass":284,"id":95,"nodeIDs":[94,95],"elementLoadsIDs":"","geomTransfId":3},"96":{"className":"ElasticBeamColumn","A":100,"E":29000,"I":833.3333,"density":0.284,"isSelected":false,"isShow":true,"mass":284,"id":96,"nodeIDs":[95,96],"elementLoadsIDs":"","geomTransfId":3},"97":{"className":"ElasticBeamColumn","A":100,"E":29000,"I":833.3333,"density":0.284,"isSelected":false,"isShow":true,"mass":284,"id":97,"nodeIDs":[96,97],"elementLoadsIDs":"","geomTransfId":3},"98":{"className":"ElasticBeamColumn","A":100,"E":29000,"I":833.3333,"density":0.284,"isSelected":false,"isShow":true,"mass":284,"id":98,"nodeIDs":[97,98],"elementLoadsIDs":"","geomTransfId":3},"99":{"className":"ElasticBeamColumn","A":100,"E":29000,"I":833.3333,"density":0.284,"isSelected":false,"isShow":true,"mass":284,"id":99,"nodeIDs":[98,99],"elementLoadsIDs":"","geomTransfId":3},"100":{"className":"ElasticBeamColumn","A":100,"E":29000,"I":833.3333,"density":0.284,"isSelected":false,"isShow":true,"mass":284,"id":100,"nodeIDs":[99,100],"elementLoadsIDs":"","geomTransfId":3},"101":{"className":"ElasticBeamColumn","A":100,"E":29000,"I":833.3333,"density":0.284,"isSelected":false,"isShow":true,"mass":284,"id":101,"nodeIDs":[100,101],"elementLoadsIDs":"","geomTransfId":3},"102":{"className":"ElasticBeamColumn","A":100,"E":29000,"I":833.3333,"density":0.284,"isSelected":false,"isShow":true,"mass":284,"id":102,"nodeIDs":[101,102],"elementLoadsIDs":"","geomTransfId":3},"103":{"className":"ElasticBeamColumn","A":100,"E":29000,"I":833.3333,"density":0.284,"isSelected":false,"isShow":true,"mass":284,"id":103,"nodeIDs":[102,103],"elementLoadsIDs":"","geomTransfId":3},"104":{"className":"ElasticBeamColumn","A":100,"E":29000,"I":833.3333,"density":0.284,"isSelected":false,"isShow":true,"mass":284,"id":104,"nodeIDs":[103,104],"elementLoadsIDs":"","geomTransfId":3},"105":{"className":"ElasticBeamColumn","A":100,"E":29000,"I":833.3333,"density":0.284,"isSelected":false,"isShow":true,"mass":284,"id":105,"nodeIDs":[104,105],"elementLoadsIDs":"","geomTransfId":3},"106":{"className":"ElasticBeamColumn","A":100,"E":29000,"I":833.3333,"density":0.284,"isSelected":false,"isShow":true,"mass":284,"id":106,"nodeIDs":[105,106],"elementLoadsIDs":"","geomTransfId":3},"107":{"className":"ElasticBeamColumn","A":100,"E":29000,"I":833.3333,"density":0.284,"isSelected":false,"isShow":true,"mass":284,"id":107,"nodeIDs":[106,107],"elementLoadsIDs":"","geomTransfId":3},"108":{"className":"ElasticBeamColumn","A":100,"E":29000,"I":833.3333,"density":0.284,"isSelected":false,"isShow":true,"mass":284,"id":108,"nodeIDs":[107,108],"elementLoadsIDs":"","geomTransfId":3},"109":{"className":"ElasticBeamColumn","A":100,"E":29000,"I":833.3333,"density":0.284,"isSelected":false,"isShow":true,"mass":284,"id":109,"nodeIDs":[108,109],"elementLoadsIDs":"","geomTransfId":3},"110":{"className":"ElasticBeamColumn","A":100,"E":29000,"I":833.3333,"density":0.284,"isSelected":false,"isShow":true,"mass":284,"id":110,"nodeIDs":[109,110],"elementLoadsIDs":"","geomTransfId":3},"111":{"className":"ElasticBeamColumn","A":100,"E":29000,"I":833.3333,"density":0.284,"isSelected":false,"isShow":true,"mass":284,"id":111,"nodeIDs":[110,111],"elementLoadsIDs":"","geomTransfId":3},"112":{"className":"ElasticBeamColumn","A":100,"E":29000,"I":833.3333,"density":0.284,"isSelected":false,"isShow":true,"mass":284,"id":112,"nodeIDs":[111,112],"elementLoadsIDs":"","geomTransfId":3},"113":{"className":"ElasticBeamColumn","A":100,"E":29000,"I":833.3333,"density":0.284,"isSelected":false,"isShow":true,"mass":284,"id":113,"nodeIDs":[112,113],"elementLoadsIDs":"","geomTransfId":3},"114":{"className":"ElasticBeamColumn","A":100,"E":29000,"I":833.3333,"density":0.284,"isSelected":false,"isShow":true,"mass":284,"id":114,"nodeIDs":[113,114],"elementLoadsIDs":"","geomTransfId":3},"115":{"className":"ElasticBeamColumn","A":100,"E":29000,"I":833.3333,"density":0.284,"isSelected":false,"isShow":true,"mass":284,"id":115,"nodeIDs":[114,115],"elementLoadsIDs":"","geomTransfId":3},"116":{"className":"ElasticBeamColumn","A":100,"E":29000,"I":833.3333,"density":0.284,"isSelected":false,"isShow":true,"mass":284,"id":116,"nodeIDs":[115,6],"elementLoadsIDs":"","geomTransfId":3}},"theSPCs":{"1":{"X":1,"Y":1,"RZ":1,"angle":-1.5707963267948966,"direction":"up","isSelected":false,"isShow":true,"id":1,"nodeID":2},"2":{"X":1,"Y":1,"RZ":1,"angle":-1.2445498555097438,"direction":"oblique","isSelected":false,"isShow":true,"id":2,"nodeID":5}}}';
			var testmodel2=eval('('+str+')');
			
			// Domain.BuildModelFromJson(testmodel1);
			Domain.buildModelFromJson(testmodel2);

			//init Renderer
			this.Renderer = new $D.Renderer(document.getElementById('workspace'));
			window.Renderer = this.Renderer;
			var ws;
			function get_appropriate_ws_url() {
				var pcol;
				var u = document.URL;
				if(u.substring(0, 5) == "https") {
					pcol = "wss://";
					u = u.substr(8);
				} else {
					pcol = "ws://";
					if(u.substring(0, 4) == "http") {
						u = u.substr(7);
					}
				}
				u = u.split('/');
			
				return pcol + u[0];
			}
			
			// if(BrowserDetect.browser == "Firefox") {
				// ws = new MozWebSocket(get_appropriate_ws_url(), "ops-protocol");
			// } else {
				// ws = new WebSocket(get_appropriate_ws_url(), "ops-protocol");
			// }
			ws = new WebSocket(get_appropriate_ws_url(), "ops-protocol");
		
			try {
				ws.onopen = function(msg) {
					console.log("websocket connection opened!")
				}
				ws.onmessage = function(msg) {
					sketchit.printMessage("welcome: "+msg.data+" !");
					ws.onmessage = function(m) {
						var str_in=m.data;
						var buffer;
						console.log("message:",m)
						
						if(str_in.slice(0,2)==="1]") {
							buffer=eval(str_in.slice(2));
							$D.apply(sketchit.Domain.nodeDispHistory,buffer);
							console.log("nodeDispHistory",Domain.nodeDispHistory);
						} else if (str_in.slice(0,2)==="2]"){
							buffer=eval(str_in.slice(2));
							$D.apply(sketchit.Domain.elementForceHistory,buffer);
							console.log("elementForceHistory",Domain.elementForceHistory);
						} else {
							console.log("unknown header: "+str_in.slice(0,2));
							
						}
					};
					
				}
		
				ws.onclose = function() {
					// OPSTerm.message("websocket connection CLOSED\n")
					alert("websocket connection CLOSED\n")
				}
			} catch(exception) {
				alert('<p>Error' + exception);
			}
			this.ws=ws;

			this.settings = {};
			$D.apply(this.settings, defaultSettings)

			this.inputStrokes = [];
			this.logs = [];
			this.shapeRecognizer = new DollarRecognizer();
			// this.initHandlers();
			this.setCanvasPosition(0, this.views.topBar.getHeight(), this.views.viewport.getWidth(), this.views.viewport.getHeight() - this.views.topBar.getHeight() - this.views.bottomBar.getHeight());
			this.resetViewPort();
			this.refresh();
			
	    },
		loadSettingsToSettingTabs : function() {
			var str = "";
			var S = this.settings;
			var value;
			for (var key in S) {
				value = S[key];
				if ($D.isString(value)) {
					value = "\""+value+"\"";
				}
				str += key+" : "+ value+ ",\n";
			}
			str = str.slice(0,-2);				
			this.views.settingTabs.getComponent(4).getComponent(0).setValue(str);
		},
		applySettingsFromSettingTabs : function() {
			var temp = eval("({"+this.views.settingTabs.getComponent(4).getComponent(0).getValue()+"})")
			$D.apply(this.settings,temp);
			console.log("settings",temp)
		},
		getCanvasCoordFromViewPort : function(p) {
			return {
				X : (p.X - this.settings.viewPortShiftX) / this.settings.viewPortScale,
				Y : (p.Y - this.settings.viewPortShiftY) / this.settings.viewPortScale
			}
		},
		getViewPortCoordFromCanvas : function(p) {
			return {
				X : p.X * this.settings.viewPortScale + this.settings.viewPortShiftX,
				Y : p.Y * this.settings.viewPortScale + this.settings.viewPortShiftY
			}
		},
		getViewPortCoordFromPage : function(p) {
			return {
				X : p.X - this.canvasUpLeftX,
				Y : this.canvasHeight - p.Y + this.canvasUpleftY
			}
		},
		getCanvasCoordFromPage : function(p) {
			return {
				X : (p.X - this.canvasUpLeftX - this.settings.viewPortShiftX) / this.settings.viewPortScale,
				Y : (this.canvasHeight - p.Y + this.canvasUpleftY - this.settings.viewPortShiftY) / this.settings.viewPortScale
			}
		},
		setCanvasPosition : function(upleftX, upleftY, width, height) {
			this.canvasUpLeftX = upleftX;
			this.canvasUpleftY = upleftY;
			this.canvasWidth = width;
			this.canvasHeight = height;
		},
		initCT : function() {
			this.Renderer.setTransform(1.0, 0, 0, -1.0, 0, this.canvasHeight);
		},
		applyViewPortTransform : function() {
			this.Renderer.transform(this.settings.viewPortScale, 0, 0, this.settings.viewPortScale, this.settings.viewPortShiftX, this.settings.viewPortShiftY);
		},
		resetViewPort : function() {
			this.settings.viewPortScale = 1.0;
			this.settings.viewPortShiftX = 0.0;
			this.settings.viewPortShiftY = 0.0;
		},
		deltaTransform : function(dScale, dShiftX, dShiftY) {
			var R = this.Renderer;
			R.save();
			R.transform(dScale, 0, 0, dScale, dShiftX, dShiftY);
			this.deltaScale = dScale;
			this.deltaShiftX = dShiftX;
			this.deltaShiftY = dShiftY;
			this.clearScreen();
			R.save();
			this.initCT();
			this.drawMessage();
			R.restore();
			this.drawDomain();
			R.restore();
		},
		recordDeltaTransform : function() {
			var S = this.settings;
			S.viewPortShiftX = S.viewPortScale * this.deltaShiftX + S.viewPortShiftX;
			S.viewPortShiftY = S.viewPortScale * this.deltaShiftY + S.viewPortShiftY;
			S.viewPortScale = S.viewPortScale * this.deltaScale;
		},
		applyInputStrokeStyle : function(scale) {
			var R = this.Renderer, S = this.settings;
			R.strokeStyle = S.inputStrokeStyle;
			R.lineWidth = S.inputStrokeWidth / scale;
		},
		applyGridStyle : function(scale) {
			var R = this.Renderer, S = this.settings;
			R.strokeStyle = S.gridLineStyle;
			R.lineWidth = S.gridLineWidth / scale;
		},
		getAutoDeformationScale : function(maxDispOnScreen) {
			var a = $D.getAbsMax(this.Domain.theNodes, "dispX"), b = $D.getAbsMax(this.Domain.theNodes, "dispY"), m = a > b ? a : b;
			return maxDispOnScreen / m;
		},
		getAutoMomentScale : function(maxDispOnScreen) {
			var a = $D.getAbsMax(this.Domain.theElements, "maxMoment");
			console.log("maxM", a)
			return maxDispOnScreen / a;
		},
		clearScreen : function() {
			var R = this.Renderer;
			R.save();
			this.initCT();
			R.fillStyle = this.settings.canvasBgColor;
			R.fillRect(0, 0, this.canvasWidth, this.canvasHeight);
			R.restore();
		},
		drawObjectStore : function(store, fn) {
			var iter = $D.iterate;
			var args = Array.prototype.slice.call(arguments, 2);
			iter(store, function(e) {
				e[fn].apply(e, args)
			})
		},
		drawMessage : function() {
			var R = this.Renderer, //
			S = this.settings, //
			msg = this.screenMessage;
			if($D.isDefined(msg)) {
				R.save();
				R.font = S.messageTextFont;
				R.fillStyle = S.messageTextFillStye;
				R.transform(1, 0, 0, -1, 0, 0);
				R.fillText(msg, S.messageBoxPositionX, -S.messageBoxPositionY);
				R.restore();
			}
		},
		drawFrameRate : function() {
			var R = this.Renderer, //
			S = this.settings, //
			msg = "fps:"+fps.toFixed(2);
			if($D.isDefined(msg)) {
				R.save();
				R.font = S.messageTextFont;
				R.fillStyle = S.messageTextFillStye;
				R.transform(1, 0, 0, -1, 0, 0);
				// R.fillText(msg, S.frameRatePositionX, -S.frameRatePositionY);
				R.fillText(msg, this.canvasWidth-80, -(this.canvasHeight-10));
				R.restore();
			}
		},
		drawDomain : function() {
			var R = this.Renderer, //
			S = this.settings, //
			vps = S.viewPortScale, //
			dfs = S.deformationScale, //
			draw = this.drawObjectStore, //
			domain = this.Domain, //
			nodes = domain.theNodes, //
			eles = domain.theElements;

			if(S.showGrid) {
				var c1 = this.getCanvasCoordFromViewPort({
					X : 0,
					Y : 0
				}), c2 = this.getCanvasCoordFromViewPort({
					X : this.canvasWidth,
					Y : this.canvasHeight
				});
				this.applyGridStyle(vps);
				R.drawGrid(S.grid, S.grid, c1.X, c2.X, c1.Y, c2.Y);
			}
			
			if (S.showStructure) {
				draw(eles, "display", R, vps);
			}
			if (S.showConstraints) {
				draw(domain.theSPCs, "display", R, vps);
			}
			if (S.showLoads) {
				draw(domain.thePatterns[1].Loads, "display", R, vps);
				draw(domain.thePatterns[2].Loads, "display", R, vps);
			}
			if (S.showNodes) {
				draw(nodes, "display", R, vps);
			}

			if(S.showDeformation) {
			// if(this.Domain.deformationAvailable && S.showDeformation) {
				if (S.autoAnalysis) {
					domain.loadNodeDispResultAtT("1.000");
				}
				
				draw(eles, "displayDeformation", R, vps, dfs, S.deformationResolution);
				draw(nodes, "displayDeformation", R, vps, dfs);
			}
			if(S.showMoment) {
			// if(this.Domain.deformationAvailable && S.showMoment) {
				draw(eles, "displayMoment", R, vps, S.momentScale, S.momentResolution);
			}
			if(S.showNodeId) {
				draw(nodes, "displayTag", R, vps);
			}
			if(S.showElementId) {
				draw(eles, "displayTag", R, vps);
			}
			if(S.showLineElementDirection) {
				draw(eles, "displayDirection", R, vps);
			}
		},
		
		drawInputStrokes : function() {
			for (var i=1; i < this.inputStrokes.length; i++) {
				this.applyInputStrokeStyle(this.settings.viewPortScale);
				this.Renderer.drawLine(this.inputStrokes[i - 1], this.inputStrokes[i]);
			};					
		},
		
		refresh : function() {
			// console.log('refresh')
			this.clearScreen();
			this.initCT();
			this.drawMessage();
			this.drawFrameRate();
			if (this.Domain.readyToRender){
				this.applyViewPortTransform();
				this.drawDomain();
			}
			this.drawInputStrokes();
			var thisFrameFPS = 1000 / ((now=new Date) - lastUpdate);
		  	fps += (thisFrameFPS - fps) / fpsFilter;
		  	lastUpdate = now;
		  	var temp = this;
		
		  	setTimeout( function(){
		  		temp.refresh();
		  	}, 1000/temp.settings.fps);
		},
		onOrientationchange : function() {
			this.setCanvasPosition(0, this.views.topBar.getHeight(), this.viewport.getWidth(), this.viewport.getHeight() - this.views.topBar.getHeight() - this.views.bottomBar.getHeight());
			this.resetViewPort();
			// this.refresh();
		},
		onDoubleTap : function(e, el, obj) {
			this.resetViewPort();
			// this.refresh();
		},
		animate : function(condition, fn, dt) {
			setTimeout(function(scope) {
				fn.call(scope);
				if(condition.call(scope)) {
					scope.animate(condition, fn, dt)
				}
			}, dt, this);
		},
		animate : function(condition, fn, dt) {
			var id = setInterval(function(scope) {
				fn.call(scope);
				if(!condition.call(scope)) {
					clearInterval(id);
				}
			}, dt, this)
		},
		onTouchStart : function(e, el, obj) {
			// console.log("touch start!",e)
			var P = this.getCanvasCoordFromPage({
				X : e.touches[0].pageX,
				Y : e.touches[0].pageY
			});
			this.touchCurrentX = P.X;
			this.touchCurrentY = P.Y;
			this.touchStartX = P.X;
			this.touchStartY = P.Y;
			this.shiftKey = e.event.shiftKey;
			if(this.settings.mode === "move") {
				if((e.event.type === "mousedown" && e.event.button == 1) || e.event.shiftKey) {

				} else {
					this.settings.touchMoveAnimation = true;
					this.analyze();
					// this.animate(function() {
						// return this.settings.touchMoveAnimation;
					// }, function() {
						// if(this.settings.autoAnalysis) {
							// this.analyze(function() {
								// // this.refresh();
							// });
						// } else {
							// // this.refresh();
						// }
					// }, 1000 / this.settings.touchMoveFps);

				}
			} else {
				this.inputStrokes = [];
				this.inputStrokes.push(P);
			}
		},
		onTouchMove : function(e, el, obj) {
			var P = this.getCanvasCoordFromPage({
				X : e.touches[0].pageX,
				Y : e.touches[0].pageY
			});
			var S = this.settings;
			if(e.event.type === "mousemove" && (e.event.button == 1 || e.event.shiftKey)) {
				this.deltaTransform(1, P.X - this.touchStartX, P.Y - this.touchStartY);
				this.recordDeltaTransform();
			} else {
				if(this.settings.mode === "move" && this.settings.touchMoveAnimation === true) {
					duplicateNodes = false;
					for(i in this.Domain.selectedNodes) {
						var np1 = this.Domain.snapToNode(this.Domain.selectedNodes[1], S.moveSnapToNodeThreshold);
						if(np1.capture) {
							duplicateNodes = true;
						}
					}
					
					
					// if($D.isDefined(this.Domain.selectedNodes[1])) {
						// var np1 = this.Domain.snapToNode(this.Domain.selectedNodes[1], S.moveSnapToNodeThreshold);
						// if(np1.capture) {
							// isAnalysisReadyToRun = false;
						// } else {
							// isAnalysisReadyToRun = true;
						// }
					// }
					this.Domain["moveSelectedObjects"](P.X - this.touchCurrentX, P.Y - this.touchCurrentY);
					if (!duplicateNodes && isAnalysisReadyToRun) {
						this.analyze();
					}
				
				
				
				} else {
					this.inputStrokes.push(P);
					// var l = this.inputStrokes.length;
					// 							this.applyInputStrokeStyle(this.settings.viewPortScale);
					// 							this.Renderer.drawLine(this.inputStrokes[l - 2], this.inputStrokes[l - 1]);
				}
			}
			this.touchCurrentX = P.X;
			this.touchCurrentY = P.Y;
		},
		onTouchEnd : function(e, el, obj) {
			// this.inputStrokes = [];
			if(this.settings.mode === "move") {
				this.settings.touchMoveAnimation = false;
			}
			if((e.event.type === "mouseup" && e.event.button == 1) || (e.event.type === "mouseup" && this.shiftKey)) {
				this.recordDeltaTransform();
				// this.refresh();
			} else if(e.touches.length === 0 || e.touches.length === 1) {
				switch (this.settings.mode) {
					case "move":
						this.afterMovingObjects();
						break;
					case "select":
						this.sketchSelect();
						break;
					case "draw":
					case "load":
						var shape = this.shapeRecognizer.Recognize(this.inputStrokes, false);
						var handler = this.sketchVocabulary[this.settings.mode][shape.name];
						if(handler && this.sketchHandlers[handler]) {
							this.sketchHandlers[handler].call(this, shape.data);
						} else {
							this.sketchHandlers["noSuchGesture"].call(this);
						}

						break;
					default:
						break;

				}
				this.Domain.group();
				this.Domain.unmark();

			}
			this.inputStrokes = [];
			this.shiftKey = e.event.shiftKey;
		},
		afterMovingObjects : function() {
			this.beforeUndoableCommand();
			var count;
			var dx = this.touchCurrentX - this.touchStartX;
			var dy = this.touchCurrentY - this.touchStartY;
			var S = this.settings;
			count = this.Domain["transitSelectedObjects"](dx, dy);
			var changed = count > 0 ? true : false;
			// test merge
			if($D.isDefined(this.Domain.selectedNodes[1])) {
				var np1 = this.Domain.snapToNode(this.Domain.selectedNodes[1], S.moveSnapToNodeThreshold);
				if(np1.capture) {
					this.Domain.mergeNodes(this.Domain.selectedNodes[1], np1.node);
				}
			}
			if(count) {
				var msg = "move " + count + " objects, dx = " + dx + " dy = " + dy;
			}
			this.afterUndoableCommand(changed, changed, msg);
			// this.refresh();
		},
		sketchSelect : function() {
			this.beforeUndoableCommand();
			var data = this.shapeRecognizer.Recognize(this.inputStrokes, false).data;
			var d = $D.distance(data.from, data.to);
			var l = data.PathLength;
			var tags;
			if(d / l > this.settings.circleSelectThreshold) {
				tags = this.Domain["intersectSelect"]({
					"curve" : data.ResamplePoints
				});
			} else {
				if(l >= this.settings.clickSelectThreshold) {
					tags = this.Domain["circleSelect"]({
						"poly" : data.ResamplePoints
					});
				}
			}
			if(tags) {
				var selectCount = this.Domain.count(tags.select);
				var unSelectCount = this.Domain.count(tags.unselect);
				var msg = "";
				if(selectCount > 0) {
					msg += "select " + selectCount + " objects; ";
				}
				if(unSelectCount > 0) {
					msg += "unselect " + unSelectCount + " objects; ";
				}
			}
			this.afterUndoableCommand(tags, msg);
			// this.refresh();
		},
		beforeUndoableCommand : function() {
			this.Domain.mark();
		},
		afterUndoableCommand : function(domainTouched, domainChanged, isUndoable) {
			var touched = $D.isDefined(domainTouched)? domainTouched:true;
			var changed = $D.isDefined(domainChanged)? domainChanged:true;
			var undoable = $D.isDefined(isUndoable)? isUndoable:true;
			this.Domain.group();
			if(touched) {
				if(changed) {
					if(!undoable) {
						this.Domain._timeline.pop();
						this.Domain._head--;
					} else {
						this.views.undoButton.setDisabled(false);
						this.views.redoButton.setDisabled(true);	
					}
				} else {
					this.Domain.undo();
					this.Domain._timeline.pop();
				}
			}
		},
		afterReanalyzeRequiredCommand: function() {
			var S = this.settings;
			if (S.autoAnalysis) {
				this.analyze(function(){
					// this.refresh();
				})
			} else {
				// this.refresh();
			}
		},
		printMessage : function(msg) {
			this.screenMessage = msg;
		},
		logMessage: function(msgobj) {
			this.logs.push(msgobj);
		},
		meshLine : function(l,n) {
			this.beforeUndoableCommand();
			var touched = true;
			var changed = true;
			var dm = this.Domain;
			var arr = [];
			if ($D.isArray(n)) {
				arr = n
			} else {
				for (var i = 1; i < n; i++) {
					arr.push(i/n);
				}
			}
			dm.splitLineElement (l, arr, true);
			var msg;
			if(changed) {
				msg = "mesh line "+ l.id + " into " + n + " segments";
				this.logMessage(msg);
			} else {
				msg = "do nothing";
			}
			this.afterUndoableCommand(touched,changed,true);
			this.printMessage(msg);
			this.afterReanalyzeRequiredCommand();
		},
		
		autoMesh: function(){
			this.beforeUndoableCommand();
			var touched = true;
			var changed = true;
			var dm = this.Domain;
			var sz = this.settings.autoMeshSize;
			var ratio = [];
			var n;
			var l;
			var ls = [];
			
			for (var i in dm.theElements) {
				if(dm.theElements.hasOwnProperty(i)) {
					ls.push(dm.theElements[i])
				}
			};
			
			for (var i=0; i < ls.length; i++) {
				l = ls[i];
				n = Math.round(l.getLength()/sz);
				ratio = [];
				for (var j=0; j < n-1; j++) {
					ratio.push((j+1)/n);
				};
				dm.splitLineElement (l, ratio, true);
			};

			var msg;
			if(changed) {
				msg = "auto mesh with size = "+ sz;
				this.logMessage(msg);
			} else {
				msg = "do nothing";
			}
			this.afterUndoableCommand(touched,changed,true);
			this.printMessage(msg);
			this.afterReanalyzeRequiredCommand();
			
		},
		
		sketchVocabulary : {
			"draw" : {
				"line" : "drawLine",
				"triangle" : "drawTriangle",
				"circle" : "drawCircle",
			},
			"load" : {
				"line" : "loadLine",
				"squareBracket" : "loadSquareBracket"
			}
		},
		sketchHandlers : {

			"noSuchGesture" : function() {
				var msg = "no such gesture :-(";
				// this.logMessage(msg);
				this.printMessage(msg);
				// this.refresh();
			},
			"drawLine" : function(data) {
				this.beforeUndoableCommand();
				var dm = this.Domain;
				var S = this.settings;
				var fx = data.from.X;
				var fy = data.from.Y;
				var tx = data.to.X;
				var ty = data.to.Y;
				var n1 = dm.createNode(fx, fy);
				var n2 = dm.createNode(tx, ty);
				var changed = false;
				var touched = true;
				var nt = S.snapToNodeThreshold / S.viewPortScale;
				var lt = S.snapToLineThreshold / S.viewPortScale;
				var at = S.autoMergeNodeOnLineThreshold / S.viewPortScale
				if(S.snapToNode) {
					var np1 = dm.snapToNode(n1, nt);
					var np2 = dm.snapToNode(n2, nt);
					// var np1 = dm.snapToNode(n1, S.snapToNodeThreshold);
					// var np2 = dm.snapToNode(n2, S.snapToNodeThreshold);
					if(np1.capture && np2.capture) {
						if(np1.nodeId == n2.id || np2.nodeId == n1.id || np1.nodeId == np2.nodeId) {
							// dm.removeLineElement(e);
							// dm.removeNode(n1);
							// dm.removeNode(n2);
							changed = false;
						} else {
							n1 = dm.mergeNodes(n1, np1.node);
							n2 = dm.mergeNodes(n2, np2.node);
							// touched = true;
							changed = true;
						}
					} else if(np1.capture && !np2.capture) {
						n1 = dm.mergeNodes(n1, np1.node);
						// touched = true;
						changed = true;
					} else if(!np1.capture && np2.capture) {
						n2 = dm.mergeNodes(n2, np2.node);
						// touched = true;
						changed = true;
					} else {
						// touched = true;
						changed = true;
					}
				};
				
				if (!S.snapToNode || (!(np1.capture && np2.capture) && S.snapToLine)) {
					var nl1 = dm.snapToLine(n1, lt);
					var nl2 = dm.snapToLine(n2, lt);
					// var nl1 = dm.snapToLine(n1, S.snapToLineThreshold);
					// var nl2 = dm.snapToLine(n2, S.snapToLineThreshold);
					if ((nl1.capture && nl2.capture) && (nl1.lineId == nl2.lineId)) {
						changed = false
					} else {
						if (nl1.capture && Math.abs(nl1.ratio -1.0) > 0.01 && Math.abs(nl1.ratio) > 0.01){
							var nonnl1 = dm.splitLineElement(nl1.line,nl1.ratio);
							n1 = dm.mergeNodes(n1,nonnl1);
						}; 
						if (nl2.capture && Math.abs(nl2.ratio -1.0) > 0.01 && Math.abs(nl2.ratio) > 0.01) {
							var nonnl2 = dm.splitLineElement(nl2.line,nl2.ratio);
							n2 = dm.mergeNodes(n2,nonnl2);
						};
					};
				};
				
				if (S.snapToGrid) {
					var grid = S.grid;
					dm.transitNode(["theNodes",n1.id], Math.round(n1.X / grid) * grid - n1.X, Math.round(n1.Y / grid) * grid - n1.Y); 
					dm.transitNode(["theNodes",n2.id], Math.round(n2.X / grid) * grid - n2.X, Math.round(n2.Y / grid) * grid - n2.Y); 
					$D.iterate(dm.theNodes,function(n){
						if (n.id != n1.id && $D.distance(n,n1) < 0.01) {
							n1 = dm.mergeNodes(n1, n);
						};
						if (n.id != n2.id && $D.distance(n,n2) < 0.01) {
							n2 = dm.mergeNodes(n2, n);
						};
						
					});
					// dm.transitNode(n2.id, n2.X - Math.round(n2.X / grid) * grid, n2.Y - Math.round(n2.Y / grid) * grid); 
				};
				if (n1.id != n2.id) {
					var e = dm.createLineElement(S.defaultLineELementType, n1, n2, {
						geomTransf : dm.theGeomTransfs[S.defaultGeomTransfId]
					});
				} else {
					changed = false;
				}
				
				if (changed) {
					var ratios = [];
					var ns = [];
					$D.iterate(dm.theNodes,function(n){
						if (n.id != e.getFrom().id && n.id != e.getEnd().id) {
							var nl = dm.snapToLine(n, at);
							if (nl.capture && nl.lineId == e.id) {
							// if (nl.capture) {
								ratios.push(nl.ratio);
								ns.push(n);
							};
							
						}
					});
					if (ns.length > 0) {
						var narr = dm.splitLineElement(e, ratios);
						if (!$D.isArray(narr)) {
							narr = [narr]
						};
						for (var i=0; i < ns.length; i++) {
							dm.mergeNodes(ns[i],narr[i]);
							if (ns[i].SPC) {
								dm.set(["theNodes", narr[i].id, "SPC"], ns[i].SPC);
								dm.set(["theSPCs", ns[i].SPC.id, "node"], narr[i]);
							}
						};
					};
					
				};
				
				var msg;
				if(changed) {
					msg = "add a " + e.className;
					this.logMessage(msg);
				} else {
					msg = "nodes are merged, abort";
				}
				this.afterUndoableCommand(touched,changed,true);
				this.printMessage(msg);
				this.afterReanalyzeRequiredCommand();
				// this.refresh();
			},
			"drawTriangle" : function(data) {
				this.beforeUndoableCommand();
				var dm = this.Domain;
				var S = this.settings;
				var n = dm.createNode(data.from.X, data.from.Y);
				var changed;
				var np;
				var nt = S.SPCSnapToNodeThreshold/S.viewPortScale;
				var lt = S.SPCSnapToLineThreshold/S.viewPortScale;
				
				var temp = dm.snapTo4Direction(data.IndicativeAngle, S.SPCSnapToDirectionThreshold);
				// if(temp.capture) {
					// angle = temp.angle;
					// direction = temp.direction;
				// }
				// temp = {
					// node : n,
					// angle : angle,
					// show : show
				// };
				// if($D.isDefined(direction)) {
					// temp.direction = direction;
				// }
				// spc = new SPC(temp);
				// this.addComponent({
					// storeSelector : "theSPCs",
					// item : spc
				// });
				// //this.run("addAComponent", spc, this.theSPCs);
				// this.set("theNodes." + n.id + ".SPC", spc);
				
				
				var spc = dm.createSPC(n,{
					direction: temp.dir,
					angle : temp.angle
				});
				if(S.SPCSnapToNode) {
					// np = dm.snapToNode(n, S.SPCSnapToNodeThreshold);
					np = dm.snapToNode(n, nt);
					if(np.capture) {
						if(!$D.isDefined(np.node.SPC)) {
							dm.mergeNodes(n, np.node);
							dm.set(["theNodes", np.nodeId, "SPC"], spc);
							dm.set(["theSPCs", spc.id, "node"], np.node);
							changed = true;
						} else {
							// dm.removeNode(n);
							// dm.removeSPC(spc);
							changed = false;
						}
					}
				}
				
				if (!S.snapToNode || (!(np && np.capture) && S.SPCSnapToLine)) {
					var nl = dm.snapToLine(n, lt);
					if (nl.capture){
						var nonnl = dm.splitLineElement(nl.line,nl.ratio);
						dm.mergeNodes(n,nonnl);
						dm.set(["theNodes", nonnl.id, "SPC"], spc);
						dm.set(["theSPCs", spc.id, "node"], nonnl);
						changed = true;
					}
				};

				if(changed) {
					var msg = "add a single point constraint";
					this.logMessage(msg);
				} else {
					var msg = "the point is already fixed, abort";
				}
				this.afterUndoableCommand(true, changed, true);
				this.printMessage(msg);
				this.afterReanalyzeRequiredCommand();
				// this.refresh();
			},
			"drawCircle" : function(data) {
				this.beforeUndoableCommand();
				var discard;
				var dm = this.Domain;
				var S = this.settings;
				var cen = data.Centroid;
				var top;
				var d = S.circleSnapToSPCThreshold + 10;
				var theSPC;
				var changed = false;
				$D.iterate(dm.theSPCs, function(spc) {
					var d1 = $D.distance(spc.node, cen);
					var d2 = $D.distance(spc.getBottomCenter(), cen);
					var tmp = (d1 < d2) ? d1 : d2;
					if(tmp < d) {
						d = tmp;
						top = (d1 < d2) ? true : false;
						theSPC = spc;
					}
				})
				if(theSPC && d < S.circleSnapToSPCThreshold) {
					if(top) {
						if(theSPC.RZ == 1) {
							dm.set(["theSPCs", theSPC.id, "RZ"], 0);
						}
					} else {
						if(theSPC.direction === "up" || theSPC.direction === "down") {
							if(theSPC.X == 1) {
								dm.set(["theSPCs", theSPC.id, "X"], 0);
							}
						} else if(theSPC.direction === "left" || theSPC.direction === "right") {
							if(theSPC.Y == 1) {
								dm.set(["theSPCs", theSPC.id, "Y"], 0);
							}
						}
					}
					changed = true;
				}

				var msg;
				if(changed) {
					if(top) {
						msg = "make a pin";
					} else {
						msg = "make a roller";
					}
					this.logMessage(msg);
				} else {
					msg = "do nothing";
				}
				
				var touched = changed;
				this.afterUndoableCommand(touched, changed, true);
				this.printMessage(msg);
				this.afterReanalyzeRequiredCommand();
				// this.refresh();
			},
			
			"loadLine" : function(data) {
				this.beforeUndoableCommand();
				var dm = this.Domain;
				var touched;
				var changed;
				var S = this.settings;
				var node;
				var freeEnd;
				var nodeAtArrowEnd;
				var dx, dy;
				var nt = S.loadSnapToNodeThreshold/S.viewPortScale;
				var lt = S.loadSnapToLineThreshold/S.viewPortScale;
				var np1 = dm.snapToNode(data.to, nt);
				var addOnNode = false;
				var uniformLoad = false;
				if (S.loadSnapToNode) {
					if (np1.capture) {
						node = np1.node;
						freeEnd = new $D.Node({
							X:data.from.X,
							Y:data.from.Y
						});
						
						nodeAtArrowEnd = true;
						dx = node.X - freeEnd.X;
						dy = node.Y - freeEnd.Y;
						addOnNode = true;

					} else {
						var np2 = dm.snapToNode(data.from, nt); 
						if (np2.capture) {
							node = np2.node;
							// freeEnd = data.to;
							freeEnd = new $D.Node({
								X:data.to.X,
								Y:data.to.Y
							});
							nodeAtArrowEnd = false;
							dx = freeEnd.X - node.X;
							dy = freeEnd.Y - node.Y;
							addOnNode = true;
						};
					};
				}
				
				
				if (!node && S.loadSnapToLine) {
					var nl = dm.snapToLine(data.to, lt);
					if (nl.capture){
						node = dm.splitLineElement(nl.line, nl.ratio, true);
						// freeEnd = data.from;
						freeEnd = new $D.Node({
							X:data.from.X,
							Y:data.from.Y,
							// constraintDirection:{
								// X : nl.line.getDx(),
								// Y : nl.line.getDy()
							// },
							// constraintOnLine:new $D.LineElement({
								// nodes : [{
									// X : nl.line.getFrom().X - data.to.X + data.from.X,
									// Y : nl.line.getFrom().Y - data.to.Y + data.from.Y
								// },{
									// X : nl.line.getEnd().X - data.to.X + data.from.X,
									// Y : nl.line.getEnd().Y - data.to.Y + data.from.Y
								// }]
							// })
						});
						nodeAtArrowEnd = true;
						dx = node.X - freeEnd.X;
						dy = node.Y - freeEnd.Y;
					} else {
						nl = dm.snapToLine(data.from, lt);
						if (nl.capture) {
							node = dm.splitLineElement(nl.line, nl.ratio, true);
							// freeEnd = data.to;
							freeEnd = new $D.Node({
								X:data.to.X,
								Y:data.to.Y,
								// constraintDirection:{
									// X : nl.line.getDx(),
									// Y : nl.line.getDy()
								// },
								// constraintOnLine:new $D.LineElement({
									// nodes : [{
										// X : nl.line.getFrom().X + data.to.X - data.from.X,
										// Y : nl.line.getFrom().Y + data.to.Y - data.from.Y
									// },{
										// X : nl.line.getEnd().X + data.to.X - data.from.X,
										// Y : nl.line.getEnd().Y + data.to.Y - data.from.Y
									// }]
								// })
							});
							nodeAtArrowEnd = false;
							dx = freeEnd.X - node.X;
							dy = freeEnd.Y - node.Y;
						}
					}
				};
				
				var load;
				if (node) {
					load = dm.createNodeLoad(node, freeEnd, nodeAtArrowEnd);
					// load = dm.createNodeLoad(node, freeEnd, nodeAtArrowEnd, dx, dy, 0.0);
					touched = true;
					changed = true;
					if (S.snapToGrid) {
						var grid = S.grid;
						
						if (load) {
							if (node) {
								dm.transitNode(["theNodes",node.id], Math.round(node.X / grid) * grid - node.X, Math.round(node.Y / grid) * grid - node.Y); 
								// dm.set(["theNodes",node.id,"X"],Math.round(node.X / grid) * grid)
								// dm.set(["theNodes",node.id, "Y"],Math.round(node.Y / grid) * grid)
								node.X = dm.theNodes[node.id].X;
								node.Y = dm.theNodes[node.id].Y;
								$D.iterate(dm.theNodes,function(n){
									if (n.id != node.id && $D.distance(n,node) < 0.01) {
										node = dm.mergeNodes(node, n);
									};
								});
							} 
							if (freeEnd) {
								dm.transitNode(["currentPattern","Loads",load.id,"freeEnd"], Math.round(freeEnd.X / grid) * grid - freeEnd.X, Math.round(freeEnd.Y / grid) * grid - freeEnd.Y); 
								freeEnd.X = load.freeEnd.X;
								freeEnd.Y = load.freeEnd.Y;
								// freeEnd.X = Math.round(freeEnd.X / grid) * grid;
								// freeEnd.Y = Math.round(freeEnd.Y / grid) * grid;
								// dm.set(["currentPattern","Loads",load.id,"freeEnd","X"],Math.round(freeEnd.X / grid) * grid)
								// dm.set(["currentPattern","Loads",load.id,"freeEnd","Y"],Math.round(freeEnd.Y / grid) * grid )
								$D.iterate(dm.theNodes,function(n){
									if (n.id != freeEnd.id && $D.distance(n,freeEnd) < 0.01) {
										freeEnd = dm.mergeNodes(freeEnd, n);
									};
								});
							} 
						}
						
					}
					if (freeEnd && !addOnNode) {
						dm.set(["currentPattern","Loads",load.id,"freeEnd","constraintOnLine"],new $D.LineElement({
							nodes : [{
								X : nl.line.getFrom().X + freeEnd.X - node.X,
								Y : nl.line.getFrom().Y + freeEnd.Y - node.Y,
							},{
								X : nl.line.getEnd().X + freeEnd.X - node.X,
								Y : nl.line.getEnd().Y + freeEnd.Y - node.Y,
							}]
						}))
					}
				} else {
					if (S.analysisType == 'Static') {
						$D.iterate(dm.theNodes,function(n){
							if (!$D.isDefined(n.SPC)) {
								var fE = new $D.Node({
									X:n.X + data.to.X - data.from.X,
									Y:n.Y + data.to.Y - data.from.Y
								});
								load = dm.createNodeLoad(n, fE, false);
								touched = true;
								changed = true;
								
							}
						})
					} else if (S.analysisType == 'Dynamic'){
						var n1 = new $D.Node({
							X:data.from.X,
							Y:data.from.Y
						});
						var n2 = new $D.Node({
							X:data.to.X,
							Y:data.to.Y
						});
						
						
						load = dm.createUniformLoad(n1,n2);
						
						touched = true;
						changed = true;
						
					} else {
						touched = false;
						changed = false;
						
					}
					uniformLoad = true;
					// touched = true;
					// changed = true;
					// S.dynamicsAnalysis = true;
					// S.analysisType = ''
					
					// S.autoAnalysis = false;
				}
				
				
				isAnalysisReadyToRun = true;
				var msg;
				if(changed) {
					if (uniformLoad) {
						msg = "add uniform loads"
					} else {
						msg = "add a point load";
					}
				} else {
					msg = "do nothing";
				}
				this.afterUndoableCommand(touched, changed, msg);
				this.printMessage(msg);
				this.afterReanalyzeRequiredCommand();
				// this.refresh();
			},
			"loadSquareBracket" : function(data) {
				this.beforeUndoableCommand();
				var flag;

				if(flag) {
					var msg = "add a uniform distributed load";
				}
				this.afterUndoableCommand(flag, msg);
				// this.refresh();
			},
		},
		onPinchStart : function(e, el, obj) {
			var first = this.getCanvasCoordFromPage({
				X : e.touches[0].pageX,
				Y : e.touches[0].pageY
			}), second = this.getCanvasCoordFromPage({
				X : e.touches[1].pageX,
				Y : e.touches[1].pageY
			});
			this.pinchCenter0 = {
				X : 0.5 * (first.X + second.X),
				Y : 0.5 * (first.Y + second.Y)
			};
		},
		onPinch : function(e, el, obj) {
			if(!$D.isDefined(e.touches[0]) || !$D.isDefined(e.touches[1])) {
				return;
			}
			var first = this.getCanvasCoordFromPage({
				X : e.touches[0].pageX,
				Y : e.touches[0].pageY
			}), second = this.getCanvasCoordFromPage({
				X : e.touches[1].pageX,
				Y : e.touches[1].pageY
			}), s = e.scale, S = this.settings;
			this.pinchCenter1 = {
				X : 0.5 * (first.X + second.X),
				Y : 0.5 * (first.Y + second.Y)
			};
			if(S.viewPortScale * s < S.maxViewPortScale && S.viewPortScale * s > S.minViewPortScale) {
				this.deltaTransform(s, this.pinchCenter1.X - this.pinchCenter0.X * s, this.pinchCenter1.Y - this.pinchCenter0.Y * s);
			} else if(S.viewPortScale * s >= S.maxViewPortScale) {
				alert("max scale reached");
			} else {
				alert("min scale reached");
			}
		},
		onMouseWheel : function(event) {
			// console.log("event",event)
			var e = event.browserEvent, //
			S = this.settings, //
			s = Math.pow(Math.E, e.wheelDelta * S.mouseWheelSpeedFactor / S.mouseWheelSpeedBase), //
			P = this.getCanvasCoordFromPage({
				X : e.pageX,
				Y : e.pageY
			});

			if(S.viewPortScale * s < S.maxViewPortScale && S.viewPortScale * s > S.minViewPortScale) {
				S.viewPortShiftX = S.viewPortScale * P.X * (1 - s) + S.viewPortShiftX;
				S.viewPortShiftY = S.viewPortScale * P.Y * (1 - s) + S.viewPortShiftY;
				S.viewPortScale = S.viewPortScale * s;
			} else if(S.viewPortScale * s >= S.maxViewPortScale) {
				alert("max scale reached");
			} else {
				alert("min scale reached");
			}

			// this.refresh();
		},
		onPinchEnd : function() {
			this.recordDeltaTransform();
			// this.refresh();
		},
		autoScale: function() {
			this.settings.autoDeformationScale = true;
			this.settings.autoMomentScale = true;
			this.analyze(function(){
				// this.refresh();
			})
			
		},
		clearAll : function() {
			var r = confirm("Restart the sketch: you can not undo this operation, are you sure?");
			if(r === true) {
				this.Domain.restart();
				this.views.undoButton.setDisabled(true);
				this.views.redoButton.setDisabled(true);
				// this.refresh();
			}
		},
		undo : function() {
			this.Domain.undo();
			this.views.redoButton.setDisabled(false);
			if(this.Domain._head === -1) {
				this.views.undoButton.setDisabled(true);
			}

			if(this.settings.autoAnalysis) {
				this.analyze(function() {
					// this.refresh();
				});
			} else {
				// this.refresh();
			}
			this.logMessage("undo");

		},
		redo : function() {
			this.Domain.redo();
			this.views.undoButton.setDisabled(false);
			if(this.Domain._head === this.Domain._timeline.length - 1) {
				this.views.redoButton.setDisabled(true);
			}

			if(this.settings.autoAnalysis) {
				this.analyze(function() {
					// this.refresh();
				});
			} else {
				// this.refresh();
			}
			this.logMessage("redo");
		},
		saveScript : function() {
			// alert(this.Domain.runStaticConstant(this.settings.modelScale, this.settings.loadScale));
			if (this.views.exportModelPanel.isHidden()) {
				this.views.exportModelPanel.show();
				this.views.exportModelPanel.getComponent(0).setValue(this.Domain.runStaticConstant())
				var J=this.Domain.exportToJSON();
				console.log(J)
				this.views.exportModelPanel.getComponent(1).setValue(JSON.stringify(J))
			} else {
				this.views.exportModelPanel.hide();
			}
		},
		showLog: function() {
			var str = "";
			str += "logs:\n";
			for (var i = 0; i < this.logs.length; i++) {
				str += this.logs[i] + "\n";
			}
			alert(str);
		},
		analyze : function() {
			var str = "";
			if (this.settings.analysisType == 'Dynamic') {
				str = this.Domain.runDynamics(this.settings.dynamicAnalysisNsteps);
				// this.ws.send(this.Domain.runDynamics(this.settings.dynamicAnalysisNsteps));
				// this.ws.send("2]");
			} else if (this.settings.analysisType == 'Static') {
				str = this.Domain.runStaticConstant();
				// this.ws.send(this.Domain.runStaticConstant());
			}
			
			// avoid interupt inside a command
			var getSubstrIndex=function(str){
				var index = 4000;
				while(str[index]!=="\n" && str[index]!==";" && index>0) {
					index--;
				}
				console.log('; is at '+index);
				return index;
			};
			
			while(str.length>4000){
				var index = getSubstrIndex(str);
				this.ws.send(str.slice(0,index+1));
				str = str.slice(index+1);
				
			}
			
			
			this.ws.send(str);
		},
		nodeSnapAnimate : function(nodesBefore, nodesAfter, fn) {
			var count = 0, dt = 1000 / this.settings.snapAnimationFps, dx = [], dy = [], it, i, len = nodesBefore.length;
			max = this.settings.snapAnimationElapse * this.settings.snapAnimationFps / 1000;
			for( i = 0; i < len; i++) {
				dx.push((nodesAfter[i].X - nodesBefore[i].X) / max);
				dy.push((nodesAfter[i].Y - nodesBefore[i].Y) / max);
			}
			it = setInterval(function(scope) {
				for( i = 0; i < len; i++) {
					scope.Domain.moveNode(nodesBefore[i].id, dx[i], dy[i]);
				}
				// scope.refresh()
				count++;
				if(count >= max) {
					clearInterval(it);
					fn.call(scope)
				}
			}, dt, this)
		}
	});
})();
