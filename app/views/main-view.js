sketchit.views.Bottombar = Ext.extend(Ext.Toolbar, {
	initComponent : function() {
		Ext.apply(this, {
			dock : "bottom",
			ui : "light",
			defaults : {
				iconMask : true,
				iconAlign : 'top',
			},
			scroll : {
				direction : 'horizontal',
				useIndicators : false
			},
			items : [{
				text : 'setting',
				width : 45,
				iconCls : 'settings3',
				style : {
					"font-size" : "80%"
				}
			}, {
				width : 45,
				text : 'run',
				iconCls : 'run',
				style : {
					"font-size" : "80%"
				}
			}, {
				text : 'rescale',
				width : 45,
				iconCls : 'contract',
				style : {
					"font-size" : "80%"
				}
			}, {
				text : 'mesh',
				iconCls : 'mesh',
				width : 45,
				style : {
					"font-size" : "80%"
				}
			}, {
				xtype : 'spacer',
			}, {
				ui : "plain",
				iconCls : 'refresh5',
			}, {
				ui : "plain",
				iconCls : 'unselect'
			}, {
				iconCls : 'trash',
				ui : "plain",
			}, {
				ui : "plain",
				iconCls : 'undo',
			}, {
				ui : "plain",
				iconCls : 'redo',
			}, {
				ui : "plain",
				iconCls : 'save'
			}, {
				ui : "plain",
				iconCls : 'monitor1',
			}, {
				iconCls : 'log',
				ui : "plain",
			}, {
				xtype : 'spacer',
			}, {
				xtype : 'segmentedbutton',
				defaults : {
					iconMask : true,
					iconAlign : 'top',
					width : 45,
					style : {
						"font-size" : "80%"
					}
				},
				items : [{
					text : 'draw',
					iconCls : 'draw',
					pressed : true,
				}, {
					text : 'select',
					iconCls : 'select',
				}, {
					text : 'load',
					iconCls : 'load',
				}, {
					text : 'move',
					iconCls : 'move',
				}]
			}]
		});

		sketchit.views.Bottombar.superclass.initComponent.apply(this, arguments);
	}
});
Ext.reg('sketchitBottombar', sketchit.views.Bottombar);


sketchit.views.Topbar = Ext.extend(Ext.Toolbar, {
	initComponent : function() {
		Ext.apply(this, {
			dock : "top",
			layout : "hbox",
			ui : "light",
			defaults : {
				iconMask : true,
				ui : "plain",
			},
			scroll : {
				direction : 'horizontal',
				useIndicators : false
			},
			items : [{
				xtype : 'segmentedbutton',
				items : [{
					text : 'Static',
					pressed : true,
				}, {
					text : 'Dynamic',
				}, {
					text : 'Eigen',
				}]
			}]
		});
		sketchit.views.Topbar.superclass.initComponent.apply(this, arguments);
	}
});
Ext.reg('sketchitTopbar', sketchit.views.Topbar);


sketchit.views.Canvas = Ext.extend(Ext.Panel, {

	layout : 'fit',
	Renderer : undefined,

	style : {
		"background-color" : "white"
	},

});
Ext.reg('sketchitCanvas', sketchit.views.Canvas);

sketchit.views.AnimateWidget = Ext.extend(Ext.Toolbar,{
	floating: true,
	draggable: true,
	centered: true,
	// width : 200,
	hight : 100,
	hideOnMaskTap : false,
	layout: {
		// type : "hbox"
	},
	defaults : {
		xtype : 'button',
		iconMask : true
		// iconAlign : 'top',
	},
	items : [{
		// text : 'play',
		iconCls: 'ButtonFFW'
		
	},{
		iconCls : 'ButtonStop'
	},{
		iconCls : 'ButtonRemove'
	}]
	
	
})
Ext.reg('sketchitAnimateWidget', sketchit.views.AnimateWidget);


sketchit.views.Main = Ext.extend(Ext.Panel, {

	layout : 'fit',
	fullscreen : true,

	items : [{
		xtype : 'sketchitCanvas',
		html : '<canvas id="workspace"  width="' + 1500 + '" height="' + 1500 + '">no canvas support</canvas>',
	}],

	dockedItems : [{
		xtype : 'sketchitTopbar'
	}, {
		xtype : 'sketchitBottombar'
	}],
});
sketchitMainView = sketchit.views.Main;



