module("WebSocket", {
	setup : function() {
		var url = document.location.href;
		this.domain = url.replace(/(http|https):\/\//,"").split("/")[0].split(":")[0];
		this.ws = new WebSocket("ws://" + this.domain + ":8080");
		//this.ws = new WebSocket("ws://"+this.domain+":8080","directFEA");

	}
});

test("basic communication", function() {
	stop();
	var ws = this.ws;
	ws.onopen = function() {
		console.log("open");
		ws.send("test");
	};

	ws.onmessage = function(event) {
		console.log(event.data);
		equals(event.data, "from server:test");
		start();

	};

	ws.onclose = function(event) {
		console.log("close");
	};

	window.ws = ws;

});
