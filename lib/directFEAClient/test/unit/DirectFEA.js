module("DirectFEA-Basic", {
	setup : function() {

	}
});

test("method:vectorAngle", function() {
	var $D = DirectFEA;
	ok(Math.abs($D.vectorAngle(10.0, 0.0) - 0.0) < 0.001);
	ok(Math.abs($D.vectorAngle(10.0, 10.0) - Math.PI / 4) < 0.001);
	ok(Math.abs($D.vectorAngle(0.0, 10.0) - Math.PI / 2) < 0.001);
	ok(Math.abs($D.vectorAngle(-10.0, 0.01) - Math.PI) < 0.001);
	ok(Math.abs($D.vectorAngle(-10.0, -0.01) + Math.PI) < 0.001);
	ok(Math.abs($D.vectorAngle(0.0, -10.0) + Math.PI / 2) < 0.001);
	ok(Math.abs($D.vectorAngle(-10.0, -10.0) + 3 * Math.PI / 4) < 0.001);
});
test("method:dotLineLength", function() {
	var $D = DirectFEA;
	ok(Math.abs($D.dotLineLength(0.0, 3.0, 0.0, 0.0, 4.0, 3.0) - 2.4) < 0.001);

});
// test("method:ajaxPost", function() {
	// var $D = DirectFEA;
	// $D.ajaxPost({
		// url : "/cgi-bin/lge/test-server/test.tcl",
		// success : function(text) {
			// console.log("response", text);
			// equals(text,"You sent:\nhello server\n");	
		// },
		// data:"hello server"
	// });
// });
test("method:isEmptyObject", function() {
	var $D = DirectFEA;
	ok($D.isEmptyObject({}));
	ok(!$D.isEmptyObject({key:"value"}));
	//ok($D.isEmptyObject({}));

});
test("method:lineSegmentIntersection", function() {
	var $D = DirectFEA,result;
	result = $D.lineSegmentIntersection(1.0,1.0,2.0,2.0,1.0,2.0,2.0,1.0);
	ok(Math.abs(result.X-1.5)<0.00001);
	
	result = $D.lineSegmentIntersection(1.0,1.0,2.0,2.0,3.0,2.0,3.0,1.0);
	ok(result === false);

});
/*

 test("method:apply", function() {

 });

 test("method:applyIf", function() {

 });

 test("method:clone", function() {

 });

 test("method:merge", function() {

 });

 test("method:extend", function() {

 });

 test("method:override", function() {

 });

 test("method:isArray", function() {

 });

 test("method:isBoolean", function() {

 });

 test("method:isDate", function() {

 });

 test("method:isDefined", function() {

 });

 test("method:isEmpty", function() {

 });

 test("method:isFunction", function() {

 });

 test("method:isNumber", function() {

 });

 test("method:isObject", function() {

 });

 test("method:isString", function() {

 });*/
