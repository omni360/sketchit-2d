module("Domain", {
	setup : function() {
		//this.Domain=new DirectFEA.Domain();
		var $D = DirectFEA;
		console.log("$D", $D)

		this.dm = window.Domain = new $D.Domain();
		var dm = this.dm;
		console.log("domain", this.dm)
		var n1 = dm.createNode(100, 100), n2 = dm.createNode(200, 300), n3 = dm.createNode(400, 300), n4 = dm.createNode(400, 100);
		console.log("the nodes", dm.theNodes);
		var l1 = dm.createLineElement("ElasticBeamColumn", n1, n2, {
			geomTransf : dm.theGeomTransfs[2]
		}), l2 = dm.createLineElement("ElasticBeamColumn", n2, n3, {
			geomTransf : dm.theGeomTransfs[2]
		}), l3 = dm.createLineElement("ElasticBeamColumn", n2, n4, {
			geomTransf : dm.theGeomTransfs[2]
		}), l4 = dm.createLineElement("ElasticBeamColumn", n3, n4, {
			geomTransf : dm.theGeomTransfs[2]
		});
		console.log("the elements", dm.theElements);
		var s1 = dm.createSPC(n1, {
			RZ : 0
		}), s2 = dm.createSPC(n4);

		console.log("the SPCs", dm.theSPCs);

	}
});

test("method:create components", function() {
	var dm = this.dm, l1 = dm.theElements[1], l3 = dm.theElements[3];

	equals(dm.theElements[3].getLocalNodeId(3), -1);
	equals(dm.theElements[3].getLocalNodeId(2), 0);
	equals(dm.theElements[3].getLocalNodeId(4), 1);
	equals(dm.theElements[1].getLocalNodeId(2), 1);
	equals(dm._getLocalNodeId(2, 3), 0);
	equals(dm._getLocalNodeId(2, 1), 1);
	equals(dm._getLocalNodeId(3, 3), -1);

	// nodes in element
	equals(dm.theElements[2].nodes[0].id, dm.theNodes[2].id)
	equals(dm.theElements[2].nodes[1].id, dm.theNodes[3].id)
	equals(dm.theElements[2].getFrom().id, dm.theNodes[2].id)
	equals(dm.theElements[2].getEnd().id, dm.theNodes[3].id)

	// element counts and elements
	equals(dm.theNodes[1].eleCount, 1)
	equals(dm.theNodes[2].eleCount, 3)
	equals(dm.theNodes[2].elements[3].id, dm.theElements[3].id)
	ok(dm.theNodes[2].elements[2])
	ok(dm.theNodes[2].elements[1])
	ok(!dm.theNodes[2].elements[4])

	ok(dm.theNodes[4].elements[3])
	ok(dm.theNodes[4].elements[4])
	ok(!dm.theNodes[4].elements[1])

	// ajacent nodes

	equals(dm.theNodes[3].ajacentNodes[4].X, 400)
	ok(dm.theNodes[2].ajacentNodes[1])
	ok(dm.theNodes[2].ajacentNodes[3])
	ok(dm.theNodes[2].ajacentNodes[4])
	ok(!dm.theNodes[4].ajacentNodes[1])
	ok(dm.theNodes[4].ajacentNodes[2])
	ok(dm.theNodes[4].ajacentNodes[3])

	// dm.commit();
	dm.mark();
	dm.removeLineElement(l3);
	dm.group();
	// dm.commit();
	ok(!dm.theElements[3]);

	// element counts and elements
	equals(dm.theNodes[1].eleCount, 1)
	equals(dm.theNodes[2].eleCount, 2)
	ok(!dm.theNodes[2].elements[3])
	ok(dm.theNodes[2].elements[2])
	ok(dm.theNodes[2].elements[1])
	ok(!dm.theNodes[2].elements[4])

	ok(!dm.theNodes[4].elements[3])
	ok(dm.theNodes[4].elements[4])
	ok(!dm.theNodes[4].elements[1])

	// ajacent nodes

	equals(dm.theNodes[3].ajacentNodes[4].X, 400)
	ok(dm.theNodes[2].ajacentNodes[1])
	ok(dm.theNodes[2].ajacentNodes[3])
	ok(!dm.theNodes[2].ajacentNodes[4])
	ok(!dm.theNodes[4].ajacentNodes[1])
	ok(!dm.theNodes[4].ajacentNodes[2])
	ok(dm.theNodes[4].ajacentNodes[3])

	dm.undo();
	// console.log("stupid",dm.theNodes[1].eleCount)

	// element counts and elements
	ok(dm.theNodes[1].eleCount === 1)
	ok(dm.theNodes[2].eleCount === 3)
	equals(dm.theNodes[2].elements[3].id, dm.theElements[3].id)
	ok(dm.theNodes[2].elements[2])
	ok(dm.theNodes[2].elements[1])
	ok(!dm.theNodes[2].elements[4])

	ok(dm.theNodes[4].elements[3])
	ok(dm.theNodes[4].elements[4])
	ok(!dm.theNodes[4].elements[1])

	// ajacent nodes
	equals(dm.theNodes[3].ajacentNodes[4].X, 400)
	ok(dm.theNodes[2].ajacentNodes[1])
	ok(dm.theNodes[2].ajacentNodes[3])
	ok(dm.theNodes[2].ajacentNodes[4])
	ok(!dm.theNodes[4].ajacentNodes[1])
	ok(dm.theNodes[4].ajacentNodes[2])
	ok(dm.theNodes[4].ajacentNodes[3])

	dm.redo();

	// element counts and elements
	equals(dm.theNodes[1].eleCount, 1)
	equals(dm.theNodes[2].eleCount, 2)
	ok(!dm.theNodes[2].elements[3])
	ok(dm.theNodes[2].elements[2])
	ok(dm.theNodes[2].elements[1])
	ok(!dm.theNodes[2].elements[4])

	ok(!dm.theNodes[4].elements[3])
	ok(dm.theNodes[4].elements[4])
	ok(!dm.theNodes[4].elements[1])

	// ajacent nodes

	equals(dm.theNodes[3].ajacentNodes[4].X, 400)
	ok(dm.theNodes[2].ajacentNodes[1])
	ok(dm.theNodes[2].ajacentNodes[3])
	ok(!dm.theNodes[2].ajacentNodes[4])
	ok(!dm.theNodes[4].ajacentNodes[1])
	ok(!dm.theNodes[4].ajacentNodes[2])
	ok(dm.theNodes[4].ajacentNodes[3])

	dm.undo();

	var p = dm.splitLineElement(l1, 0.2);
	ok(p.X == 120, "ratio = 0.2");

	ok(dm.theNodes[1].eleCount === 1)
	ok(dm.theNodes[2].eleCount === 3)
	equals(dm.theNodes[2].elements[3].id, dm.theElements[3].id)
	ok(dm.theNodes[2].elements[2])
	ok(dm.theNodes[2].elements[5])
	ok(!dm.theNodes[2].elements[1])
	ok(!dm.theNodes[2].elements[4])

	ok(dm.theNodes[4].elements[3])
	ok(dm.theNodes[4].elements[4])
	ok(!dm.theNodes[4].elements[1])

	// ajacent nodes
	equals(dm.theNodes[3].ajacentNodes[4].X, 400)
	ok(!dm.theNodes[2].ajacentNodes[1])
	ok(dm.theNodes[2].ajacentNodes[5])
	ok(dm.theNodes[2].ajacentNodes[3])
	ok(dm.theNodes[2].ajacentNodes[4])
	ok(!dm.theNodes[4].ajacentNodes[1])
	ok(dm.theNodes[4].ajacentNodes[2])
	ok(dm.theNodes[4].ajacentNodes[3])
});
test("method:merge nodes", function() {
	var dm = this.dm, e = dm.theElements, n = dm.theNodes;
	dm.createNode(200, 100);
	dm.createNode(100, 0);
	dm.createNode(200, 400);
	dm.createNode(400, 400);
	dm.createNode(500, 0);
	dm.createNode(100, 400);
	dm.createSPC(n[9]);
	
	console.log("the nodes", dm.theNodes);
	
	dm.createLineElement("ElasticBeamColumn", n[4], n[5], {
		geomTransf : dm.theGeomTransfs[2]
	});
	dm.createLineElement("ElasticBeamColumn", n[5], n[6], {
		geomTransf : dm.theGeomTransfs[2]
	});
	dm.createLineElement("ElasticBeamColumn", n[7], n[8], {
		geomTransf : dm.theGeomTransfs[2]
	});
	dm.createLineElement("ElasticBeamColumn", n[7], n[10], {
		geomTransf : dm.theGeomTransfs[2]
	});
	console.log("the elements", dm.theElements);
	
	dm.mark();
	dm.mergeNodes(n[7],n[2]);
	dm.group();
	dm.undo();

});




























