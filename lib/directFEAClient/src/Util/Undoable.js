(function() {
	var Undoable;
	Undoable = function(obj) {
		if(obj && typeof obj === 'object') {
			for(var key in obj) {
				this[key] = obj[key];
			}
		}
		this._timeline = [];
		this._unCommitChanges = [];
		this._head = -1;

	};
	//get obj referrence or value
	// Undoable.prototype.get = function(selector) {
		// var arr, value, i, len;
		// if( typeof selector === "string") {
			// arr = selector.replace(/\s/g, "").split(".")
		// } else if(Object.prototype.toString.apply(selector) === '[object Array]') {
			// arr = selector;
		// }
		// value = this;
		// i = 0;
		// len = arr.length;
		// while(i < len) {
			// value = value[arr[i]];
			// i += 1;
		// }
		// return value;
	// };
	//get obj referrence or value
	Undoable.prototype.get = function(selector) {
		if( typeof selector === "string") {
			var arr = selector.replace(/\s/g, "").split(".");
			if (arr.length === 1) {
				return this[arr[0]];
			} else if (arr.length === 0) {
				return this;
			} else {
				return Undoable.prototype.get.call(this[arr[0]],arr.slice(1));
			}
		} else if(Object.prototype.toString.apply(selector) === '[object Array]') {
			if (selector.length === 1) {
				return this[selector[0]];
			} else if (selector.length === 0) {
				return this;
			} else {
				return Undoable.prototype.get.call(this[selector[0]],selector.slice(1));
			}
		} else {
            return this;
        }
	};
	//stage the changes going to made before the change apply
	Undoable.prototype._stage = function(selector, arrOp) {
		var change;

		if( typeof selector === 'string') {
			selector = selector.replace(/\s/g, "").split(".");
		} else if(Object.prototype.toString.apply(selector) === '[object Array]') {
		} else {
			return this;
		}
		change = {};
		if(arrOp === "remove") {
			change.value = this.get(selector);
			change.key = parseInt(selector[selector.length - 1]);
			change.ref = selector.slice(0, selector.length - 1);
			change.arrOp = arrOp;
		} else if(arrOp === "insert") {
			change.key = parseInt(selector[selector.length - 1]);
			change.ref = selector.slice(0, selector.length - 1);
			change.arrOp = arrOp;
		} else {
			change.value = this.get(selector);
			change.key = selector[selector.length - 1];
			change.ref = selector.slice(0, selector.length - 1);
		}

		this._unCommitChanges.push(change)
		return this;
	};
	//commit the changes to timeline
	Undoable.prototype._commit = function() {
		if(this._unCommitChanges.length == 0) {
			return this;
		}
		this._head += 1;
		if(this._timeline.length != this._head) {
			this._timeline.splice(this._head, this._timeline.length)

		}
		var i;
		for( i = 0; i < this._unCommitChanges.length; i++) {
			this._unCommitChanges[i].newValue = this.get(this._unCommitChanges[i].ref)[this._unCommitChanges[i].key];
		}
		this._timeline.push(this._unCommitChanges);
		this._unCommitChanges = [];
		return this;
	}
	// set obj key value, if it do change the object, return true, otherwise reture false;
	// changes can be reverted by call undo()
	Undoable.prototype.set = function(selector, value) {
		var target, i, len, key;
		if( typeof selector === "string") {
			selector = selector.replace(/\s/g, "").split(".")
		} else if(Object.prototype.toString.apply(selector) === '[object Array]') {
		} else {
			return false;
		}
		if(selector.length == 0) {
			return false;
		}
		target = this;
		i = 0;
		len = selector.length - 1;
		key = selector[len]
		while(i < len) {
			if (typeof target === "undefined"){
				return false;
			}
			target = target[selector[i]];
			i += 1;
		}
		if( typeof value === "undefined") {
			if (typeof target[key] === "undefined"){
				return false;
			} else {
				this._stage(selector);
				delete (target[key]);
				this._commit();
			}
		} else {
			this._stage(selector);
			target[key] = value;
			this._commit();
		}
		return true;
	};
	//erase a obj key
	Undoable.prototype.erase = function(selector) {
		return this.set(selector);
	};
	// push obj to the end of a array, the first parameter is the selector of the array (not its elements)
	// If it do change the object, return true, otherwise reture false;
	// changes can be reverted by call undo()
	Undoable.prototype.pushTo = function(selector, value) {
		var target, i, len;
		if( typeof selector === "string") {
			selector = selector.replace(/\s/g, "").split(".")
		} else if(Object.prototype.toString.apply(selector) === '[object Array]') {
		} else {
			return false
		}
		if(selector.length == 0) {
			return false;
		}
		target = this;
		i = 0;
		len = selector.length;
		while(i < len) {
			if (typeof target === "undefined"){
				return false;
			}
			target = target[selector[i]];
			i += 1;
		}
		if (typeof target === "undefined"){
			return false;
		}
		selector.push(target.length)
		this._stage(selector, "insert");
		target.push(value);
		this._commit();
		return true;
	};
	//remove a obj from array by index,  the first parameter is the selector of the object to be deleted
	Undoable.prototype.removeAt = function(selector) {
		var target, i, len, index;
		if( typeof selector === "string") {
			selector = selector.replace(/\s/g, "").split(".")
		} else if(Object.prototype.toString.apply(selector) === '[object Array]') {
		} else {
			return false
		}
		if(selector.length == 0) {
			return false;
		}
		target = this;
		i = 0;
		len = selector.length - 1;
		index = selector[len];
		while(i < len) {
			if (typeof target === "undefined"){
				return false;
			}
			target = target[selector[i]];
			i += 1;
		}
		if (typeof target === "undefined" || typeof target[index] === "undefined"){
			return false;
		}
		if(index < target.length && index > -1) {
			this._stage(selector, "remove");
			target.splice(index, 1);
			this._commit();
			return true;
		} else {
			return false;
		}
	};
	Undoable.prototype.undo = function() {
		var changes, i;
		if(this._head > -1) {
			changes = this._timeline[this._head];
			i = changes.length;
			while(--i > -1) {
				if(changes[i].arrOp === "remove") {
					this.get(changes[i].ref).splice(changes[i].key, 0, changes[i].value);
				} else if(changes[i].arrOp === "insert") {
					this.get(changes[i].ref).splice(changes[i].key, 1);
				} else {
					if( typeof changes[i].value === "undefined") {
						delete (this.get(changes[i].ref)[changes[i].key]);
					} else {
						this.get(changes[i].ref)[changes[i].key] = changes[i].value;
					}
				}
			}
			this._head -= 1;
		}
		return this;
	};
	Undoable.prototype.redo = function() {
		var changes, i;
		if(this._head < this._timeline.length - 1) {
			this._head += 1;
			changes = this._timeline[this._head];
			i = -1;
			while(++i < changes.length) {
				if(changes[i].arrOp === "remove") {
					this.get(changes[i].ref).splice(changes[i].key, 1);
				} else if(changes[i].arrOp === "insert") {
					this.get(changes[i].ref).splice(changes[i].key, 0, changes[i].newValue);
				} else {
					this.get(changes[i].ref)[changes[i].key] = changes[i].newValue;
				}
			}
		}
		return this;
	};
	// group several operations into one
	Undoable.prototype.group = function(fr, to) {
		if (typeof fr === "undefined" && typeof to === "undefined"){
			if (typeof this._groupStart === "undefined"){
				return this;
			} else {
				fr = this._groupStart;
			}
			to = this._head;
		}
		if(fr >= to || fr > this._timeline.length - 1 || to < 0) {
			return this;
		}
		if(fr < 0) {
			fr = 0;
		}
		if(to > this._timeline.length - 1) {
			to = this._timeline.length - 1;
		}
		var gr = this._timeline[fr], i = fr + 1;
		while(i <= to) {
			gr = gr.concat(this._timeline[i]);
			i++;
		}
		if(fr < this._head && to >= this._head) {
			this._head = fr;
		} else if(to < this._head) {
			this._head -= to - fr
		}
		this._timeline.splice(fr, to - fr + 1, gr);
		this.unmark();
		return this;
	};
	// mark the start point of group operation
	Undoable.prototype.mark = function(timelineIndex) {
		if (typeof timelineIndex === "undefined"){
			this._groupStart = this._head+1;
		} else {
			this._groupStart = timelineIndex;
		}
		return true;
	};
	// clear mark
	Undoable.prototype.unmark = function() {
		if (typeof this._groupStart != "undefined"){
			delete this._groupStart;
		}
		return true;
	};
	Undoable.prototype.clearHistory = function() {
		this._timeline = [];
		this._unCommitChanges = [];
		this._head = -1;
		return this;
	};
	// Discard operations, the change is preserved, but the operation can not be undo by undo() function.
	// number n > 0; discard first n operations from the start; n = 1 means discard the very first operation
	// If no parameters, will discard the whole undo stack
	// call it when undo history is too large
	Undoable.prototype.discard = function(n) {
		if (n > 0){
		} else {
			n = this._head + 10;
		}
		var count = 0;
		while (this._head > -1 && count < n) {
			this._timeline.shift();
			this._head--;
			count ++;
		}
		return this;
	};

	// Undoable.prototype.restart = function() {
		// var key;
		// for(key in this) {
			// if(this.hasOwnProperty(key)) {
				// delete this[key];
			// }
		// }
		// this.clearHistory();
		// return this;
	// };
	window.Undoable = Undoable;
})()