/**
 * @class DirectFEA
 * DirectFEA core utilities and functions.
 * @singleton
 */
(function() {
	window.DirectFEA = window.DirectFEA ? window.DirectFEA : {};

	function apply(object, config, defaults) {
		if(defaults) {
			apply(object, defaults);
		}
		if(object && config && typeof config === 'object') {
			for(var key in config) {
				object[key] = config[key];
			}
		}
		return object;
	};

	function applyIf(object, config) {
		var property, undefined;

		if(object) {
			for(property in config) {
				if(object[property] === undefined) {
					object[property] = config[property];
				}
			}
		}

		return object;
	};

	function clone(obj) {
		if(null == obj || "object" != typeof obj) {
			return obj;
		}

		if( obj instanceof Date) {
			var copy = new Date();
			copy.setTime(obj.getTime());
			return copy;
		}
		if( obj instanceof Array) {
			var i, len, copy = [];
			for( i = 0, len = obj.length; i < len; ++i) {
				copy[i] = clone(obj[i]);
			}
			return copy;
		}
		if( obj instanceof Object) {
			var attr, copy = {};
			for(attr in obj) {
				if(obj.hasOwnProperty(attr)) {
					copy[attr] = clone(obj[attr]);
				}

			}
			copy.__proto__ = obj.__proto__;
			return copy;
		}
		throw new Error("Unable to copy obj! Its type isn't supported.");
	};

	//merge objects to the first one recursively. The letter one overrides first one's value.

	function merge(object, config) {
		if(object && config && typeof config === 'object') {
			for(var key in config) {
				if(ut.isObject(config[key]) || ut.isArray(config[key])) {

					if(object[key].constructor != config[key].constructor) {
						object[key] = config[key].constructor.call(window)
					}
					merge(object[key], config[key])
				} else {
					object[key] = config[key];
				}

			}
		}
		return object;
	};

	var inlineOverrides = function(o) {
		for(var m in o) {
			if(!o.hasOwnProperty(m)) {
				continue;
			}
			this[m] = o[m];
		}
	};
	var objectConstructor = Object.prototype.constructor;

	function extend(subclass, superclass, overrides) {
		// First we check if the user passed in just the superClass with overrides
		if(Ext.isObject(superclass)) {
			overrides = superclass;
			superclass = subclass;
			subclass = overrides.constructor != objectConstructor ? overrides.constructor : function() {
				superclass.apply(this, arguments);
			};
		}

		if(!superclass) {
			throw "Attempting to extend from a class which has not been loaded on the page.";
		}

		// We create a new temporary class
		var F = function() {
		}, subclassProto, superclassProto = superclass.prototype;

		F.prototype = superclassProto;
		subclassProto = subclass.prototype = new F();
		subclassProto.constructor = subclass;
		subclass.superclass = superclassProto;

		if(superclassProto.constructor == objectConstructor) {
			superclassProto.constructor = superclass;
		}

		subclass.override = function(overrides) {
			Ext.override(subclass, overrides);
		};

		subclassProto.superclass = subclassProto.supr = (function() {
			return superclassProto;
		});

		subclassProto.override = inlineOverrides;
		subclassProto.proto = subclassProto;

		subclass.override(overrides);
		subclass.extend = function(o) {
			return Ext.extend(subclass, o);
		};
		return subclass;
	};

	function override(origclass, overrides) {
		apply(origclass.prototype, overrides);
	};

	function isEmpty(value, allowBlank) {
		var isNull = value == null, emptyArray = (isArray(value) && !value.length), blankAllowed = !allowBlank ? value === '' : false;

		return isNull || emptyArray || blankAllowed;
	};

	function isEmptyObject(obj) {
		var key;
		for(key in obj) {
			if(obj.hasOwnProperty(key)) {
				return false;
			}
		}
		return true;
	};

	function isArray(v) {
		return Object.prototype.toString.apply(v) === '[object Array]';
	};

	function isDate(v) {
		return Object.prototype.toString.apply(v) === '[object Date]';
	};

	function isObject(v) {
		return !!v && !v.tagName && Object.prototype.toString.call(v) === '[object Object]';
	};

	function isFunction(v) {
		return Object.prototype.toString.apply(v) === '[object Function]';
	};

	function isNumber(v) {
		return Object.prototype.toString.apply(v) === '[object Number]' && isFinite(v);
	};

	function isString(v) {
		return typeof v === 'string';
	};

	function isBoolean(v) {
		return Object.prototype.toString.apply(v) === '[object Boolean]';
	};

	function isDefined(v) {
		return typeof v !== 'undefined';
	};

	function vectorAngle(dx, dy) {
		var a = Math.acos(dx / Math.sqrt(dx * dx + dy * dy));
		if(dy < 0) {
			a = -a;
		}
		return a
	};

	function distance(p1, p2) {
		var dx = p2.X - p1.X;
		var dy = p2.Y - p1.Y;
		return Math.sqrt(dx * dx + dy * dy);
	};
	
	function splat(obj) {
		if (!isArray(obj)) {
			return [obj];
		} else {
			return obj;
		}
	};

	function dotLineLength(x, y, x0, y0, x1, y1, o) {
		function lineLength(x, y, x0, y0) {
			return Math.sqrt((x -= x0) * x + (y -= y0) * y);
		}

		if(o && !( o = function(x, y, x0, y0, x1, y1) {
			if(!(x1 - x0))
				return {
					x : x0,
					y : y
				};
			else if(!(y1 - y0))
				return {
					x : x,
					y : y0
				};
			var left, tg = -1 / ((y1 - y0) / (x1 - x0));
			return {
				x : left = (x1 * (x * tg - y + y0) + x0 * (x * -tg + y - y1)) / (tg * (x1 - x0) + y0 - y1),
				y : tg * left - tg * x + y
			};
		}(x, y, x0, y0, x1, y1), o.x >= Math.min(x0, x1) && o.x <= Math.max(x0, x1) && o.y >= Math.min(y0, y1) && o.y <= Math.max(y0, y1))) {
			var l1 = lineLength(x, y, x0, y0), l2 = lineLength(x, y, x1, y1);
			return l1 > l2 ? l2 : l1;
		} else {
			var a = y0 - y1, b = x1 - x0, c = x0 * y1 - y0 * x1;
			return Math.abs(a * x + b * y + c) / Math.sqrt(a * a + b * b);
		}
	};
	
	function isPointInPoly(poly, pt) {
		for(var c = false, i = -1, l = poly.length, j = l - 1; ++i < l; j = i)((poly[i].Y <= pt.Y && pt.Y < poly[j].Y) || (poly[j].Y <= pt.Y && pt.Y < poly[i].Y)) && (pt.X < (poly[j].X - poly[i].X) * (pt.Y - poly[i].Y) / (poly[j].Y - poly[i].Y) + poly[i].X) && ( c = !c);
		return c;
	};

	/*
	 * adapted from Darel Rex Finley, 2006: http://alienryderflex.com/intersect/
	 *
	 * Determines the intersection point of the line segment defined by points A and B
	 * with the line segment defined by points C and D.
	 *
	 * If they intersect, return the intersection point, if not, return false
	 *
	 * ax:Number, ay:Number,bx:Number, by:Number,cx:Number, cy:Number,dx:Number, dy:Number
	 *
	 * It is translated into javascript by Li Ge, 2011
	 */

	function lineSegmentIntersection(ax, ay, bx, by, cx, cy, dx, dy) {

		var distAB, theCos, theSin, newX, posAB;

		//  Fail if either line segment is zero-length.
		if(ax == bx && ay == by || cx == dx && cy == dy) {
			return false
		};

		if(ax == cx && ay == cy || bx == cx && by == cy) {
			return {
				X : cx,
				Y : cy
			};
		}
		if(ax == dx && ay == dy || bx == dx && by == dy) {
			return {
				X : dx,
				Y : dy
			};
		}

		//  (1) Translate the system so that point A is on the origin.
		bx -= ax;
		by -= ay;
		cx -= ax;
		cy -= ay;
		dx -= ax;
		dy -= ay;

		//  Discover the length of segment A-B.
		distAB = Math.sqrt(bx * bx + by * by);

		//  (2) Rotate the system so that point B is on the positive X axis.
		theCos = bx / distAB;
		theSin = by / distAB;
		newX = cx * theCos + cy * theSin;
		cy = cy * theCos - cx * theSin;
		cx = newX;
		newX = dx * theCos + dy * theSin;
		dy = dy * theCos - dx * theSin;
		dx = newX;

		//  Fail if segment C-D doesn't cross line A-B.
		if(cy < 0. && dy < 0. || cy >= 0. && dy >= 0.) {
			return false;
		}

		//  (3) Discover the position of the intersection point along line A-B.
		posAB = dx + (cx - dx) * dy / (dy - cy);

		//  Fail if segment C-D crosses line A-B outside of segment A-B.
		if(posAB < 0. || posAB > distAB) {
			return false;
		}

		//  (4) Apply the discovered position to line A-B in the original coordinate system.
		return {
			X : ax + posAB * theCos,
			Y : ay + posAB * theSin
		}
	};

	function array2rgba(arr) {
		return "rgba(" + arr[0] + "," + arr[1] + "," + arr[2] + "," + arr[3] / 255 + ")";
	};

	function ajaxPost(obj) {
		var xmlHttpReq = false;
		var self = obj.scope || this;
		// Mozilla/Safari
		if(window.XMLHttpRequest) {
			self.xmlHttpReq = new XMLHttpRequest();
		}
		// IE
		else if(window.ActiveXObject) {
			self.xmlHttpReq = new ActiveXObject("Microsoft.XMLHTTP");
		}
		self.xmlHttpReq.open('POST', obj.url, true);
		self.xmlHttpReq.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
		self.xmlHttpReq.onreadystatechange = function() {
			if(self.xmlHttpReq.readyState == 4) {
				obj.success.call(self, self.xmlHttpReq)
			}
		}
		self.xmlHttpReq.send(obj.data);
	};

	function getAbsMax(obj, propName, mode) {
		var v = 0, i = 0;
		for(i in obj) {
			if(obj.hasOwnProperty(i) && isDefined(obj[i]) && Math.abs(obj[i][propName]) > v) {
				v = Math.abs(obj[i][propName]);
				i = i + 1;
			}
		}
		if(mode == 0) {
			return i;
		} else if(mode == 1) {
			return {
				value : v,
				index : i
			}
		} else {
			return v;
		}

	};
	
	function iterate(store, fn, scope) {
		var i,scope = scope || this;
		for (i in store){
			if (store.hasOwnProperty(i) && isDefined(store[i])) {
				fn.call(scope, store[i], i, store);
			}
		}
	};


	DirectFEA.apply = apply;
	DirectFEA.applyIf = applyIf;
	DirectFEA.clone = clone;
	DirectFEA.override = override;
	DirectFEA.isArray = isArray;
	DirectFEA.isBoolean = isBoolean;
	DirectFEA.isDate = isDate;
	DirectFEA.isDefined = isDefined;
	DirectFEA.merge = merge;
	DirectFEA.extend = extend;
	DirectFEA.isEmpty = isEmpty;
	DirectFEA.isEmptyObject = isEmptyObject;
	DirectFEA.isFunction = isFunction;
	DirectFEA.isNumber = isNumber;
	DirectFEA.isObject = isObject;
	DirectFEA.isString = isString;
	DirectFEA.vectorAngle = vectorAngle;
	DirectFEA.distance = distance;
	DirectFEA.dotLineLength = dotLineLength;
	DirectFEA.array2rgba = array2rgba;
	DirectFEA.ajaxPost = ajaxPost;
	DirectFEA.getAbsMax = getAbsMax;
	DirectFEA.lineSegmentIntersection = lineSegmentIntersection;
	DirectFEA.isPointInPoly = isPointInPoly;
	DirectFEA.iterate = iterate;
	DirectFEA.splat = splat;

	DirectFEA.ObjectHasDefault = extend(Object, {
		constructor : function(config) {
			apply(this, config, this.defaults);
		},
		configure : function(config) {
			merge(this, config);
			return this;
		}
	});

	DirectFEA.Selectable = extend(DirectFEA.ObjectHasDefault, {
		select : function() {
			this.isSelected = true;
		},
		unselect : function() {
			this.isSelected = false;
		},
		toggle : function() {
			if(this.isSelected) {
				this.unselect();
			} else {
				this.select();
			}
		}
	});


	// Object.prototype.each = function(fn,scope) {
		// var i, scope = scope || this;
		// for(i in this) {
			// if(this.hasOwnProperty(i) && isDefined(this[i])) {
				// fn.call(scope, this[i], i, this);
			// }
		// }
	// }
})();
