(function() {
	window.DirectFEA = window.DirectFEA ? window.DirectFEA : {};
	var AnalysisSetting, AnalysisSettingStore, //
	Constraints, Numberer, System, Test, Algorithm, Integrator, Analysis;
	$D = window.DirectFEA;

	/*
	 constraints Plain;
	 numberer Plain;
	 system BandGeneral;
	 test NormDispIncr 1.0e-8 6;
	 algorithm Newton;
	 integrator LoadControl 0.1;
	 analysis Static;
	 analyze 5;    */

	Constraints = $D.extend($D.ObjectHasDefault, {
		defaults : {
			type : "Plain"
		},
		toTcl : function() {
			return "constraints " + this.type;
		}
	});
	Numberer = $D.extend($D.ObjectHasDefault, {
		defaults : {
			type : "Plain"
		},
		toTcl : function() {
			return "numberer " + this.type;
		}
	});
	System = $D.extend($D.ObjectHasDefault, {
		defaults : {
			type : "BandGeneral"
		},
		toTcl : function() {
			return "system " + this.type;
		}
	});
	Test = $D.extend($D.ObjectHasDefault, {
		defaults : {
			type : "NormDispIncr",
			tol : 1.0e-8,
			iter : 30
		},
		toTcl : function() {
			return "test " + this.type + " " + this.tol + " " + this.iter;
		}
	});
	Algorithm = $D.extend($D.ObjectHasDefault, {
		defaults : {
			type : "Newton",
		},
		toTcl : function() {
			return "algorithm " + this.type;
		}
	});
	Integrator = $D.extend($D.ObjectHasDefault, {});
	StaticIntegrator = $D.extend(Integrator, {
		defaults : {
			type : "LoadControl",
			lambda : 0.1
		},
		toTcl : function() {
			return "integrator " + this.type + " " + this.lambda;
		}
	});
	TransientIntegrator = $D.extend(Integrator, {
		defaults : {
			type : "Newmark",
			gamma : 0.5,
			beta : 0.25
		},
		toTcl : function() {
			return "integrator " + this.type + " " + this.gamma+" "+this.beta;
		}
	});
	Analysis = $D.extend($D.ObjectHasDefault, {
		defaults : {
			type : "Static",
			dt: 0.02
		},
		toTcl : function() {
			if (this.type == "Static") {
				return "analysis " + this.type;
			} else{
				return "analysis " + this.type;
			};
			
		}
	});
	Eigen = $D.extend($D.ObjectHasDefault, {
		defaults : {
			numEigenvalues : 1,
			type : "",
			solver : ""
		},
		toTcl : function() {
			return "eigen " + this.numEigenvalues;
		}
	});
	Analyze = $D.extend($D.ObjectHasDefault, {
		defaults : {
			numIncr : 1,
			dt : 0.1
		},
		toTcl : function() {
			return "analyze " + this.numIncr + " " + this.dt;
		}
	});
	AnalysisSetting = $D.extend($D.ObjectHasDefault, {
		toTcl : function() {
			var result = "";
			result += this.theConstraints.toTcl() + ";\n";
			result += this.theNumberer.toTcl() + ";\n";
			result += this.theSystem.toTcl() + ";\n";
			result += this.theTest.toTcl() + ";\n";
			result += this.theAlgorithm.toTcl() + ";\n";
			result += this.theIntegrator.toTcl() + ";\n";
			result += this.theAnalysis.toTcl() + ";\n";
			return result;
		}
	});

	//Constraints,Numberer,System,Test,Algorithm,Integrator,Analysis
	$D.AnalysisSetting = AnalysisSetting;
	$D.Constraints = Constraints;
	$D.Numberer = Numberer;
	$D.System = System;
	$D.Test = Test;
	$D.Algorithm = Algorithm;
	$D.Integrator = Integrator;
	$D.StaticIntegrator = StaticIntegrator;
	$D.TransientIntegrator = TransientIntegrator;
	$D.Analysis = Analysis;

})();
