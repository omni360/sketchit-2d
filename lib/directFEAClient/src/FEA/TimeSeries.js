(function() {
	window.DirectFEA = window.DirectFEA ? window.DirectFEA : {};
	var TimeSeries, ConstantTimeSeries,PathTimeSeries,LinearTimeSeries,TriangleTimeSeries,
	TimeSeriesStore;
	$D = window.DirectFEA;
	
	// abstract class
	
	TimeSeries = $D.extend($D.Selectable, {});
	
	ConstantTimeSeries = $D.extend(TimeSeries, {
		defaults : {
			factor : 1.0
		},
		toTcl:function(){
			return "timeSeries Constant "+this.id+" -factor " + this.factor;
		}
	});
	
	LinearTimeSeries = $D.extend(TimeSeries, {
		defaults : {
			factor : 1.0
		},
		toTcl:function(){
			return "timeSeries Linear "+this.id+" -factor " + this.factor;
		}
	});

	TriangleTimeSeries = $D.extend(TimeSeries, {
		defaults : {
			factor : 1.0,
            tStart : 0.0,
            tEnd : 20.0,
            period : 20.0,
            shift : 0.0
		},
		toTcl:function(){
            // timeSeries Triangle $tag $tStart $tEnd $period <-shift $shift> <-factor $cFactor>
			return "timeSeries Triangle " + this.id + " " + this.tStart
                + " " + this.tEnd + " " + this.period + " -shift " + this.shift
                + " -factor " + this.factor;
		}
	});
	
	PathTimeSeries = $D.extend(TimeSeries, {

		defaults : {
			factor : 1.0,
			dt : 0.02,
			filePath : "$TS_DIR/gmotion.txt"
		},
		toTcl : function() {
			return "timeSeries Path "+ this.id + " -dt " + this.dt + " -filePath " + this.filePath + " -factor " + this.factor;
		}
	});

	
	
	

	$D.TimeSeries = TimeSeries;
	$D.ConstantTimeSeries=ConstantTimeSeries;
	$D.LinearTimeSeries=LinearTimeSeries;
	$D.TriangleTimeSeries=TriangleTimeSeries;
	$D.PathTimeSeries = PathTimeSeries;

})();
