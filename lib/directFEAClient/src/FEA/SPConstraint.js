(function() {
	window.DirectFEA = window.DirectFEA ? window.DirectFEA : {};
	var $D = DirectFEA, //
	SPConstraint = $D.extend($D.Selectable, {
		className : "SPConstraint",
		defaults : {
			X : 1,
			Y : 1,
			RZ : 1,
			node : undefined,
			angle : -Math.PI / 2,
			direction : "up",
			isSelected : false,
			isShow : true,
		},
		toTcl : function() {
			if(this.X == 0 && this.Y == 0 && this.RZ == 0) {
				return "";
			} else {
				return "fix " + this.node.id + " " + this.X + " " + this.Y + " " + this.RZ;
			}
		},
		// solve the circular reference problem:
		toJSON : function() {
			result={};
			
			for(i in this) {
				if(this.hasOwnProperty(i) && $D.isDefined(this[i]) && !$D.isObject(this[i]) && !$D.isArray(this[i])) {
					result[i]=this[i];
				}
			}
			
			result.nodeID = this.node?this.node.id:"";
			
			return result;
		},
		findReference:function(Dm){
			if (this.nodeID!=="") {
				this.node=Dm.theNodes[this.nodeID];
			}
			
		},
		getBottomCenter : function() {
			return {
				X : this.node.X + this.triangleSize * Math.cos(this.angle),
				Y : this.node.Y + this.triangleSize * Math.sin(this.angle),
				SPC:this,
				bottom:true
			}
		},
		lineWidth : 2,
		normalStrokeStyle : [0, 0, 0, 255],
		selectedStrokeStyle : [255, 0, 255, 255],

		triangleSize : 20,
		groundLength : 40,
		groundThickness : 8,
		groundN : 10,
		pinRadius : 5,
		rollerRadius : 3,

		display : function(R, scale) {
			if(!this.isShow) {
				return false;
			} else {
				R.lineWidth = this.lineWidth / scale;
				if(this.isSelected) {
					R.strokeStyle = $D.array2rgba(this.selectedStrokeStyle);
				} else {
					R.strokeStyle = $D.array2rgba(this.normalStrokeStyle);
				}

				R.save();
				R.translate(this.node.X, this.node.Y);
				R.rotate(this.angle);
				R.beginPath();
				if(this.RZ === 0) {
					R.arc(0, 0, this.pinRadius, 0, Math.PI * 2, true);
					R.moveTo(this.pinRadius * Math.cos(Math.PI / 6), this.pinRadius * Math.sin(Math.PI / 6));
					R.lineTo(this.triangleSize, this.triangleSize * 0.5);
					R.lineTo(this.triangleSize, -this.triangleSize * 0.5);
					R.lineTo(this.pinRadius * Math.cos(Math.PI / 6), -this.pinRadius * Math.sin(Math.PI / 6));
				} else {
					R.moveTo(0, 0);
					R.lineTo(this.triangleSize, this.triangleSize * 0.5);
					R.lineTo(this.triangleSize, -this.triangleSize * 0.5);
					R.lineTo(0, 0);
				}
				if(this.X === 0 || this.Y === 0) {
					R.moveTo(this.triangleSize + 2 * this.rollerRadius, this.triangleSize * 0.5 - this.rollerRadius);
					R.arc(this.triangleSize + this.rollerRadius, this.triangleSize * 0.5 - this.rollerRadius, this.rollerRadius, 0, Math.PI * 2, true);
					R.moveTo(this.triangleSize + 2 * this.rollerRadius, -this.triangleSize * 0.5 + this.rollerRadius);
					R.arc(this.triangleSize + this.rollerRadius, -this.triangleSize * 0.5 + this.rollerRadius, this.rollerRadius, 0, Math.PI * 2, true);
					R.translate(this.triangleSize + 2 * this.rollerRadius, 0);
				} else {
					R.translate(this.triangleSize, 0);
				}

				R.moveTo(0, -this.groundLength * 0.5);
				R.lineTo(0, this.groundLength * 0.5);
				var k = -this.groundLength * 0.5, s = this.groundLength / this.groundN;

				while(k < this.groundLength * 0.5) {
					R.moveTo(0, k);
					R.lineTo(s, k + s);
					k += s;
				}

				R.stroke();
				R.restore();
				return true;
			}
		}
	});
	$D.SPConstraint = SPConstraint;
})();
