// Given a interval [a,b], return an array that uniformly devide the interval into n segments
// The returned array start with a and ends with b
var linspace = function(a,b,n){
	var ret = [];
	var d = (b - a)/n;
	for(var i = 0; i < n+1; i++) {
		ret.push(a+i*d);	
	}
	return ret;
}

// var testx = linspace(0,1,10);
// alert("testx:",testx);

var linear_piece_wise_space_1d = function(xi,yi){
	
	this.controlPoints = [];
	for(var i=0; i < xi.length; i++) {
		if (typeof yi[i] == 'undefined') {
			break;
		}
		this.controlPoints.push({
			X : xi[i],
			Y : yi[i]
		});
	}
	this.numOfCtrlPts = this.controlPoints.length;
	this.controlPoints.sort(function(a,b){return a.X-b.X});
	this.xi = [];
	this.yi = [];
	for(var i=0; i < this.numOfCtrlPts; i++) {
		this.xi.push(this.controlPoints[i].X);
		this.yi.push(this.controlPoints[i].Y);
	}
}

// TODO: all functions can be modified to support array operations by using Array.map()
// Give value x, find the interval it belongs to.
// returns -1 if it is in ( -inf, xi[0] ],
// returns i if it is in ( xi[i],xi[i+1] ],
// returns -2 if it is in ( xi[end], +inf ),
linear_piece_wise_space_1d.prototype.findInterval=function(x){
	
	// To Do: use bisection
	for(var i = 0; i < this.numOfCtrlPts; i++) {
		if(this.xi[i] > x) {
			break;
		}		
	}
	if(i == this.numOfCtrlPts) {
		return -2;
	} else {
		return i-1;
	}
};

// Map parametric coordinate xi to global coordinate x 
linear_piece_wise_space_1d.prototype.geometry_map=function(xi){
	
	return x;
};

// Give global coordinate x, evaluate the function value f(x)
linear_piece_wise_space_1d.prototype.eval=function(x){
	var intvID = this.findInterval(x);
	if(intvID < 0) {
		alert("x is not in parametric domain");
	} else {
		
	}
};

// Give parametric coordinate x, evaluate the function value f(x)
linear_piece_wise_space_1d.prototype.eval_para=function(x){
	var intvID = this.findInterval(x);
	if(intvID < 0) {
		alert("x is not in parametric domain");
	} else {
		
	}
};

var testx = linspace(0,1,10);
var testy = testx.map(function(x){
	return x*x;
});
var space_test=new linear_piece_wise_space_1d(testx,testy);
// console.log('find 0.3 in interval:',space_test.findInterval(0.3));
// console.log('find -1 in interval:',space_test.findInterval(-1));
// console.log('find 1.2 in interval:',space_test.findInterval(1.2));



(function() {
	window.DirectFEA = window.DirectFEA ? window.DirectFEA : {};
	var Element;
	var LineElement;
	var ElasticBeamColumn;
	var Truss;
	var NoneLinearBeamColumn;
	var BeamWithHinges;
	$D = DirectFEA;
	Element = $D.extend($D.Selectable, {
		className : "Element",
        lineWidth : 1,
        normalStrokeStyle : [0, 0, 255, 255],
		selectedStrokeStyle : [255, 0, 255, 255],
		deformationStrokeStyle : [0, 0, 255, 128],
		constructor : function() {
			Element.superclass.constructor.apply(this, arguments);
			this.elementLoads = {};
			// this.nodes = {};

		},
		getLocalNodeId : function(nodeID) {
			var i;
			for(i=0;i<this.nodes.length;i++) {
				if(this.nodes[i].id == nodeID) {
					return i;
				}
			}
			return -1;
		},
		
		// solve the circular reference problem:
		toJSON : function() {
			result={};
			
			for(i in this) {
				if(this.hasOwnProperty(i) && $D.isDefined(this[i]) && !$D.isObject(this[i]) && !$D.isArray(this[i])) {
					result[i]=this[i];
				}
			}
			
			var i;
			var nodeIDs=[];
			for(i in this.nodes) {
				if(this.nodes.hasOwnProperty(i) && $D.isDefined(this.nodes[i])) {
					nodeIDs.push(this.nodes[i].id);
				}
			}
			result.nodeIDs = nodeIDs.length==0?"":nodeIDs;
			
			var i;
			var elementLoadsIDs=[];
			for(i in this.elementLoads) {
				if(this.elementLoads.hasOwnProperty(i) && $D.isDefined(this.elementLoads[i])) {
					elementLoadsIDs.push(i);
				}
			}
			result.elementLoadsIDs = elementLoadsIDs.length==0?"":elementLoadsIDs;
			
			result.geomTransfId=this.geomTransf?this.geomTransf.id:"";
			
			return result;
		},
		findReference:function(Dm){
            debugger;
			if (this.geomTransfId!=="") {
				this.geomTransf=Dm.theGeomTransfs[this.geomTransfId];
			}
			if (this.nodeIDs!=="") {
				for(var i=0,j=nodeIDs.length; i<j; i++){
					this.nodes[nodeIDs[i]]=Dm.theNodes[nodeIDs[i]];
				}
			}
		},
        display : function(R, scale) {
			if(!this.isShow) {
				return false;
			} else {
				R.lineWidth = this.lineWidth / scale;
				if(this.isSelected) {
					R.strokeStyle = $D.array2rgba(this.selectedStrokeStyle);
				} else {
					R.strokeStyle = $D.array2rgba(this.normalStrokeStyle);
				}
                // debugger;
                var nn = this.nodes.length;
                if (nn === 1) {
                    debugger;
                    R.fillStyle = '#0000ff';
                    R.drawSquare(this.nodes[0], 5 / scale, 1);
                } else if (nn === 2) {
                    R.drawLine(this.nodes[0], this.nodes[1]);
                } else if (nn > 2) {
                    for (var i = 0, len = this.nodes.length; i < len - 1; i++) {
                        R.drawLine(this.nodes[i], this.nodes[i+1]);
                    }
				    R.drawLine(this.nodes[i], this.nodes[0]);
                }
                return true;
			}
		}
		
	});
	LineElement = $D.extend(Element, {
		// from : undefined,
		// to : undefined,
		
		nodeNum : 2,
		eleResponseFx1 : undefined,
		eleResponseFx2 : undefined,
		eleResponseFy1 : undefined,
		eleResponseFy2 : undefined,
		eleResponseM1 : undefined,
		eleResponseM2 : undefined,
		eleLocalResponseFx1 : undefined,
		eleLocalResponseFx2 : undefined,
		eleLocalResponseFy1 : undefined,
		eleLocalResponseFy2 : undefined,
		eleLocalResponseM1 : undefined,
		eleLocalResponseM2 : undefined,
		isSelected : false,
		isShow : true,
		showDeformation : true,
		showMoment : true,
		showDirection : true,
		showTag:true,

		lineWidth : 5,
		normalStrokeStyle : [0, 0, 255, 255],
		selectedStrokeStyle : [255, 0, 255, 255],
		deformationStrokeStyle : [0, 0, 255, 128],
		getFrom:function(){
			return this.nodes[0];
		},
		getEnd:function(){
			return this.nodes[1];
		},

		constructor : function() {
			LineElement.superclass.constructor.apply(this, arguments);
            // debugger;
            // this.geomTranfId = sketchit.settings.defaultGeomTransfId;
			
			// this.from = this.nodes[0];
			// this.to = this.nodes[1];
		},
		calculateMass: function(){
			this.mass = this.density*this.A*this.getLength();
		},
		// display : function(R, scale) {
		// 	if(!this.isShow) {
		// 		return false;
		// 	} else {
		// 		R.lineWidth = this.lineWidth / scale;
		// 		if(this.isSelected) {
		// 			R.strokeStyle = $D.array2rgba(this.selectedStrokeStyle);
		// 		} else {
		// 			R.strokeStyle = $D.array2rgba(this.normalStrokeStyle);
		// 		}
		// 		R.drawLine(this.getFrom(), this.getEnd());
		// 		return true;
		// 	}
		// },
		
		directionArrowLineWidth : 2,
		directionArrowHalfAngle : Math.PI/6,
		directionArrowLength : 15,
		directionArrowPosition : 0.5,
		directionArrowStyle : [0, 0, 255, 127],
		
		
		displayDirection : function(R, scale) {
			if(!this.showDirection) {
				return false;
			} else {
				var cenx = this.getFrom().X + (this.getEnd().X-this.getFrom().X)*this.directionArrowPosition, //
				ceny = this.getFrom().Y + (this.getEnd().Y-this.getFrom().Y)*this.directionArrowPosition;
				R.save();
				R.lineWidth = this.directionArrowLineWidth / scale;
				R.strokeStyle = $D.array2rgba(this.directionArrowStyle);
				R.translate(cenx,ceny);
				R.rotate(this.getAngle())
				
				R.drawLine({
					X : 0,
					Y : 0
				}, {
					X : -this.directionArrowLength*Math.cos(this.directionArrowHalfAngle)/ scale,
					Y : this.directionArrowLength*Math.sin(this.directionArrowHalfAngle)/ scale
				});
				R.drawLine({
					X : 0,
					Y : 0
				}, {
					X : -this.directionArrowLength*Math.cos(this.directionArrowHalfAngle)/ scale,
					Y : -this.directionArrowLength*Math.sin(this.directionArrowHalfAngle)/ scale
				});
				R.restore();
				return true;
			}
		},
		
		tagShiftX:0,
		tagShiftY:10,
		tagFont:"bold 12px sans-serif",
		tagFillStyle:[0, 0, 255, 127],
		
		displayTag : function(R, scale) {
			if(!this.showTag) {
				return false;
			} else {
				var cenx = (this.getFrom().X + this.getEnd().X)*0.5, //
				ceny = (this.getFrom().Y + this.getEnd().Y)*0.5;
				R.save();
				R.font = this.tagFont;
				R.fillStyle = $D.array2rgba(this.tagFillStyle);
				R.translate(cenx,ceny);
				R.rotate(this.getAngle());
				R.translate(this.tagShiftX/scale,this.tagShiftY/scale)
				R.transform(1, 0, 0, -1, 0, 0);
				R.fillText(this.id, 0,0);
				R.restore();
				
				// R.fillStyle = $D.array2rgba(this.tagFillStyle);
				// R.drawSquare({
					// X : this.X + this.dispX * deformScale,
					// Y : this.Y + this.dispY * deformScale
				// }, this.deformedNodeSize / viewPortScale, 1);
				return true;
			}
		},
		
		//mode:1 linear, mode:2 quardratic, mode:3 cubic...
		getDeflection : function(xL, scale, mode) {
			var dT1 = this.getFrom().dispRZ;
			var dT2 = this.getEnd().dispRZ;
			// var dT1 = this.getFrom().dispRZ * scale;
			// var dT2 = this.getEnd().dispRZ * scale;
			var dT = this.getDeformedAngle(1) - this.getAngle();
			// var dT = 0.0;
			if (Math.abs(dT) > Math.PI) {
				if (dT > 0) {
					dT = dT - Math.PI*2;
				} else {
					dT = dT + Math.PI*2;
				}
			}
			// var t1 = this.getAngle() + this.getFrom().dispRZ * scale - this.getDeformedAngle(scale);
			// var t2 = this.getAngle() + this.getEnd().dispRZ * scale - this.getDeformedAngle(scale);
			var t1 = dT1 - dT;
			var t2 = dT2 - dT;
			// l0= this.getLength();
			var l1 = this.getDeformedLength(scale);
			// var l1 = this.getLength();
			switch (mode) {
				case 1:
					return 0.0;
					break;
				case 2:
					break;
				case 3:
					// no element loads
					// y=((theta1+theta2)*xL^3-(2*theta1+theta2)*xL^2+theta1*xL)*deformedLength()
					return scale*(((t1 + t2) * xL * xL * xL - (2 * t1 + t2) * xL * xL + t1 * xL) * l1);
					break;
				default:
					break;
			}

		},
		displayDeformation : function(R, viewPortScale, deformScale, delta) {

			if(!this.showDeformation) {
				return false;
			} else {
				var n = Math.round(this.getDeformedLength(deformScale) / delta * viewPortScale);
				R.save();
				R.strokeStyle = $D.array2rgba(this.deformationStrokeStyle);
				R.lineWidth = this.lineWidth / viewPortScale;
				R.beginPath();
				// R.translate(this.getFrom().X + this.getFrom().dispX * deformScale, this.getFrom().Y + this.getFrom().dispY * deformScale);
				// R.rotate(this.getDeformedAngle(1));
				// R.rotate(this.getDeformedAngle(deformScale));
				// R.moveTo(0, this.getDeflection(0, deformScale, 3));
				// for( i = 1; i < n + 1; i++) {
					// R.lineTo(i / n * this.getDeformedLength(deformScale), this.getDeflection(i / n, deformScale, 3));
				// }
				R.moveTo(this.getFrom().X + this.getFrom().dispX * deformScale, this.getFrom().Y + this.getFrom().dispY * deformScale);
				R.lineTo(this.getEnd().X + this.getEnd().dispX * deformScale, this.getEnd().Y + this.getEnd().dispY * deformScale);
				R.stroke();
				R.restore();
				return true;
			}
		},
		// if the moment makes the componet bend to bottom, it is possitive, see below:
		//         A                         A
		//        /                           \
		//       ( 1 ---------------------> 2 )
		//       \                           /
		getLocalResponse : function() {
			this.eleLocalResponseM1 = -this.eleResponseM1;
			this.eleLocalResponseM2 = this.eleResponseM2;
			if(Math.abs(this.eleLocalResponseM1) > Math.abs(this.eleLocalResponseM2)) {
				this.maxMoment = this.eleLocalResponseM1;
			} else {
				this.maxMoment = this.eleLocalResponseM2;
			}

		},
		getMoment : function(xL, scale) {
			return scale * (this.eleLocalResponseM1 + xL * (this.eleLocalResponseM2 - this.eleLocalResponseM1))
		},
		momentFillColor : [255, 255, 0, 127],
		showMomentEdge : true,
		momentEdgeWidth : 1,
		momentEdgeStyle : [0, 0, 0, 127],

		displayMoment : function(R, viewPortScale, momentScale, delta) {

			if(!this.showMoment) {
				return false;
			} else {
				var n = Math.round(this.getLength() / delta * viewPortScale);
				R.save();
				R.fillStyle = $D.array2rgba(this.momentFillColor);
				R.lineWidth = this.momentEdgeWidth / viewPortScale;
				R.strokeStyle = $D.array2rgba(this.momentEdgeStyle);

				R.translate(this.getFrom().X, this.getFrom().Y);
				R.rotate(this.getAngle());
				R.beginPath();
				R.moveTo(0, 0);
				for( i = 0; i < n + 1; i++) {
					R.lineTo(i / n * this.getLength(), this.getMoment(i / n, momentScale));
				}
				R.lineTo(this.getLength(), 0);
				R.closePath();
				R.fill();

				if(this.showMomentEdge === true) {
					R.beginPath();
					R.moveTo(0, 0);
					for( i = 0; i < n + 1; i++) {
						R.lineTo(i / n * this.getLength(), this.getMoment(i / n, momentScale));
					}
					R.lineTo(this.getLength(), 0);
					R.stroke();
				}

				R.restore();
				return true;
			}
		},
		move : function(dx, dy) {
			this.getFrom().move(dx, dy);
			this.getEnd().move(dx, dy);
		},
		getDx : function() {
			return this.getEnd().X - this.getFrom().X;
		},
		getDy : function() {
			return this.getEnd().Y - this.getFrom().Y;
		},
		getLength : function() {
			return Math.sqrt((this.getFrom().X - this.getEnd().X) * (this.getFrom().X - this.getEnd().X) + (this.getFrom().Y - this.getEnd().Y) * (this.getFrom().Y - this.getEnd().Y));
		},
		getDeformedLength : function(scale) {
			return Math.sqrt((this.getEnd().X + scale * this.getEnd().dispX - this.getFrom().X - scale * this.getFrom().dispX) * (this.getEnd().X + scale * this.getEnd().dispX - this.getFrom().X - scale * this.getFrom().dispX) + (this.getEnd().Y + scale * this.getEnd().dispY - this.getFrom().Y - scale * this.getFrom().dispY) * (this.getEnd().Y + scale * this.getEnd().dispY - this.getFrom().Y - scale * this.getFrom().dispY));
		},
		getAngle : function() {
			var a = Math.acos(this.getDx() / this.getLength());
			if(this.getDy() < 0) {
				a = 2*Math.PI-a;
			}
			return a
		},
		getDeformedAngle : function(scale) {
			var a = Math.acos((this.getDx() + scale * (this.getEnd().dispX - this.getFrom().dispX)) / this.getDeformedLength(scale));
			if(this.getDy() + scale * (this.getEnd().dispY - this.getFrom().dispY) < 0) {
				a = Math.PI*2-a;
			}
			return a
		}
	});
	ElasticBeamColumn = $D.extend(LineElement, {
		//acceptEleLoad : true,
		acceptEleLoad : false,
		interporlationMode : 3,
		defaults : {
			className : "ElasticBeamColumn",
			// from : undefined,
			// to : undefined,
			A : 44.8,
			E : 29000,
			I : 8160,
			density : 0.284 / 1000,
			// geomTransf : undefined,

			isSelected : false,
			isShow : true,
		},
		getProperty:function(){
			return  {
				className : "ElasticBeamColumn",
				A : this.A,
				E : this.E,
				I : this.I,
				geomTransf: this.geomTransf,
				isSelected : this.isSelected,
				isShow : this.isShow,
			}
		},
		toTcl : function() {
			// var fid = this.getFrom().id;
			// var tid = this.getEnd().id;
			// var gid = this.getEnd().id;
			// var id = this.id;
			// return "element elasticBeamColumn " + id + " " + fid + " " + tid + " " + this.A + " " + this.E + " " + this.I + " " + this.geomTransf.id;
            var gfid;
            if (this.geomTransf){
                gfid = this.geomTransf.id;
            } else {
                gfid = this.geomTranfId;
            }
			return "element elasticBeamColumn " + this.id + " " + this.getFrom().id + " " + this.getEnd().id + " " + this.A + " " + this.E + " " + this.I + " " + gfid;
		}
	});
	ForceBeamColumn2d = $D.extend(LineElement, {
		//acceptEleLoad : true,
		acceptEleLoad : false,
		interporlationMode : 3,
		defaults : {
			className : "ForceBeamColumn2d",
			// from : undefined,
			// to : undefined,
			sectionID:1,
			numIntgrPts:5,
			// A : 100,
			// E : 29000,
			// I : 833.3333,
			density : 0.284,
			// geomTransf : undefined,

			isSelected : false,
			isShow : true,
		},
		getProperty:function(){
			return  {
				className : "ElasticBeamColumn",
				sectionID : this.sectionID,
				numIntgrPts : this.numIntgrPts,
				density : this.density,
				geomTransf: this.geomTransf,
				isSelected : this.isSelected,
				isShow : this.isShow,
			}
		},
		toTcl : function() {
			// var fid = this.getFrom().id;
			// var tid = this.getEnd().id;
			// var gid = this.getEnd().id;
			// var id = this.id;
			// return "element elasticBeamColumn " + id + " " + fid + " " + tid + " " + this.A + " " + this.E + " " + this.I + " " + this.geomTransf.id;
            var sec;
            if ($D.isArray(this.sectionID)){
                sec = "-sections " + this.sectionID.join(' ');
            } else {
                sec = this.sectionID;
            }
			return "element forceBeamColumn " + this.id + " " + this.getFrom().id +
                " " + this.getEnd().id + " " + this.numIntgrPts + " " +
                sec + " " + this.geomTransf.id;
		}
	});
	
	Truss = $D.extend(LineElement, {
		acceptEleLoad : false,
		defaults : {
			className : "Truss",
			A : 100,
			E : 29000,
			I : 833.3333
		},
		toTcl : function() {
			return "element truss " + this.id + " " + this.getFrom().id + " " + this.getEnd().id + " " + this.A + " " + this.E + " " + this.I + " " + this.geomTransf.id;
		}
	});

	$D.Element = Element;
	$D.LineElement = LineElement;

	$D.ElasticBeamColumn = ElasticBeamColumn;
	$D.ElasticBeam2d = ElasticBeamColumn;

	$D.Truss = Truss;
	$D.ForceBeamColumn2d = ForceBeamColumn2d;
	$D.DispBeamColumn2d = ForceBeamColumn2d;
	$D.BeamWithHinges = BeamWithHinges;
})();
