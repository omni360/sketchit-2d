(function() {
	window.DirectFEA = window.DirectFEA ? window.DirectFEA : {};

	/**
	 * private utility functions
	 *
	 */
	var $D = DirectFEA,
	    distance = $D.distance,
	    dotLineLength = $D.dotLineLength,
	    vectorAngle = $D.vectorAngle,
	    isPointInPoly = $D.isPointInPoly;
    
	function lineSegmentIntersect(p1, p2, p3, p4) {
		return $D.lineSegmentIntersection(p1.X, p1.Y, p2.X, p2.Y, p3.X, p3.Y, p4.X, p4.Y);
	}

	function findNearestNode(n, nodes) {
		var i, result = {};
		result.d = Number.MAX_VALUE;
		for(i in nodes) {
			if(nodes.hasOwnProperty(i) && $D.isDefined(nodes[i]) && nodes[i].id != n.id) {
				if(distance(n, nodes[i]) < result.d) {
					result.index = i;
					result.d = distance(n, nodes[i]);
				}

			}
		}
		if($D.isDefined(result.index)) {
			return result;
		} else {
			return false;
		}

	};

	function findNearestLineElement(n, lelements) {
		var i, d, temp, result = {};
		result.d = Number.MAX_VALUE;
		for(i in lelements) {
			if(lelements.hasOwnProperty(i) && $D.isDefined(lelements[i]) && lelements[i].getFrom().id != n.id && lelements[i].getEnd().id != n.id) {
				temp = dotLineLength(n.X, n.Y, lelements[i].getFrom().X, lelements[i].getFrom().Y, lelements[i].getEnd().X, lelements[i].getEnd().Y, true);
				if(temp < result.d) {
					result.index = i;
					result.d = temp;
				}

			}
		}
		if($D.isDefined(result.index)) {
			return result;
		} else {
			return false;
		}
	};

	function findPerFoot(n, e) {
		if( typeof n === "undefined" || typeof e === "undefined") {
			alert("point or line undefined");
			return false;
		}
		var flag = false;
		if(!(e.getFrom().X - e.getEnd().X)) {
			if((n.Y - e.getFrom().Y) * (n.Y - e.getEnd().Y) <= 0) {
				flag = true;
			}
			return {
				pf : {
					X : (e.getFrom().X + e.getEnd().X) / 2,
					Y : n.Y
				},
				dir : "vertical",
				online : flag
			}
		} else if(!(e.getFrom().Y - e.getEnd().Y)) {
			if((n.X - e.getFrom().X) * (n.X - e.getEnd().X) <= 0) {
				flag = true;
			}
			return {
				pf : {
					X : n.X,
					Y : (e.getFrom().Y + e.getEnd().Y) / 2
				},
				dir : "horizontal",
				online : flag
			}
		} else {
			//var x1 = e.getFrom().X, y1 = e.getFrom().Y,
			var dx = e.getEnd().X - e.getFrom().X, dy = e.getEnd().Y - e.getFrom().Y, y = (dx * dx * e.getFrom().Y + n.X * dx * dy - e.getFrom().X * dx * dy + n.Y * dy * dy) / (dx * dx + dy * dy), x = e.getFrom().X + dx * (y - e.getFrom().Y) / dy, r = (x - e.getFrom().X) / dx;
			if(r <= 1 && r >= 0) {
				flag = true;
			}
			return {
				pf : {
					X : x,
					Y : y
				},
				dir : "oblique",
				online : flag
			}
		}
	};

	function snapToNode(node, threshold, nodes) {
		var np = findNearestNode(node, nodes), result = {};
		result.capture = false;
		if(np.d < threshold) {
			result.node = nodes[np.index];
			result.nodeId = np.index;
			result.capture = true;
		}
		return result;
	};

	function snapToLine(node, threshold, lelements) {
		var nl = findNearestLineElement(node, lelements), result = {}, v;
		result.capture = false;
		if(nl.d < threshold) {
			v = findPerFoot(node, lelements[nl.index]);
			if(v.online) {
				result.node = new node.constructor({
					X : v.pf.X,
					Y : v.pf.Y
				});
				if(lelements[nl.index].getEnd().X - lelements[nl.index].getFrom().X) {
					result.ratio = (v.pf.X - lelements[nl.index].getFrom().X) / (lelements[nl.index].getEnd().X - lelements[nl.index].getFrom().X);
				} else {
					result.ratio = (v.pf.Y - lelements[nl.index].getFrom().Y) / (lelements[nl.index].getEnd().Y - lelements[nl.index].getFrom().Y);
				}
				result.line = lelements[nl.index];
				result.lineId = nl.index;
				result.capture = true;
			}

		}
		return result;
	};

	function snapToGrid(node, dx, dy) {
		var result = {};
		result.node = new node.constructor({
			X : Math.round(node.X / dx) * dx,
			Y : Math.round(node.Y / dy) * dy
		});
		result.capture = true;
		return result;
	};

	function snapTo4Direction(angle, threshold) {
		var result = {}, n;
		n = Math.round(angle / (Math.PI / 2));
		result.angle = n * (Math.PI / 2);
		result.capture = false;
		// if(Math.abs(angle - n * (Math.PI / 2)) / (Math.PI / 2) < threshold) {
			result.capture = true;
			switch ((n+4)%4) {
				case 0:
					result.dir = "left";
					break;
				case 1:
					result.dir = "down";
					break;
				case 2:
					result.dir = "right";
					break;
				case 3:
					result.dir = "up";
					break;
				default:
					break;
			}
		// } else {
			// result.angle = angle;
			// result.dir = "oblique"
		// }
		return result;
	};
	
	function snapToSPC(node, threshold, SPCs) {
		var i, nodes = [], result = {}, flag, min, d = Number.MAX_VALUE, bottom = false, index;
		result.capture = false;
		for(i in SPCs) {
			if(SPCs.hasOwnProperty(i) && $D.isDefined(SPCs[i])) {
				if(distance(SPCs[i].node, node) < distance(SPCs[i].getBottomCenter(), node)) {
					flag = false;
					min = distance(SPCs[i].node, node);
				} else {
					flag = true;
					min = distance(SPCs[i].getBottomCenter(), node);
				}

				if(min < d) {
					d = min;
					bottom = flag;
					index = i;
				}
			}
		}
		if(d < threshold) {
			result.capture = true;
			result.d = d;
			result.bottom = bottom;
			result.SPCId = index;
			result.SPC = SPCs[index];
		}
		return result;
	};

	var Domain;
	Domain = $D.extend(Undoable, {
		constructor : function() {
			Domain.superclass.constructor.apply(this, arguments); 
			this.deformationAvailable = false;
			this.readyToRender = true;
			this.changed = false;
			this.theTimeSeries = {};
			this.theGeomTransfs = {};
			this.theMaterials = {};
			this.theSections = {};
			this.theNodes = {};
			this.theElements = {};
			this.theSPCs = {};
			this.thePatterns = {};
			this.theAnalysisSettngs = {};
			this.theSections = {};
			this.theMaterials = {};
			this.currentTimeSeries;
			this.currentPattern;
			this.currentAnalysisSetting;
			this.selectedObjects = {};
			this.selectedNodes = {};
			this.selectedElements = {};
			this.selectedLoads = {};
			this.selectedSPCs = {};
			
			// this.timeHistory = [];
			this.nodeDispHistory={};
			this.nodeDispHistoryVolumn=0;
			this.elementEndForceHistory={};

			this.theGeomTransfs[1] = new $D.GeomTransf({
				id : 1,
				type : "Linear"
			});
			this.theGeomTransfs[2] = new $D.GeomTransf({
				id : 2,
				type : "PDelta"
			});
			this.theGeomTransfs[3] = new $D.GeomTransf({
				id : 3,
				type : "Corotational"
			});

			// this.theTimeSeries[1] = new $D.ConstantTimeSeries({
				// id : 1this.readyToRender = true;
			// });
			this.theTimeSeries[1] = new $D.LinearTimeSeries({
				id : 1
			});
            this.theTimeSeries[1] = new $D.LinearTimeSeries({
				id : 1
			});
			this.theTimeSeries[2] = new $D.PathTimeSeries({
				id : 2
			});
            // this.theTimeSeries[2] = new $D.TriangleTimeSeries({
			// 	id : 2
			// });

			this.thePatterns[1] = new $D.PlainPattern({
				id : 1,
				Loads : {},
				TimeSeries : this.theTimeSeries[1]
			});

			this.thePatterns[2] = new $D.UniformExcitationPattern({
				id : 2,
				Direction : 1,
				TimeSeries : this.theTimeSeries[2]
			});

			this.theAnalysisSettngs["staticDefault"] = new $D.AnalysisSetting({
				theConstraints : (new $D.Constraints({
					type : "Plain"
				})),
				theNumberer : (new $D.Numberer({
					type : "Plain"
				})),
				theSystem : (new $D.System({
					type : "BandGeneral"
				})),
				theTest : (new $D.Test({
					type : "NormDispIncr",
					tol : 1.0e-8,
					iter : 15
				})),
				theAlgorithm : (new $D.Algorithm({
					type : "Newton"
				})),
				theIntegrator : (new $D.StaticIntegrator({
					type : "LoadControl",
					lambda : 0.1
				})),
				theAnalysis : (new $D.Analysis({
					type : "Static"
				}))
			});

			this.theAnalysisSettngs["transientDefault"] = new $D.AnalysisSetting({
				theConstraints : (new $D.Constraints({
					//type : "Plain"
				})),
				theNumberer : (new $D.Numberer({
					//type:"Plain"
				})),
				theSystem : (new $D.System({
					//type:"BandGeneral"
				})),
				theTest : (new $D.Test({
					type : "NormDispIncr",
					tol : 1.0e-8,
					iter : 15
				})),
				theAlgorithm : (new $D.Algorithm({
					//type:"Newton"
				})),
				theIntegrator : (new $D.TransientIntegrator({
					type : "Newmark",
					gamma : 0.5,
					beta : 0.25
				})),
				theAnalysis : (new $D.Analysis({
					type:"Transient"
					// dt
				}))
			});

			this.theAnalysisSettngs[1] = $D.clone(this.theAnalysisSettngs["staticDefault"]);
			this.theAnalysisSettngs[2] = $D.clone(this.theAnalysisSettngs["transientDefault"]);

			this.currentTimeSeries = this.theTimeSeries[1];
			this.currentPattern = this.thePatterns[1];
			this.currentAnalysisSetting = this.theAnalysisSettngs["staticDefault"];

			$D.apply(this, {
				
				"setConnection" : function(ws) {
					this.ws=ws;
					this.send = this.ws.send;
				},
				"applyDefaultStaticAnalysisSettings" : function() {
					this.theAnalysisSettngs[1] = $D.clone(this.theAnalysisSettngs["staticDefault"]);
				},
				"applyDefaultDynamicAnalysisSettings" : function() {
					this.theAnalysisSettngs[1] = $D.clone(this.theAnalysisSettngs["transientDefault"]);
				},
				// private functions:
				"restart" : function() {
					this.deformationAvailable = false;
					this.changed = false;
					this.theNodes = {};
					this.theElements = {};
					this.theSPCs = {};
					this.thePatterns[1].Loads = {};
					this.selectedObjects = {};
					this.selectedNodes = {};
					this.selectedElements = {};
					this.selectedLoads = {};
					this.selectedSPCs = {};
					this.clearHistory();
				},
				"pickTag" : function(storeSelector) {
					var i = 1;
					while($D.isDefined(this.get($D.splat(storeSelector).concat(i)))) {
						i++;
					}
					return i; + " "+ this.dt
				},
				"count" : function(store) {
					var i = 0;
					$D.iterate(store, function(){
						i++;
					})
					return i;
				},
				// public functions, return true or false to indicate the execute state
				"addComponent" : function(arg) {
					var id = this.pickTag(arg.storeSelector), idName = arg.idName || "id";
					arg.item[idName] = id;
					this.set($D.splat(arg.storeSelector).concat(id), arg.item);
					return true;
				},
				"removeComponent" : function(arg) {
					this.set(arg.componentSelector);
					return true;
				},
				// "wipeStore" : function(arg) {
					// var store = this.get(arg.storeSelector), i;
					// for(i in store) {
						// if(store.hasOwnProperty(i) && $D.isDefined(store[i])) {
							// this.set(arg.storeSelector + "." + store[i].id)
						// }
					// }
					// return true;
				// },
				// "splitLineElement" : function(arg) {
				// var nSel = arg.nodeSelector, eSel = arg.elementSelector, //
				// n, e, ne;
				// n = this.get(nSel);
				// e = this.get(eSel);
				// ne = new e.constructor({
				// from : n,
				// to : e.getEnd(),
				// geomTransf : e.geomTransf
				// });
				// this.set(eSel + ".getEnd()", n);
				// this.addComponent({
				// storeSelector : "theElements",
				// item : ne
				// });
				// this.set(nSel + ".eleCount", 2);
				// return true;
				// },
				"_getLocalNodeId" : function(globalNodeId, elementId) {
					// for (var i=0;i<this.theElements[elementId].nodes.length;i++){
					// if (this.theElements[elementId].nodes[i].id == globalNodeId){
					// return i;
					// }
					// }
					// return -1
					//return this.theNodes[globalNodeId].getLocalId(elementId);
					return this.theElements[elementId].getLocalNodeId(globalNodeId);
				},
				"snapToNode" : function(node, t) {

					return snapToNode(node, t, this.theNodes);
				},
				"snapToLine":function(node,t){
					return snapToLine(node, t, this.theElements);
					
				},
				"snapTo4Direction":function(angle,t){
					return snapTo4Direction(angle,t);
					
				},
				"equalDivideLineElement" : function(l, n) {
					var i, ratio = [];
					for( i = 1; i < n; i++) {
						ratio.push(i / n);
					}
					return this.splitLineElement(l, ratio);
				},
				"splitLineElement" : function(l, ratio, constraintOnLine) {
					var lid = l.id;
					var Node = $D.Node;
					var eSel = ["theElements", lid];
					var e = this.get(eSel);
					var constraintOnLine = $D.isDefined(constraintOnLine) ? constraintOnLine : false;
					// console.log("ele", e)

					if($D.isNumber(ratio)) {
						ratio = [ratio];
					}
					if($D.isArray(ratio) && ratio.length > 0) {
						ratio.sort();
						if(ratio[0] <= 0 || ratio[ratio.length - 1] >= 1) {
							return false;
						}
						// this.set(eSel);
						this.removeLineElement(e);
						var i, len = ratio.length, nodes = [], nids = [], x0 = e.getFrom().X, //
						dx = e.getEnd().X - x0, y0 = e.getFrom().Y, dy = e.getEnd().Y - y0, opt = {
							geomTransf : e.geomTransf
						};
						nodes.push(e.getFrom());
						
						for( i = 0; i < len; i++) {
							if (constraintOnLine) {
								nodes.push(this.createNode(x0 + ratio[i] * dx, y0 + ratio[i] * dy,{
									constraintOnLine:l
								}));
							} else {
								nodes.push(this.createNode(x0 + ratio[i] * dx, y0 + ratio[i] * dy));
								
							}
							// TODO: copeyelement property
							this.createLineElement(e.className, nodes[i], nodes[i+1], opt)
						}
						this.createLineElement(e.className, nodes[i], e.getEnd(), opt)

						if(nodes.length == 2) {
							return nodes[1];
						} else {
							return nodes.slice(1);
						}
					} else {
						return false;
					}

				},
				// keep n2, redirect all the referrence of n1 to n2
				"mergeNodes" : function(n1, n2) {
					var i, localId, e, anotherEnd;
					for(i in n1.elements) {
						if(n1.elements.hasOwnProperty(i) && $D.isDefined(n1.elements[i])) {
							// TODO, check the elements has n2, determin delete or not, save adjacent nodes

							e = n1.elements[i];
							localId = e.getLocalNodeId(n1.id);
							anotherEnd = e.nodes[1 - localId];

							if( e instanceof $D.LineElement) {
								this.removeLineElement(e);
								if(anotherEnd.id == n2.id) {
									

								} else if($D.isDefined(anotherEnd.ajacentNodes[n2.id]) && anotherEnd.ajacentNodes[n2.id].id == n2.id) {
									// this.set(["theElements",e.id]);

								} else {
									if(localId == 0) {
										this.createLineElement(e.className, n2, anotherEnd, e.getProperty());
									} else {
										this.createLineElement(e.className, anotherEnd, n2, e.getProperty());
									}
								}

							}

// 
// 
// 
// 
// 
							// } else {
								// // e.nodes[localId]
								// // this.set(["theElements",e.id,"nodes",localId],n2);
								// // if (localId == 0){
								// // this.set(["theElements",e.id,"from"],n2);
								// // } else {
								// // this.set(["theElements",e.id,"to"],n2);
								// // }
								// // if ($D.isDefined(n1.SPC)){
								// // this.set(["theSPCs",n1.SPC.id,"node"],n2);
								// // this.set(["theNodes",n2.id,"SPC"],n1.SPC);
								// // }
								// // this.set(["theNodes",n2.id,"elements",e.id],e);
								// // this.set(["theNodes",n2.id,"eleCount"],this.theNodes[n2.id].eleCount+1);
								// // this.set(["theNodes",n2.id,"ajacentNodes",anotherEnd.id],anotherEnd);
								// // this.set(["theNodes",anotherEnd.id,"ajacentNodes",n2.id],n2);
// 
								// if( e instanceof $D.LineElement) {
									// this.removeLineElement(e);
									// if(localId == 0) {
										// this.createElement(e.className, anotherEnd, n2, e.getProperty());
									// } else {
										// this.createElement(e.className, n2, anotherEnd, e.getProperty());
									// }
								// }
// 
							// }

							// oe  =

						}

					}
					// this.commit();
					// this._removeANode(["theNodes",n1.id]);
					this.removeNode(n1);
					return n2;
					// this.set(["theNodes",n1.id]);
					// this.commit();

				},
				"mesh" : function() {

				},
				"createNode" : function(x, y, opt) {
					var n = new $D.Node({
						X : x,
						Y : y
					});
					$D.apply(n, opt);
					this.addComponent({
						storeSelector : "theNodes",
						item : n
					})
					return n;
				},
				"removeNode" : function(n) {
					// Do not call it unless you are sure the node is isolate
					if(n.isSelected) {
						this.set(["selectedObjects", n.selectedId]);
						this.set(["selectedNodes", n.selectedNodeId]);
					}
					this.set(["theNodes",n.id]);

				},
				"createElement" : function(type, nodes, opt) {
					var from = nodes[0], to = nodes[1], //
					e = new $D[type]({
						nodes : nodes
					});
					e.calculateMass();
					$D.apply(e, opt);
					this.addComponent({
						storeSelector : "theElements",
						item : e
					})

					this.set(["theNodes", from.id, "eleCount"], this.theNodes[from.id].eleCount + 1);
					this.set(["theNodes", from.id, "elements", e.id], e);
					this.set(["theNodes", from.id, "massX"], this.theNodes[from.id].massX+e.mass/e.nodeNum);
					this.set(["theNodes", from.id, "massY"], this.theNodes[from.id].massY+e.mass/e.nodeNum);
					this.set(["theNodes", from.id, "massRz"], this.theNodes[from.id].massRz+e.mass/e.nodeNum);

					this.set(["theNodes", to.id, "eleCount"], this.theNodes[to.id].eleCount + 1);
					this.set(["theNodes", to.id, "elements", e.id], e);
					this.set(["theNodes", to.id, "massX"], this.theNodes[to.id].massX+e.mass/e.nodeNum);
					this.set(["theNodes", to.id, "massY"], this.theNodes[to.id].massY+e.mass/e.nodeNum);
					this.set(["theNodes", to.id, "massRz"], this.theNodes[to.id].massRz+e.mass/e.nodeNum);
					
					

					return e
				},
				"removeElement" : function(e) {
					var i;
					for( i = 0; i < e.nodes.length; i++) {
						this.set(["theNodes", e.nodes[i].id, "elements", e.id]);
						this.set(["theNodes", e.nodes[i].id, "eleCount"], this.theNodes[e.nodes[i].id].eleCount - 1);
						this.set(["theNodes", e.nodes[i].id, "massX"], this.theNodes[e.nodes[i].id].massX-e.mass/e.nodeNum);
						this.set(["theNodes", e.nodes[i].id, "massY"], this.theNodes[e.nodes[i].id].massY-e.mass/e.nodeNum);
						this.set(["theNodes", e.nodes[i].id, "massRz"], this.theNodes[e.nodes[i].id].massRz-e.mass/e.nodeNum);
					}
					if(e.isSelected) {
						this.set("selectedObjects." + e.selectedId);
						this.set("selectedElements." + e.selectedElementId);
					}
					this.set(["theElements", e.id]);
				},
				"createLineElement" : function(type, from, to, opt) {
					var e = this.createElement(type, [from, to], opt);
					this.set(["theNodes", from.id, "ajacentNodes", to.id], to);
					this.set(["theNodes", to.id, "ajacentNodes", from.id], from);
					return e
				},
				"removeLineElement" : function(e) {
					this.set(["theNodes", e.getFrom().id, "ajacentNodes", e.getEnd().id]);
					this.set(["theNodes", e.getEnd().id, "ajacentNodes", e.getFrom().id]);
					this.removeElement(e);
				},
				"createSPC" : function(node, opt) {
					var spc = new $D.SPConstraint({
						node : node
					});
					$D.apply(spc, opt);
					this.addComponent({
						storeSelector : "theSPCs",
						item : spc
					})
					this.set(["theNodes", node.id, "SPC"], spc);
					return spc
				},
				"removeSPC" : function(spc) {
					this.set(["theNodes",spc.node.id,"SPC"]);
					this.set(["theSPCs", spc.id]);
					
				},
				"createNodeLoad": function(node, freeEnd, nodeAtArrowEnd, x, y, rz, opt){
					var load = new $D.NodeLoad({
						node : node,
						freeEnd : freeEnd,
						nodeAtArrowEnd : nodeAtArrowEnd,
						X : x,
						Y : y,
						RZ : rz
					});
					$D.apply(load, opt);
					this.addComponent({
						storeSelector : ["currentPattern","Loads"],
						item : load
					});
					this.set(["theNodes",node.id,"nodeLoads",load.id],load);
					return load;
				},
				"removeNodeLoad": function(l) {
					this.set(["theNodes",l.node.id,"nodeLoads",l.id]);
					this.set(["currentPattern","Loads", l.id]);
					
				},
				
				"createUniformLoad": function(start,end,opt){
					var load = new $D.UniformLoad({
						node : start,
						freeEnd : end,
						// nodeAtArrowEnd : nodeAtArrowEnd,
						// X : x,
						// Y : y,
						// RZ : rz
					});
					$D.apply(load, opt);
					this.addComponent({
						storeSelector : ["currentPattern","Loads"],
						item : load
					});
					// this.set(["theNodes",node.id,"nodeLoads",load.id],load);
					return load;
				},
				"removeUniformLoad": function(l) {
					// this.set(["theNodes",l.node.id,"nodeLoads",l.id]);
					this.set(["currentPattern","Loads", l.id]);
					
				},
				
				
				"_selectANode" : function(nSelector) {
					this.set(nSelector + ".isSelected", true);
					this.addComponent({
						storeSelector : "selectedObjects",
						item : this.get(nSelector),
						idName : "selectedId"
					});
					this.addComponent({
						storeSelector : "selectedNodes",
						item : this.get(nSelector),	
						idName : "selectedNodeId"
					});
					var n = this.get(nSelector)
					if ($D.isDefined(n.SPC)) {
						this.set(["theSPCs",n.SPC.id,"isSelected"], true);
						this.addComponent({
							storeSelector : "selectedObjects",
							item : n.SPC,
							idName : "selectedId"
						});
						this.addComponent({
							storeSelector : "selectedSPCs",
							item : n.SPC,	
							idName : "selectedSPCId"
						});
					}
					
				},
				"_unselectANode" : function(nSelector) {
					var n = this.get(nSelector);
					this.set(nSelector + ".isSelected", false);
					this.removeComponent({
						componentSelector : "selectedNodes." + n.selectedNodeId
					});
					this.removeComponent({
						componentSelector : "selectedObjects." + n.selectedId
					});
					// var n = this.get(nSelector);
					if ($D.isDefined(n.SPC)) {
						this.set(["theSPCs",n.SPC.id,"isSelected"], false);
						this.removeComponent({
							componentSelector : "selectedSPCs." + n.SPC.selectedSPCId
						});
						this.removeComponent({
							componentSelector : "selectedObjects." + n.SPC.selectedId
						});
					}
					
				},
				"_toggleSelectANode" : function(nSelector) {
					if(this.get(nSelector).isSelected) {
						this._unselectANode(nSelector);
					} else {
						this._selectANode(nSelector);
					}
				},
				"_selectAElement" : function(eSelector) {
					this.set(eSelector + ".isSelected", true);
					this.addComponent({
						storeSelector : "selectedObjects",
						item : this.get(eSelector),
						idName : "selectedId"
					});
					this.addComponent({
						storeSelector : "selectedElements",
						item : this.get(eSelector),
						idName : "selectedElementId"
					});
				},
				"_unselectAElement" : function(eSelector) {
					var e = this.get(eSelector);
					this.set(eSelector + ".isSelected", false);
					this.removeComponent({
						componentSelector : "selectedElements." + e.selectedElementId
					});
					this.removeComponent({
						componentSelector : "selectedObjects." + e.selectedId
					});
				},
				"_toggleSelectAElement" : function(eSelector) {
					if(this.get(eSelector).isSelected) {
						this._unselectAElement(eSelector);
					} else {
						this._selectAElement(eSelector);
					}
				},
				"_selectALoad" : function(eSelector) {
					this.set(eSelector + ".isSelected", true);
					this.addComponent({
						storeSelector : "selectedObjects",
						item : this.get(eSelector),
						idName : "selectedId"
					});
					this.addComponent({
						storeSelector : "selectedLoads",
						item : this.get(eSelector),
						idName : "selectedLoadId"
					});
				},
				"_unselectALoad" : function(eSelector) {
					var l = this.get(eSelector)
					this.set(eSelector + ".isSelected", false);
					this.removeComponent({
						componentSelector : "selectedLoads." + l.selectedLoadId
					});
					this.removeComponent({
						componentSelector : "selectedObjects." + l.selectedId
					});
				},
				"_toggleSelectALoad" : function(eSelector) {
					if(this.get(eSelector).isSelected) {
						this._unselectALoad(eSelector);
					} else {
						this._selectALoad(eSelector);
					}
				},
				"unselectAllNodes" : function() {
					var i, flag = false;
					for(i in this.selectedNodes) {
						if(this.selectedNodes.hasOwnProperty(i) && $D.isDefined(this.selectedNodes[i])) {
							this._unselectANode("selectedNodes." + i);
							flag = true;
						}
					}
					return flag;
				},
				"unselectAllElements" : function() {
					var i, flag = false;
					for(i in this.selectedElements) {
						if(this.selectedElements.hasOwnProperty(i) && $D.isDefined(this.selectedElements[i])) {
							this._unselectAElement("selectedElements." + this.selectedElements[i].selectedElementId);
							flag = true;
						}
					}
					return flag;
				},
				"unselectAllLoads" : function() {
					var i, flag = false;
					for(i in this.selectedLoads) {
						if(this.selectedLoads.hasOwnProperty(i) && $D.isDefined(this.selectedLoads[i])) {
							this._unselectALoad("selectedLoads." + this.selectedLoads[i].selectedLoadId);
							flag = true;
						}
					}
					return flag;
				},
				"unselectAllLoadFreeEnds":function(){
					var i, flag = false;
					for(i in this.selectedLoads) {
						if(this.selectedLoads.hasOwnProperty(i) && $D.isDefined(this.selectedLoads[i])) {
							this._unselectANode("selectedLoads." + this.selectedLoads[i].id+".freeEnd");
							flag = true;
						}
					}
					return flag;
					
				},
				"unselectAll" : function() {
					var flag1 = this.unselectAllNodes();
					var flag2 = this.unselectAllElements(); 
					var flag3 = this.unselectAllLoads();
					var flag4 = this.unselectAllLoadFreeEnds();
					return flag1 || flag2 || flag3 || flag4;
				},
				"removeSelectedObjects" : function() {
					var i, flag = false, e;

                    for(i in this.selectedElements) {
						if(this.selectedElements.hasOwnProperty(i) && $D.isDefined(this.selectedElements[i])) {
							e = this.selectedElements[i];
							if( e instanceof $D.LineElement) {
								this.removeLineElement(this.selectedElements[i]);
							}
							flag = true;
						}
					}
                    
					for(i in this.selectedSPCs) {
						if(this.selectedSPCs.hasOwnProperty(i) && $D.isDefined(this.selectedSPCs[i])) {
							this.removeSPC(this.selectedSPCs[i]);
							flag = true;
						}
					}
					for(i in this.selectedLoads) {
						if(this.selectedLoads.hasOwnProperty(i) && $D.isDefined(this.selectedLoads[i])) {
							this.removeNodeLoad(this.selectedLoads[i]);
							flag = true;
						}
					}

                    for(i in this.selectedNodes) {
						if(this.selectedNodes.hasOwnProperty(i) && $D.isDefined(this.selectedNodes[i])) {
							e = this.selectedNodes[i];
							if( e instanceof $D.Node && e.eleCount === 0 && !$D.isDefined(e.SPC) && $D.isEmptyObject(e.nodeLoads)) {
								this.removeNode(this.selectedNodes[i]);
							}
							flag = true;
						}
					}
					return flag;
				},
				"circleSelect" : function(arg) {
					console.log("circleSelect, arguments:", arguments);
					var poly = arg.poly, i, flag = false, selectTags = [],unSelectTags = [];

					for(i in this.theNodes) {
						if(this.theNodes.hasOwnProperty(i) && $D.isDefined(this.theNodes[i])) {
							if(isPointInPoly(poly, this.theNodes[i])) {
								this._toggleSelectANode("theNodes." + this.theNodes[i].id);
								if (this.theNodes[i].isSelected) {
									selectTags.push(i);
								} else {
									unSelectTags.push(i);
								}
								// flag = true;
								// tags.push(i);
							}
						}
					}

					for(i in this.selectedNodes) {
						if(this.selectedNodes.hasOwnProperty(i) && $D.isDefined(this.selectedNodes[i])) {
                            // debugger;
                            for (var j in this.selectedNodes[i].elements) {
                                if(this.selectedNodes[i].elements.hasOwnProperty(j)) {
                                    var ele = this.selectedNodes[i].elements[j];
                                    var selected = true;
                                    for (var k = 0, len = ele.nodes.length; k < len; k++) {
                                        if (ele.nodes[k].isSelected !== true) {
                                            selected = false;
                                            break;
                                        }
                                    }
                                    if (selected !== ele.isSelected) {
                                        this._toggleSelectAElement("theElements." + ele.id);
										if (ele.isSelected) {
											selectTags.push(ele.id + 'ele');
										} else {
											unSelectTags.push(ele.id + 'ele');
										}
                                    }

                                    // debugger;
                                    // var nload = 
                                    

                                }
                            }

						}
					}
                    // debugger;

                    
					for(i in this.currentPattern.Loads) {
						if(this.currentPattern.Loads.hasOwnProperty(i) && $D.isDefined(this.currentPattern.Loads[i])) {
							if(isPointInPoly(poly, this.currentPattern.Loads[i].freeEnd)) {
								this._toggleSelectANode("currentPattern.Loads."+i+".freeEnd");
								if (this.currentPattern.Loads[i].freeEnd.isSelected) {
									selectTags.push(i+",load");
								} else {
									unSelectTags.push(i+",load");
								}
								// flag = true;
								// tags.push(i);
							}
						}
					}
                    
                    for(i in this.currentPattern.Loads) {
						if(this.currentPattern.Loads.hasOwnProperty(i) && $D.isDefined(this.currentPattern.Loads[i])) {
							if (this.currentPattern.Loads[i].node.isSelected === true &&
                                this.currentPattern.Loads[i].freeEnd.isSelected === true){
                                this._toggleSelectALoad("currentPattern.Loads." + this.currentPattern.Loads[i].id);
								if (this.currentPattern.Loads[i].isSelected) {
									selectTags.push(i+"load");
								} else {
									unSelectTags.push(i+"load");
								}
                            }
						}
					}

					if (selectTags.length === 0 && unSelectTags.length === 0) {
						return false;
					} else {
						return {
							select:selectTags,
							unselect:unSelectTags
						};
					}
				},
				"clickSelect" : function(arg) {
					console.log("clickSelect, arguments:", arguments);
					var P = {
						X : arg.X,
						Y : arg.Y
					}, flag = false;
					return flag;
				},
				"intersectSelect" : function(arg) {
					console.log("intersectSelect, arguments:", arguments);
					var curve = arg.curve, flag = false,selectTags = [],unSelectTags = [], i, len = curve.length, j, bitmap = {};
					for( i = 0; i < len - 1; i++) {
						for(j in this.theElements) {
							if(this.theElements.hasOwnProperty(j) && $D.isDefined(this.theElements[j])) {
								if(lineSegmentIntersect(curve[i], curve[i + 1], this.theElements[j].getFrom(), this.theElements[j].getEnd())) {
									if(!bitmap[j]) {
										bitmap[j] = true;
										this._toggleSelectAElement("theElements." + this.theElements[j].id);
										if (this.theElements[j].isSelected) {
											selectTags.push(j);
										} else {
											unSelectTags.push(j);
										}
									}
								}
							}
						}
						bitmap = {};
						for(j in this.currentPattern.Loads) {
							if(this.currentPattern.Loads.hasOwnProperty(j) && $D.isDefined(this.currentPattern.Loads[j])) {
								if(lineSegmentIntersect(curve[i], curve[i + 1], this.currentPattern.Loads[j].getFrom(), this.currentPattern.Loads[j].getEnd())) {
									if(!bitmap[j]) {
										bitmap[j] = true;
										this._toggleSelectALoad("currentPattern.Loads." + this.currentPattern.Loads[j].id);
										if (this.currentPattern.Loads[j].isSelected) {
											selectTags.push(j+"load");
										} else {
											unSelectTags.push(j+"load");
										}
									}
								}
							}
						}
					}
					if (selectTags.length === 0 && unSelectTags.length === 0) {
						return false;
					} else {
						return {
							select:selectTags,
							unselect:unSelectTags
						}
					}
					// return flag;
				},
				// "transitNode" : function(nid,dx,dy) {
					// var n = this.get("theNodes." + nid);
					// var nowX = n.X;
					// var nowY = n.Y;
					// n.X = n.beforeMoveX;
					// n.Y = n.beforeMoveY;
					// this.set(["theNodes",nid,"X"], nowX);
					// this.set(["theNodes",nid,"Y"], nowY);
					// this.set(["theNodes",nid,"beforeMoveX"], nowX);
					// this.set(["theNodes",nid,"beforeMoveY"], nowX);
				// },
				"transitNode" : function(selector,dx,dy) {
					var n = this.get(selector);
					// var nowX = n.beforeMoveX + dx;
					// var nowY = n.beforeMoveY + dy;
					n.X = n.beforeMoveX;
					n.Y = n.beforeMoveY;
					n.move(dx,dy);
					var nowX = n.X;
					var nowY = n.Y;
					n.X = n.beforeMoveX;
					n.Y = n.beforeMoveY;
					this.set($D.splat(selector).concat("X"), nowX);
					this.set($D.splat(selector).concat("Y"), nowY);
					this.set($D.splat(selector).concat("beforeMoveX"), nowX);
					this.set($D.splat(selector).concat("beforeMoveY"), nowY);
				},
				"moveNode":function(n,dx,dy){
					// this.theNodes[nid].X +=dx;
					// this.theNodes[nid].Y +=dy;
					n.move(dx,dy);
					
				},
				"moveSelectedNodes" : function(dx,dy) {
					var i, flag = false;
					for(i in this.selectedNodes) {
						if(this.selectedNodes.hasOwnProperty(i) && $D.isDefined(this.selectedNodes[i])) {
							this.moveNode(this.selectedNodes[i].id,dx,dy)
							flag = true;
						}
					}
					return flag;
				},
				"moveSelectedObjects" : function(dx,dy) {
					var i, flag = false, escapedNodes = {};
					for(i in this.selectedElements) {
						if(this.selectedElements.hasOwnProperty(i) && $D.isDefined(this.selectedElements[i])) {
							escapedNodes[this.selectedElements[i].getFrom().id] = true;
							this.moveNode(this.selectedElements[i].getFrom(),dx,dy);
							escapedNodes[this.selectedElements[i].getEnd().id] = true;
							this.moveNode(this.selectedElements[i].getEnd(),dx,dy);
							flag = true;
						}
					}
					for(i in this.selectedLoads) {
						if(this.selectedLoads.hasOwnProperty(i) && $D.isDefined(this.selectedLoads[i])) {
							this.selectedLoads[i].node.enableConstraint = true;
							this.selectedLoads[i].freeEnd.enableConstraint = true;
							if (this.selectedLoads[i].getFrom().id) {
								escapedNodes[this.selectedLoads[i].getFrom().id] = true;
							}
							this.moveNode(this.selectedLoads[i].getFrom(),dx,dy);
							if (this.selectedLoads[i].getEnd().id) {
								escapedNodes[this.selectedLoads[i].getEnd().id] = true;
							}
							this.moveNode(this.selectedLoads[i].getEnd(),dx,dy);
							flag = true;
						}
					}
					for(i in this.selectedNodes) {
						if(this.selectedNodes.hasOwnProperty(i) && $D.isDefined(this.selectedNodes[i]) && !escapedNodes[this.selectedNodes.id]) {
							this.selectedNodes[i].enableConstraint = false;
							this.moveNode(this.selectedNodes[i],dx,dy)
							this.selectedNodes[i].enableConstraint = true;
							flag = true;
						}
					}
					return flag;
				},
				"transitSelectedNodes" : function(dx,dy) {
					var i, flag = false,count = 0;
					for(i in this.selectedNodes) {
						if(this.selectedNodes.hasOwnProperty(i) && $D.isDefined(this.selectedNodes[i])) {
							this.transitNode(this.selectedNodes[i].id,dx,dy)
							flag = true;
							count ++
						}
					}
					return count;
				},
				"transitSelectedObjects" : function(dx,dy) {
					var i, flag = false,count = 0, escapedNodes = {};
					for(i in this.selectedElements) {
						if(this.selectedElements.hasOwnProperty(i) && $D.isDefined(this.selectedElements[i])) {
							escapedNodes[this.selectedElements[i].getFrom().id] = true;
							this.transitNode(["theNodes",this.selectedElements[i].getFrom().id],dx,dy)
							escapedNodes[this.selectedElements[i].getEnd().id] = true;
							this.transitNode(["theNodes",this.selectedElements[i].getEnd().id],dx,dy)
							flag = true;
							count ++
						}
					}
					for(i in this.selectedLoads) {
						if(this.selectedLoads.hasOwnProperty(i) && $D.isDefined(this.selectedLoads[i])) {
							var sel;
							this.selectedLoads[i].node.enableConstraint = true;
							this.selectedLoads[i].freeEnd.enableConstraint = true;
							if ($D.isDefined(this.selectedLoads[i].getFrom().id)) {
								sel = ["theNodes",this.selectedLoads[i].getFrom().id];
							} else {
								escapedNodes[this.selectedLoads[i].getFrom().id] = true;
								sel = ["currentPattern","Loads",this.selectedLoads[i].id,"freeEnd"];
							}
							this.transitNode(sel,dx,dy);
							if ($D.isDefined(this.selectedLoads[i].getEnd().id)) {
								sel = ["theNodes",this.selectedLoads[i].getEnd().id];
							} else {
								escapedNodes[this.selectedLoads[i].getEnd().id] = true;
								sel = ["currentPattern","Loads",this.selectedLoads[i].id,"freeEnd"];
							}
							this.transitNode(sel,dx,dy);
							// if (this.selectedLoads[i].getEnd().id) {
								// escapedNodes[this.selectedLoads[i].getEnd().id] = true;
							// }
							// this.transitNode(this.selectedLoads[i].getEnd(),dx,dy);
							flag = true;
						}
					}
					for(i in this.selectedNodes) {
						if(this.selectedNodes.hasOwnProperty(i) && $D.isDefined(this.selectedNodes[i]) && !escapedNodes[this.selectedNodes.id]) {
							// this.moveNode(this.selectedNodes[i],dx,dy)
							// this.transitNode(["theNodes",this.selectedNodes[i].id],dx,dy)
							this.selectedNodes[i].enableConstraint = false;
							this.transitNode(["selectedNodes",this.selectedNodes[i].selectedNodeId],dx,dy)
							this.selectedNodes[i].enableConstraint = true;
							flag = true;
						}
					}
					// for(i in this.selectedNodes) {
						// if(this.selectedNodes.hasOwnProperty(i) && $D.isDefined(this.selectedNodes[i])) {
							// this.transitNode(["theNodes",this.selectedNodes[i].id],dx,dy)
							// flag = true;
							// count ++
						// }
					// }
					return count;
				},
				//clear all objects in root data
				//args:{key:"all" or array}
				"wipeAll" : function() {
					console.log("wipe All")
					this.wipeStore({
						storeSelector : "theNodes"
					});
					this.wipeStore({
						storeSelector : "theElements"
					});
					this.wipeStore({
						storeSelector : "theSPCs"
					});
					this.wipeStore({
						storeSelector : "thePatterns.1.Loads"
					});
					return true;
				},
				"isReadyToRun" : function() {
					return !$D.isEmptyObject(this.currentPattern.Loads);
				},
				"isDeformationAvailable" : function() {
					// return !$D.isEmptyObject(this.nodeDispHistory);
					return this.deformationAvailable;
				},
				"setModelScale" : function(s) {
					this.modelScale = s;
				},
				"setLoadScale" : function(s) {
					this.loadScale = s;
				},
				"storeToTcl" : function(store) {
					var i, result = "";
					for(i in store) {
						if(store.hasOwnProperty(i) && $D.isDefined(store[i])) {
							var str = store[i].toTcl();
							result += str + ";\n";
						}
					}
					return result;
				},
				"buildModel" : function() {
					var result = "";
					result += "wipe;\n";
					result += "model basic -ndm 2 -ndf 3\n";
					result += this.storeToTcl(this.theGeomTransfs);
					result += this.storeToTcl(this.theNodes);
					result += this.storeToTcl(this.theElements);
					result += this.storeToTcl(this.theSPCs);
					//result += this.storeToTcl(this.theTimeSeries);

					//result+=this.thePatterns.toTcl(modelScale,loadScale);
					return result;
				},
				"setPattern" : function(type) {
					if (type == 'dynamic') {
						this.currentPattern = this.thePatterns[2];
					} else {
						this.currentPattern = this.thePatterns[1];
					};
					var result = "";
					result += this.currentPattern.TimeSeries.toTcl() + ";\n";
					result += this.currentPattern.toTcl() + ";\n";
					return result;
				},
				"setAnalysis" : function() {
					return this.currentAnalysisSetting.toTcl();
				},
				"runEigen" : function(num) {
					var result = "";
					result += this.run("buildModel");
					result += this.run("setPattern");
					result += this.run("setAnalysis");
					result += "eigen " + num + ";";
					return result;

				},
				"runAnalysis" : function(num, dt) {
					var result = "";
					result += this.buildModel();
					result += this.setPattern();
					result += this.setAnalysis();
					result += "analyze " + num + " " + dt + ";";
					return result;
				},
				"runStaticConstant" : function() {
					var result = "";
					result += this.buildModel();
					result += this.setPattern();
					result += this.setAnalysis();
					result += "analyze 10;json-echo-disp\n";
					// result += "analyzeAndStreamNodeDisp 10\n";
					// result += "analyze 10;\n";
					// result += "puts [allNodeDisp];\n";
					// result += "allNodeDisp;\n";
					// if(Deformation === true) {
						// result += "puts -nonewline [getTime];\n";
						// result += "puts -nonewline \" \";\n";
						// result += "foreach key [getNodeTags] {;\n";
						// result += "puts -nonewline [string map {\" \" \"\"} [nodeDisp $key 1]];\n";
						// result += "puts -nonewline \" \";\n";
						// result += "puts -nonewline [string map {\" \" \"\"} [nodeDisp $key 2]];\n";
						// result += "puts -nonewline \" \";\n";
						// result += "puts -nonewline [string map {\" \" \"\"} [nodeDisp $key 3]];\n";
						// result += "puts -nonewline \" \";\n";
						// result += "}\n";
					// // }
					// // if(Moment === true) {
						// result += "puts \";\";\n";
						// result += "foreach key [getEleTags] {;\n";
						// result += "puts [regsub -all {\\s+} [string trim [eleResponse $key forces]] \" \"];\n";
						// result += "}\n";
					// }
					// console.log("tcl",result)
					return result;
					// return "test"
				},
				"setUpDynamicsAnalysis" :function(nsteps) {
					
					var result = "";
					if (!nsteps) {
						nsteps = 100;
					}
					result += this.buildModel();
					result += this.setPattern('dynamic');
					this.currentAnalysisSetting = this.theAnalysisSettngs["transientDefault"];
					
					result += this.setAnalysis();
					
					// result += "set counter 1;\n";
					// result += "while {$counter < "+nsteps+"} {\n";
// 					
					// result += "analyze 1 0.02;\n";
					// result += "puts [allNodeDisp];\n";
					// result += "incr counter;\n";
					// result += "}\n";
					// if(Deformation === true) {
						// result += "puts -nonewline [getTime];\n";
						// result += "puts -nonewline \" \";\n";
						// result += "foreach key [getNodeTags] {;\n";
						// result += "puts -nonewline [string map {\" \" \"\"} [nodeDisp $key 1]];\n";
						// result += "puts -nonewline \" \";\n";
						// result += "puts -nonewline [string map {\" \" \"\"} [nodeDisp $key 2]];\n";
						// result += "puts -nonewline \" \";\n";
						// result += "puts -nonewline [string map {\" \" \"\"} [nodeDisp $key 3]];\n";
						// result += "puts -nonewline \" \";\n";
						// result += "}\n";
					// }
					// if(Moment === true) {
						// result += "puts \";\";\n";
						// result += "foreach key [getEleTags] {;\n";
						// result += "puts [regsub -all {\\s+} [string trim [eleResponse $key forces]] \" \"];\n";
						// result += "}\n";
					// }
					// console.log("tcl",result)
					return result;
					// return "test"
				},
				"runDynamics" : function(nsteps) {
					
					var result = "";
					if (!nsteps) {
						nsteps = 1000;
					}
					result += this.buildModel();
					result += this.setPattern('dynamic');
					this.currentAnalysisSetting = this.theAnalysisSettngs["transientDefault"];
					
					result += this.setAnalysis();
					// result += "analyzeAndStreamNodeDisp "+nsteps+" 0.02\n";
					// result += "while analyzeAndStreamNodeDisp "+nsteps+" 0.02\n";
					
					
					result += "set counter 1;\n";
					result += "while {$counter < "+ nsteps +"} {\n";
// 					
					result += "analyze 1 0.02;\n";
					result += "json-echo-disp;\n";
					result += "incr counter;\n";
					result += "}\n";
					// if(Deformation === true) {
						// result += "puts -nonewline [getTime];\n";
						// result += "puts -nonewline \" \";\n";
						// result += "foreach key [getNodeTags] {;\n";
						// result += "puts -nonewline [string map {\" \" \"\"} [nodeDisp $key 1]];\n";
						// result += "puts -nonewline \" \";\n";
						// result += "puts -nonewline [string map {\" \" \"\"} [nodeDisp $key 2]];\n";
						// result += "puts -nonewline \" \";\n";
						// result += "puts -nonewline [string map {\" \" \"\"} [nodeDisp $key 3]];\n";
						// result += "puts -nonewline \" \";\n";
						// result += "}\n";
					// }
					// if(Moment === true) {
						// result += "puts \";\";\n";
						// result += "foreach key [getEleTags] {;\n";
						// result += "puts [regsub -all {\\s+} [string trim [eleResponse $key forces]] \" \"];\n";
						// result += "}\n";
					// }
					// console.log("tcl",result)
					return result;
					// return "test"
				},
				"runOpenSees" : function(tcl, success) {
					$D.Ajax.request({
						url : '/cgi-bin/lge/sketchit.bak/run.ops',
						params : tcl,
						method : 'POST',
						scope : this,
						success : function(result) {
							//console.log("displacement data: ",result.responseText);
							this.run("loadResultData", result.responseText);

						}
					});
				},
				"loadResultDataNew" : function(str) {
					var allData = str.trim().split(";");
					var len = allData.length;
					var i=0;
					var disp;
					var index;
					var flag=false;
					while(i < len && allData[i]) {
						disp=allData[i].split(':');
						index=disp[0];
						disp=disp[1].split(',');
						if($D.isDefined(this.theNodes[index])) {
							this.theNodes[index].dispX = parseFloat(disp[0]);
							this.theNodes[index].dispY = parseFloat(disp[1]);
							this.theNodes[index].dispRZ = parseFloat(disp[2]);
							if (this.theNodes[index].dispX != 0.0 || this.theNodes[index].dispY != 0.0 || this.theNodes[index].dispRZ != 0.0) {
								flag = true;
							}
						}
						i++; 
					}
					return flag;
					
				},
				"loadResultData" : function(str) {
					var arr0 = str.trim().split(";");
					var flag = false;
					// deformation data:
					if (arr0[0].trim() == "") {
						// this.deformationAvailable = false;
						return false;
					}
					var arr = arr0[0].trim().split(" "), i = 0, len = arr.length, index = 0;
					if (isNaN(parseFloat(arr[0]))) {
						return false;
					}
					while(i < len) {
						if($D.isDefined(this.theNodes[index])) {
							this.theNodes[index].dispX = parseFloat(arr[i++]);
							this.theNodes[index].dispY = parseFloat(arr[i++]);
							this.theNodes[index].dispRZ = parseFloat(arr[i++]);
							if (this.theNodes[index].dispX != 0.0 || this.theNodes[index].dispY != 0.0 || this.theNodes[index].dispRZ != 0.0) {
								// this.deformationAvailable = true;
								flag = true;
							}
							index++;
						} else {
							index++;
						}
					}
					// element response data
					if(!$D.isDefined(arr0[1]) || arr0[1].trim() == "") {
						return false;
					}
					arr = arr0[1].trim().split("\n");
					i = 0;
					index = 0;
					len = arr.length;
					var arr2;
					while(i < len) {
						if($D.isDefined(this.theElements[index])) {
							arr2 = arr[i].trim().split(" ");
							this.theElements[index].eleResponseFx1 = parseFloat(arr2[0]);
							this.theElements[index].eleResponseFy1 = parseFloat(arr2[1]);
							this.theElements[index].eleResponseM1 = parseFloat(arr2[2]);
							this.theElements[index].eleResponseFx2 = parseFloat(arr2[3]);
							this.theElements[index].eleResponseFy2 = parseFloat(arr2[4]);
							this.theElements[index].eleResponseM2 = parseFloat(arr2[5]);
							this.theElements[index].getLocalResponse();
							i++;
							index++;
						} else {
							index++;
						}
					}
					return flag;
				},
				"loadStaticAnalysisData" : function(str) {
					var arr = str.trim().split(" "), i = 0, len = arr.length, index = 0;
					while(i < len) {
						if($D.isDefined(this.theNodes[index])) {
							this.theNodes[index].dispX = parseFloat(arr[i++]);
							this.theNodes[index].dispY = parseFloat(arr[i++]);
							this.theNodes[index].dispRZ = parseFloat(arr[i++]);
							index++;
						} else {
							index++;
						}
					}
				}, 
				loadDynamicAnalysisData:function (str){
					var arr = str.trim().split(" "), i = 0, len = arr.length, index = 0;
					while(i < len) {
						if($D.isDefined(this.theNodes[index])) {
							this.theNodes[index].dispX = parseFloat(arr[i++]);
							this.theNodes[index].dispY = parseFloat(arr[i++]);
							this.theNodes[index].dispRZ = parseFloat(arr[i++]);
							index++;
						} else {
							index++;
						}
					}
				},
				loadNodeDispResultAtT:function(t) {
					var nid,node,ndisp;
					if (this.nodeDispHistory[t]){
						for(nid in this.nodeDispHistory[t]) {
							if(this.nodeDispHistory[t].hasOwnProperty(nid) && $D.isDefined(this.theNodes[nid])) {
								node = this.theNodes[nid];
								ndisp = this.nodeDispHistory[t][nid];
								node.dispX = ndisp[0];
								node.dispY = ndisp[1];
								node.dispRZ = ndisp[2];
							}
						}
					}
				},
				exportToJSON:function(){
					var result = {};
					var nodes={};
					var eles={};
					var spcs={};
					// var lpts={};
					
					
					
					
					for(i in this.theNodes) {
						if(this.theNodes.hasOwnProperty(i) && $D.isDefined(this.theNodes[i])) {
							nodes[i]=this.theNodes[i].toJSON();
						}
					}
					
					result.theNodes = nodes;
					
					for(i in this.theElements) {
						if(this.theElements.hasOwnProperty(i) && $D.isDefined(this.theElements[i])) {
							eles[i]=this.theElements[i].toJSON();
						}
					}
					
					result.theElements = eles;
					
					for(i in this.theSPCs) {
						if(this.theSPCs.hasOwnProperty(i) && $D.isDefined(this.theSPCs[i])) {
							spcs[i]=this.theSPCs[i].toJSON();
						}
					}
					
					result.theSPCs = spcs;
					// window.modelBK = result;
					return result;
					
					// console.log("domain json",result);
					
					// result.theElements = eles
					// result.theSPCs = spcs;
					// result.theLoadPatterns = lpts
					// return JSON.stringify(result);
					
				},
				nodeFindReference:function(n,Dm){
						
					if (n.SPCID!=="") {
						n.SPC=Dm.theSPCs[n.SPCID];
					}
					
					n.elements={};
					if (n.elementIDs) {
						for(var i=0,j=n.elementIDs.length; i<j; i++){
							n.elements[n.elementIDs[i]]=Dm.theElements[n.elementIDs[i]];
						}
					}
					n.ajacentNodes={};
					if (n.ajacentNodeIDs!=="") {
						for(var i=0,j=n.ajacentNodeIDs.length; i<j; i++){
							n.ajacentNodes[n.ajacentNodeIDs[i]]=Dm.theNodes[n.ajacentNodeIDs[i]];
						}
					}
				},
				eleFindReference:function(el,Dm){
					if (el.geomTransfId!=="") {
						el.geomTransf=Dm.theGeomTransfs[el.geomTransfId];
					}
					
					el.nodes=[];
					if (el.nodeIDs) {
						for(var i=0,j=el.nodeIDs.length; i<j; i++){
							el.nodes.push(Dm.theNodes[el.nodeIDs[i]])
						}
					} else if (el.conn) {
						for(var i=0,j=el.conn.length; i<j; i++){
							el.nodes.push(Dm.theNodes[el.conn[i]])
						}
					}
				},
				
				spcFindReference:function(spc,Dm){
					if (spc.nodeID!=="") {
						spc.node=Dm.theNodes[spc.nodeID];
					}
						
				},
				
				nodeClearConnectedElementIDs : function() {
					for(i in this.theNodes) {
						if(this.theNodes.hasOwnProperty(i) && $D.isDefined(this.theNodes[i])) {
							this.theNodes[i].elementIDs = [];
						}
					}
				},
				
				nodeFindConnectedElementIDs : function(){
					var nids;
					for(i in this.theElements) {
						if(this.theElements.hasOwnProperty(i) && $D.isDefined(this.theElements[i])) {
							if (this.theElements[i].nodeIDs) {
								nids = this.theElements[i].nodeIDs;
							} else if (this.theElements[i].conn){
								nids = this.theElements[i].conn;
							}
							for(var j = 0; j < nids.length; j++) {
								this.theNodes[nids[j]].elementIDs.push(this.theElements[i].id);
							}
						}
					}
				},
				
				nodeRefreshConnectedElementIDs : function() {
					this.nodeClearConnectedElementIDs();
					this.nodeFindConnectedElementIDs();
				},
				
					
				importModelFromServer:function(obj){
					this.restart();
					this.readyToRender = false;
					for(i in obj.theNodes) {
						if(obj.theNodes.hasOwnProperty(i) && $D.isDefined(obj.theNodes[i])) {
							this.theNodes[i]=new $D.Node();
                            // debugger;
							$D.apply(this.theNodes[i],{
								id : i,
								X:obj.theNodes[i][0],
								Y:obj.theNodes[i][1]
							});
							// $D.apply(this.theNodes[i], obj.theNodes[i]);
                            this.theNodes[i].beforeMoveX = this.theNodes[i].X;
                            this.theNodes[i].beforeMoveY = this.theNodes[i].Y;
						}
					}
                    for(i in obj.theMasses) {
						if(obj.theMasses.hasOwnProperty(i) && $D.isDefined(obj.theMasses[i])) {
                            if (this.theNodes[i]){
                                this.theNodes[i].massX = obj.theMasses[i][0];
                                this.theNodes[i].massY = obj.theMasses[i][1];
                                this.theNodes[i].massRz = obj.theMasses[i][2];
                            }
						}
					}
					for(i in obj.theElements) {
						if(obj.theElements.hasOwnProperty(i) && $D.isDefined(obj.theElements[i])) {
							// if(obj.theElements[i]["className"]) {
							// 	this.theElements[i]=new $D[obj.theElements[i]["className"]](obj.theElements[i]);
							// } else {
                            var eleType = obj.theElements[i]["type"];
                            // debugger;
                            if (!$D[eleType]){
                                eleType = "Element";
                            }
                            this.theElements[i]=new $D[eleType](obj.theElements[i]);
                            // debugger;
							$D.apply(this.theElements[i], {
                                nodeIDs : obj.theElements[i].conn
                            });
							$D.apply(this.theElements[i], obj.theElements[i].properties);
						}
					}
					for(i in obj.theSPConstraints) {
						if(obj.theSPConstraints.hasOwnProperty(i) && $D.isDefined(obj.theSPConstraints[i])) {
                            var spcid = obj.theSPConstraints[i][0];
                            var dir = obj.theSPConstraints[i][1];
                            var val = obj.theSPConstraints[i][2];
                            if (!this.theSPCs[spcid]){
							    this.theSPCs[spcid]=new $D.SPConstraint();
                            }
                            // debugger;
							// $D.apply(this.theSPCs[i],obj.theSPCs[i]);
							$D.apply(this.theSPCs[spcid], {
                                id : spcid,
                                nodeID : spcid
                            });
                            if (obj.theSPConstraints[i][1] === 0 && obj.theSPConstraints[i][2] === 0){
                                this.theSPCs[spcid].X = 1;
                            } else if (obj.theSPConstraints[i][1] === 1 && obj.theSPConstraints[i][2] === 0){
                                this.theSPCs[spcid].Y = 1;
                            } else if (obj.theSPConstraints[i][1] === 2 && obj.theSPConstraints[i][2] === 0){
                                this.theSPCs[spcid].RZ = 1;
                            }

                            if (this.theSPCs[spcid].X === 1 &&
                                this.theSPCs[spcid].Y === 0){
                                this.theSPCs[spcid].direction = 'left';
                                this.theSPCs[spcid].angle = Math.PI;                                
                            }
                            // this.theSPCs[i].
						}
					}
					
					this.rebuildNodeAjacency();
					
					for(i in obj.theNodes) {
						if(obj.theNodes.hasOwnProperty(i) && $D.isDefined(obj.theNodes[i])) {
							this.nodeFindReference(this.theNodes[i], this);
						}
					}
					for(i in obj.theElements) {
                        // debugger;
						if(obj.theElements.hasOwnProperty(i) && $D.isDefined(obj.theElements[i])) {
							this.eleFindReference(this.theElements[i], this);
						}
					}
					for(i in this.theSPCs) {
						if(this.theSPCs.hasOwnProperty(i) && $D.isDefined(this.theSPCs[i])) {
							this.spcFindReference(this.theSPCs[i], this);
						}
                        this.theSPCs[i].node.SPC = this.theSPCs[i];
					}
					this.readyToRender = true;
				},
				
				
				
				
				
				buildModelFromJson:function(obj){
					this.restart();
					this.readyToRender = false;
					for(i in obj.theNodes) {
						if(obj.theNodes.hasOwnProperty(i) && $D.isDefined(obj.theNodes[i])) {
							this.theNodes[i]=new $D.Node();
                            // debugger;
							// $D.apply(this.theNodes[i],{
							// 	id : i,
							// 	X:obj.theNodes[i][0] || obj.theNodes[i].X,
							// 	Y:obj.theNodes[i][1] || obj.theNodes[i].Y,
                                
							// });
							$D.apply(this.theNodes[i], obj.theNodes[i]);
						}
					}
					for(i in obj.theElements) {
						if(obj.theElements.hasOwnProperty(i) && $D.isDefined(obj.theElements[i])) {
							if(obj.theElements[i]["className"]) {
								this.theElements[i]=new $D[obj.theElements[i]["className"]](obj.theElements[i]);
							} else {
								this.theElements[i]=new $D[obj.theElements[i]["type"]](obj.theElements[i]);
							}
							// $D.apply(this.theElements[i],obj.theElements[i]);
						}
					}
					for(i in obj.theSPCs) {
						if(obj.theSPCs.hasOwnProperty(i) && $D.isDefined(obj.theSPCs[i])) {
							this.theSPCs[i]=new $D.SPConstraint();
							$D.apply(this.theSPCs[i],obj.theSPCs[i]);
						}
					}
					
					this.rebuildNodeAjacency();
					
					for(i in obj.theNodes) {
						if(obj.theNodes.hasOwnProperty(i) && $D.isDefined(obj.theNodes[i])) {
							this.nodeFindReference(this.theNodes[i],this);
						}
					}
					for(i in obj.theElements) {
                        // debugger;
						if(obj.theElements.hasOwnProperty(i) && $D.isDefined(obj.theElements[i])) {
							this.eleFindReference(this.theElements[i],this);
						}
					}
					for(i in obj.theSPCs) {
						if(obj.theSPCs.hasOwnProperty(i) && $D.isDefined(obj.theSPCs[i])) {
							this.spcFindReference(this.theSPCs[i],this);
						}
                        this.theSPCs[i].node.SPC = this.theSPCs[i];
					}
					this.readyToRender = true;
				},
				
			
				clearNodeAjacency : function() {
					obj = this;
					for(i in obj.theNodes) {
						if(obj.theNodes.hasOwnProperty(i) && $D.isDefined(obj.theNodes[i])) {
							obj.theNodes[i].ajacentNodeIDs=[];
						}
					}
					
				},
				
				buildNodeAjacency : function(){
					var obj = this;
					var nids;
					var nid;
					for(i in obj.theElements) {
						if(obj.theElements.hasOwnProperty(i) && $D.isDefined(obj.theElements[i])) {
							nids=obj.theElements[i].nodeIDs;
							for(i in nids) {
								if(nids.hasOwnProperty(i) && $D.isDefined(nids[i])) {
									nid = nids[i];
									for(i in nids) {
										if(nids.hasOwnProperty(i) && $D.isDefined(nids[i])) {
											if (nid != nids[i]) {
												obj.theNodes[nid].ajacentNodeIDs.push(nids[i]);
											}
										}
									}
								}
							}
							
						}
					}
					
				},
				
				rebuildNodeAjacency : function(){
					this.clearNodeAjacency();
					this.buildNodeAjacency();
				},
				
			});

		}
	});

	DirectFEA.Domain = Domain;

})();
